[![pipeline status](https://gitlab.com/robmoggach/robmoggach.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/robmoggach/robmoggach.gitlab.io/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/f9c82774-fca1-4334-b229-84afe6f1b4d5/deploy-status)](https://app.netlify.com/sites/moggach/deploys)
# moggach.com gatsby static site

This repo is a gatsby static site generator for a portfolio site.

This `README.md` file documents tips, notes, techniques, and all the other things that fall out of my head over time so we don't need to relearn what we've already accomplished.

