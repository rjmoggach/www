---
title: Bio
slug: about-bio
date: '2019-07-15'
headline:
cover: images/rob-hand-shadow.jpg
---

## Bio

After 20 years of working for incredibly talented directors and creative teams worldwide as Creative Director, VFX Supervisor & Flame Artist, Robert "Rob" Moggach had the distinct privilege of working on high profile projects in VFX, Animation & Design with a long list of inspiring creative and technical artists.

Originally trained as a filmmaker, photographer, designer and subsequently animator, Moggach’s creative talents have pushed the boundaries of visual effects from his perspective as artist and director. This perspective has built his reputation for having an ability to intuitively understand and deliver on complex high-end creative visions.

### History

As a VFX Supervisor and Flame Artist, Moggach has worked in collaboration with a host of world class directors for high profile clients such as Playstation, Coke, Nike, Xbox, Gatorade, Adidas and countless automotive brands. After working at studios that include Asylum, Method, and Digital Domain, Moggach led his own creative studio, Dashing Collective. From there he joined Zoic Studios and MassMarket serving as Creative Director for both studios before returning to Canada.
