---
postType: blog
published: false
featured: false
title: A New Website
date: '2019-08-01'
section: blog
cover: 'placeimg_1000_480_tech3.jpg'
tags: ['news', 'development', 'react', 'github', 'gatsbyjs', 'ssg', 'static site generator']
draft: false
categories: ['General']
---

It's been 15-20 years that I've been working from my own python/django codebase and 2019 I decided to move to static website framework Gatsby. It's fast and worth every penny.

Instead of:

```python
import django
```

I now have:

```javascript
/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
```
