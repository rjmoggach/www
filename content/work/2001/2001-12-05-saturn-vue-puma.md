---
title: Saturn Vue Puma
subtitle:
slug: saturn-vue-puma
date: 2001-12-05
role:
headline: directed by Frederic Planchon, Academy Films
summary: Puma chases arctic hares and tiny Saturn Vue through the snow.
excerpt: Puma chases arctic hares and tiny Saturn Vue through the snow.
published: false
featured: false
categories:
- Advertising
tags:
- compositing
- tracking
- weather
- automotive
- animals
albums:
-   cover:
    images:
    -   caption: ''
        filename: saturn-vue-puma.01.jpg
    -   caption: ''
        filename: saturn-vue-puma.02.jpg
    -   caption: ''
        filename: saturn-vue-puma.03.jpg
    -   caption: ''
        filename: saturn-vue-puma.04.jpg
    -   caption: ''
        filename: saturn-vue-puma.05.jpg
    path: work/2001/saturn-vue-puma/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/saturn-vue-puma/videos/saturn-vue-puma.jpg
    path: work/2001/saturn-vue-puma/videos/saturn-vue-puma
    slug: saturn-vue-puma
    sources:
    -   filename: saturn-vue-puma.1280x960.mp4
        format: mp4
        height: '960'
        size: 10560231
        width: '1280'
    -   filename: saturn-vue-puma.960x720.mp4
        format: mp4
        height: '720'
        size: 7875119
        width: '960'
    -   filename: saturn-vue-puma.640x480.mp4
        format: mp4
        height: '480'
        size: 5331732
        width: '640'
    -   filename: saturn-vue-puma.640x360.ogv
        format: ogv
        height: '360'
        size: 5264386
        width: '640'
    -   filename: saturn-vue-puma.640x360.webm
        format: webm
        height: '360'
        size: 4086698
        width: '640'
    -   filename: saturn-vue-puma.480x360.mp4
        format: mp4
        height: '360'
        size: 3728292
        width: '480'
    title: Saturn Vue Puma
credits:
-   companyName: Saturn
    companySlug: saturn
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Academy Films
    companySlug: academy-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Frederic Planchon
    personSlug: frederic-planchon
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
This commercial, shot in Argentina and northern Canada, tells the story of a
"hare-sized SUV that along with it's hare companions narrowly escapes a puma.
Practically every shot involved a great deal of complex visual effects. The
first task was to successfully create a miniature Saturn among a herd of
arctic hares. Footage of the real truck was shot using the rabbit location and
a miniature truck for size and position reference. Reflections of the rabbits
and surrounding mountain / forest were added, in addition to multiple snow
elements, to create a miniature from the original full size truck. The second
task was to create arctic hares. Because of a lack of suitable white rabbits,
brown rabbits were used. The brown rabbits were made white and their ears were
lengthened to create the hares. Each shot consists of at least three hares
shot separately in different positions. To add to the complexity of the
effects, the majority of the footage was shot hand-held to give the film a
greater sense of realism and dramatic energy.