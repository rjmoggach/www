---
title: Mercedes | "Modern Ark"
subtitle: 'Modern Ark'
slug: mercedes-modern-ark
date: 2001-02-05
role:
headline: directed by Gerard De Thame, GDT Films
summary: Two Mercedes are loaded onto Noah's Ark.
excerpt: Two Mercedes are loaded onto Noah's Ark.
published: true
featured: true
categories:
    - Advertising
tags:
    - automotive
albums:
    - cover:
      images:
          - caption: ''
            filename: mercedes-modern-ark.01.jpg
          - caption: ''
            filename: mercedes-modern-ark.02.jpg
          - caption: ''
            filename: mercedes-modern-ark.03.jpg
          - caption: ''
            filename: mercedes-modern-ark.04.jpg
          - caption: ''
            filename: mercedes-modern-ark.05.jpg
          - caption: ''
            filename: mercedes-modern-ark.06.jpg
          - caption: ''
            filename: mercedes-modern-ark.07.jpg
      path: work/2001/mercedes-modern-ark/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2001/mercedes-modern-ark/videos/mercedes-modern-ark.jpg
      path: work/2001/mercedes-modern-ark/videos/mercedes-modern-ark
      slug: mercedes-modern-ark
      sources:
          - filename: mercedes-modern-ark.1280x960.mp4
            format: mp4
            height: '960'
            size: 21044359
            width: '1280'
          - filename: mercedes-modern-ark.960x720.mp4
            format: mp4
            height: '720'
            size: 17134229
            width: '960'
          - filename: mercedes-modern-ark.640x480.mp4
            format: mp4
            height: '480'
            size: 10646636
            width: '640'
          - filename: mercedes-modern-ark.640x360.ogv
            format: ogv
            height: '360'
            size: 10465980
            width: '640'
          - filename: mercedes-modern-ark.640x360.webm
            format: webm
            height: '360'
            size: 9118082
            width: '640'
          - filename: mercedes-modern-ark.480x360.mp4
            format: mp4
            height: '360'
            size: 8010175
            width: '480'
      title: Mercedes Modern Ark
credits:
    - companyName: Mercedes Benz
      companySlug: mercedes-benz
      roleSlug: client
      roleTitle: Client
      visible: false
    - companyName: GDT Films
      companySlug: gdt-films
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Fabyan Daw
      personSlug: fabyan-daw
      roleSlug: prod-ep
      roleTitle: Executive Producer
      visible: false
    - personName: Gerard de Thame
      personSlug: gerard-de-thame
      roleSlug: director
      roleTitle: Director
      visible: false
    - companyName: Smoke & Mirrors
      companySlug: smoke-mirrors
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: false
    - personName: Tom Sparks
      personSlug: tom-sparks
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: false
    - personName: Eva Pusnik
      personSlug: eva-pusnik
      roleSlug: producer
      roleTitle: Producer
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame-artist
      roleTitle: Flame Artist
      visible: false
---

Nominated for an Emmy for the most dramatic commercial of 2001, this has been
one of my most memorable commercial projects. Gerard De Thame is world-
renowned for his 'monumental' commercials that involve the biggest commercial
productions and the best artists worldwide. Combining multiple motion control
passes of lions, giraffes, elephants, extras, mercedes, full size sets, model
boats and penguins was our task on this commercial for Mercedes Benz. Every
shot involved effects work in some form or other. All the backgrounds still
and moving were created from high resolution stills and multiple cloud footage
elements. Motion control data and 3D tracking information was used in inferno
throughout. For the opening panning shot of the animals entering the ark I had
the job of creating a background desert plane and sky that matched the live
action motion control set, model and animal elements and integrating the boat
set and model. The dramatic closing helicopter shot of the ark was created
with one live action motion control element of the ark and one oversized
stitched background image. Using 3D projections and selective matting and
tracking the illusion of a 3D environment was created. The same methods were
used in varying degrees of complexity for the Mercedes sequence and other
moving shots throughout the spot.
