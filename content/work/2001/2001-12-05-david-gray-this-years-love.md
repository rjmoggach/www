---
title: David Gray This Years Love
subtitle:
slug: david-gray-this-years-love
date: 2000-12-05
role:
headline: directed by David Slade, Bullett
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: david-gray-this-years-love.01.jpg
    -   caption: ''
        filename: david-gray-this-years-love.02.jpg
    -   caption: ''
        filename: david-gray-this-years-love.03.jpg
    -   caption: ''
        filename: david-gray-this-years-love.04.jpg
    -   caption: ''
        filename: david-gray-this-years-love.05.jpg
    -   caption: ''
        filename: david-gray-this-years-love.06.jpg
    -   caption: ''
        filename: david-gray-this-years-love.07.jpg
    -   caption: ''
        filename: david-gray-this-years-love.08.jpg
    -   caption: ''
        filename: david-gray-this-years-love.09.jpg
    -   caption: ''
        filename: david-gray-this-years-love.10.jpg
    -   caption: ''
        filename: david-gray-this-years-love.11.jpg
    path: work/2001/david-gray-this-years-love/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/david-gray-this-years-love/videos/david-gray-this-years-love.jpg
    path: work/2001/david-gray-this-years-love/videos/david-gray-this-years-love
    slug: david-gray-this-years-love
    sources:
    -   filename: david-gray-this-years-love-1280x720.mp4
        format: mp4
        height: '720'
        size: 37209188
        width: '1280'
    -   filename: david-gray-this-years-love-960x540.mp4
        format: mp4
        height: '540'
        size: 37110234
        width: '960'
    -   filename: david-gray-this-years-love-640x360.mp4
        format: mp4
        height: '360'
        size: 37038460
        width: '640'
    -   filename: david-gray-this-years-love-480x270.mp4
        format: mp4
        height: '270'
        size: 36930262
        width: '480'
    -   filename: david-gray-this-years-love-640x360.ogv
        format: ogv
        height: '360'
        size: 27721428
        width: '640'
    -   filename: david-gray-this-years-love-640x360.webm
        format: webm
        height: '360'
        size: 10043685
        width: '640'
    title: David Gray This Years Love
credits: []
---
To tell the story of lost souls, alone in the world, director David Slade came
up with this characteristically dream-like treatment. The lost souls in the
video are followed by rain clouds that continually drench them, strengthening
their plight. My job therefore was to create miniature storm clouds that
followed these people. The challenge was to create clouds that maintained
their realism without appearing cartoon-like. The video was shot on multiple
location and used a custom built rain machine to create the selective downpour
that followed the lost souls. Using milk shot in slow motion in water I
created the cloud element that follows these people. The milk was looped,
reshaped, painted and graded to create a sense of directional lighting, and
the rain was enhance with computer generated rain created in Inferno. Other
effects included the star field in the end sequence that involved Inferno
generated stars converging on David Gray on live action footage shot with a
crane. All tracking of the hand held footage was done in Inferno.