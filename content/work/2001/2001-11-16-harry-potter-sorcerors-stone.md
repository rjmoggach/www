---
title: Harry Potter | "The Sorcerors Stone"
subtitle: 'The Sorcerors Stone'
slug: harry-potter-sorcerors-stone
date: 2001-11-16
role:
headline: Harry Potter I
summary:
excerpt:
published: false
featured: false
categories:
    - Feature Films
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: harry-potter-sorcerors-stone.01.jpg
          - caption: ''
            filename: harry-potter-sorcerors-stone.02.jpg
          - caption: ''
            filename: harry-potter-sorcerors-stone.03.jpg
          - caption: ''
            filename: harry-potter-sorcerors-stone.04.jpg
      path: work/2001/harry-potter-sorcerors-stone/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone.jpg
      path: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone
      slug: harry-potter-sorcerors-stone
      sources:
          - filename: harry-potter-sorcerors-stone-1280x720.mp4
            format: mp4
            height: '720'
            size: 16505221
            width: '1280'
          - filename: harry-potter-sorcerors-stone-960x540.mp4
            format: mp4
            height: '540'
            size: 16468031
            width: '960'
          - filename: harry-potter-sorcerors-stone-640x360.mp4
            format: mp4
            height: '360'
            size: 16399298
            width: '640'
          - filename: harry-potter-sorcerors-stone-480x270.mp4
            format: mp4
            height: '270'
            size: 16352566
            width: '480'
          - filename: harry-potter-sorcerors-stone-640x360.ogv
            format: ogv
            height: '360'
            size: 5294669
            width: '640'
          - filename: harry-potter-sorcerors-stone-640x360.webm
            format: webm
            height: '360'
            size: 4309780
            width: '640'
      title: Harry Potter Sorcerors Stone
    - caption: ''
      cover: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-fireplace.jpg
      path: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-fireplace
      slug: harry-potter-sorcerors-stone-fireplace
      sources:
          - filename: harry-potter-sorcerors-stone-fireplace-480x270.mp4
            format: mp4
            height: '270'
            size: 2939746
            width: '480'
          - filename: harry-potter-sorcerors-stone-fireplace-640x360.mp4
            format: mp4
            height: '360'
            size: 2918263
            width: '640'
          - filename: harry-potter-sorcerors-stone-fireplace-960x540.mp4
            format: mp4
            height: '540'
            size: 2894771
            width: '960'
          - filename: harry-potter-sorcerors-stone-fireplace-1280x720.mp4
            format: mp4
            height: '720'
            size: 2885355
            width: '1280'
          - filename: harry-potter-sorcerors-stone-fireplace-640x360.ogv
            format: ogv
            height: '360'
            size: 955255
            width: '640'
          - filename: harry-potter-sorcerors-stone-fireplace-640x360.webm
            format: webm
            height: '360'
            size: 778724
            width: '640'
      title: Harry Potter Sorcerors Stone Fireplace
    - caption: ''
      cover: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-invisibility-cloak.jpg
      path: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-invisibility-cloak
      slug: harry-potter-sorcerors-stone-invisibility-cloak
      sources:
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-480x270.mp4
            format: mp4
            height: '270'
            size: 44973699
            width: '480'
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-640x360.mp4
            format: mp4
            height: '360'
            size: 44876555
            width: '640'
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-960x540.mp4
            format: mp4
            height: '540'
            size: 44744637
            width: '960'
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-1280x720.mp4
            format: mp4
            height: '720'
            size: 44733603
            width: '1280'
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-640x360.ogv
            format: ogv
            height: '360'
            size: 14058972
            width: '640'
          - filename: harry-potter-sorcerors-stone-invisibility-cloak-640x360.webm
            format: webm
            height: '360'
            size: 11881778
            width: '640'
      title: Harry Potter Sorcerors Stone Invisibility Cloak
    - caption: ''
      cover: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-pigtail.jpg
      path: work/2001/harry-potter-sorcerors-stone/videos/harry-potter-sorcerors-stone-pigtail
      slug: harry-potter-sorcerors-stone-pigtail
      sources:
          - filename: harry-potter-sorcerors-stone-pigtail-1280x720.mp4
            format: mp4
            height: '720'
            size: 3820258
            width: '1280'
          - filename: harry-potter-sorcerors-stone-pigtail-960x540.mp4
            format: mp4
            height: '540'
            size: 3787831
            width: '960'
          - filename: harry-potter-sorcerors-stone-pigtail-640x360.mp4
            format: mp4
            height: '360'
            size: 3750511
            width: '640'
          - filename: harry-potter-sorcerors-stone-pigtail-480x270.mp4
            format: mp4
            height: '270'
            size: 3716940
            width: '480'
          - filename: harry-potter-sorcerors-stone-pigtail-640x360.ogv
            format: ogv
            height: '360'
            size: 1177572
            width: '640'
          - filename: harry-potter-sorcerors-stone-pigtail-640x360.webm
            format: webm
            height: '360'
            size: 971229
            width: '640'
      title: Harry Potter Sorcerors Stone Pigtail
credits:
    - companyName: Warner Bros.
      companySlug: warner-bros
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Chris Columbus
      personSlug: chris-columbus
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: lead flame
      roleTitle: Lead Flame
      visible: false
---
