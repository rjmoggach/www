---
title: Robert Miles Paths
subtitle:
slug: robert-miles-paths
date: 2001-03-01
role:
headline: directed by Arran Bowyn, HLA
summary: Skeletal creature sings melodically to hypnotic beats.
excerpt: Skeletal creature sings melodically to hypnotic beats.
published: false
featured: false
categories:
- Music Videos
tags:
- compositing
- tracking
albums:
-   cover:
    images:
    -   caption: ''
        filename: robert-miles-paths.01.jpg
    -   caption: ''
        filename: robert-miles-paths.02.jpg
    -   caption: ''
        filename: robert-miles-paths.03.jpg
    -   caption: ''
        filename: robert-miles-paths.04.jpg
    -   caption: ''
        filename: robert-miles-paths.05.jpg
    path: work/2001/robert-miles-paths/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/robert-miles-paths/videos/robert-miles-paths.jpg
    path: work/2001/robert-miles-paths/videos/robert-miles-paths
    slug: robert-miles-paths
    sources:
    -   filename: robert-miles-paths.1280x720.mp4
        format: mp4
        height: '720'
        size: 63177194
        width: '1280'
    -   filename: robert-miles-paths.960x540.mp4
        format: mp4
        height: '540'
        size: 43071309
        width: '960'
    -   filename: robert-miles-paths.640x360.mp4
        format: mp4
        height: '360'
        size: 28286076
        width: '640'
    -   filename: robert-miles-paths.640x360.ogv
        format: ogv
        height: '360'
        size: 22958341
        width: '640'
    -   filename: robert-miles-paths.640x360.webm
        format: webm
        height: '360'
        size: 17980475
        width: '640'
    -   filename: robert-miles-paths.480x270.mp4
        format: mp4
        height: '270'
        size: 17947933
        width: '480'
    title: Robert Miles Paths
credits:
-   personName: Arran Bowyn
    personSlug: arran-bowyn
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Director Arran Bowyn envisioned a surreal Giger-esque creature for this music
video for Robert Miles. A woman collapses while carrying a puppet and the
puppet is imbued with life. It sings the song to us while dancing languidly
and we realise that the face on the puppet singing is the woman who had
earlier collapsed. Creating a singing puppet with a somewhat human head was a
big visual effects problem for this music video. Our solution to the problem
was to shoot the puppet without a head and remove the wires in Inferno. This
maintained the characteristic puppet motion. The singer was then fitted with a
prosthetic head that was modelled from the original miniature puppet head. Her
neck in body were covered in chroma blue fabric and she was shot against a
blue screen. This allowed us to track her real head onto the body of the
dancing headless puppet. Removing the puppet strings, recreating realistic
shadows, and rebuilding features hidden by the singer's neck and body were
some of the main obstacles and a lot of time was spent on cosmetic cleanup
work as well to enhance her beauty.