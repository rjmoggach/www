---
title: Muse Bliss
subtitle:
slug: muse-bliss
date: 2001-06-15
role:
headline: directed by David Slade, Bullett
summary: Man travels through space and time in epic sci-fi.
excerpt: Man travels through space and time in epic sci-fi.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: muse-bliss.01.jpg
    -   caption: ''
        filename: muse-bliss.02.jpg
    -   caption: ''
        filename: muse-bliss.03.jpg
    -   caption: ''
        filename: muse-bliss.04.jpg
    -   caption: ''
        filename: muse-bliss.05.jpg
    -   caption: ''
        filename: muse-bliss.06.jpg
    -   caption: ''
        filename: muse-bliss.07.jpg
    -   caption: ''
        filename: muse-bliss.08.jpg
    -   caption: ''
        filename: muse-bliss.09.jpg
    path: work/2001/muse-bliss/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2001/muse-bliss/videos/muse-bliss.jpg
    path: work/2001/muse-bliss/videos/muse-bliss
    slug: muse-bliss
    sources:
    -   filename: muse-bliss.1280x960.mp4
        format: mp4
        height: '960'
        size: 94018969
        width: '1280'
    -   filename: muse-bliss.960x720.mp4
        format: mp4
        height: '720'
        size: 72994000
        width: '960'
    -   filename: muse-bliss.640x480.mp4
        format: mp4
        height: '480'
        size: 47496003
        width: '640'
    -   filename: muse-bliss.640x360.ogv
        format: ogv
        height: '360'
        size: 42361360
        width: '640'
    -   filename: muse-bliss.480x360.mp4
        format: mp4
        height: '360'
        size: 31311751
        width: '480'
    -   filename: muse-bliss.640x360.webm
        format: webm
        height: '360'
        size: 29208045
        width: '640'
    title: Muse Bliss
credits:
-   companyName: Bullett
    companySlug: bullett
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: David Slade
    personSlug: david-slade
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Muse is one of the most popular rock bands in the United Kingdom. Their albums
are known for multiple best-selling singles. A lot of importance is therefore
afforded to the creation of their unique and creative music videos. This music
video depicting a man falling through a dramatic science-fiction landscape in
deep space involved heavy effects work on almost all the shots. The sequence
shown here depicts the man falling out of the massive spaceship / monolith and
into open space. Backgrounds were created entirely in Inferno using a
combination of different proprietary plug-ins and animated elements. I created
a massive star field environment in Inferno within which the live-action
singer could plummet. With this environment created we were able to maintain
continuity as the singer fell deeper and deeper into an ever-thickening body
of stars and light.