---
title: Mercedes | "Can't Is A Challenge"
subtitle: "Can't Is A Challenge"
slug: mercedes-cant-is-a-challenge
date: 2018-03-02
role:
    - Director
    - VFX Supervisor
    - Creative Director
    - Animator
    - TD
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: mercedes-cant-is-a-challenge.01.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.02.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.03.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.04.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.05.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.06.jpg
          - caption: ''
            filename: mercedes-cant-is-a-challenge.07.jpg
      path: work/2018/mercedes-cant-is-a-challenge/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2018/mercedes-cant-is-a-challenge/videos/mercedes-cant-is-a-challenge/mercedes-cant-is-a-challenge.jpg
      path: work/2018/mercedes-cant-is-a-challenge/videos/mercedes-cant-is-a-challenge
      slug: mercedes-cant-is-a-challenge
      provider: html5
      sources:
          - filename: mercedes-cant-is-a-challenge-1280x720.mp4
            format: mp4
            height: '720'
            size:
            width: '1280'
          - filename: mercedes-cant-is-a-challenge-960x540.mp4
            format: mp4
            height: '540'
            size:
            width: '960'
          - filename: mercedes-cant-is-a-challenge-480x270.ogv
            format: ogv
            height: '270'
            size:
            width: '480'
          - filename: mercedes-cant-is-a-challenge-640x360.mp4
            format: mp4
            height: '360'
            size:
            width: '640'
          - filename: mercedes-cant-is-a-challenge-640x360.webm
            format: webm
            height: '360'
            size:
            width: '640'
          - filename: mercedes-cant-is-a-challenge-480x270.mp4
            format: mp4
            height: '270'
            size:
            width: '480'
      title: Mercedes "Can't Is A Challenge"
credits:
    - companyName: The Artery
      companySlug: the-artery
      roleSlug: production
      roleTitle: Production
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Deborah Sullivan
      personSlug: deborah-sullivan
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Paul Cameron, ASC
      personSlug: paul-cameron
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Vico Sharibani
      personSlug: vico-sharibani
      roleSlug: director
      roleTitle: Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Vico Sharibani
      personSlug: vico-sharibani
      roleSlug: Creative Director
      roleTitle: Creative Director
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Lynzi Grant
      personSlug: lynzi-grant
      roleSlug: vfx-producer
      roleTitle: Producer
      visible: true
    - companyName: The Artery
      companySlug: the-artery
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
---
