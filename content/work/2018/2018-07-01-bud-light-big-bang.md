---
title: Bud Light | "Big Bang"
subtitle: Big Bang
slug: bud-light-big-bang
date: 2018-07-01
role:
headline:
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: bud-light-big-bang.01.jpg
          - caption: ''
            filename: bud-light-big-bang.02.jpg
          - caption: ''
            filename: bud-light-big-bang.03.jpg
          - caption: ''
            filename: bud-light-big-bang.04.jpg
          - caption: ''
            filename: bud-light-big-bang.05.jpg
          - caption: ''
            filename: bud-light-big-bang.06.jpg
          - caption: ''
            filename: bud-light-big-bang.07.jpg
          - caption: ''
            filename: bud-light-big-bang.08.jpg
          - caption: ''
            filename: bud-light-big-bang.09.jpg
      path: work/2018/bud-light-big-bang/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2018/bud-light-big-bang/videos/bud-light-big-bang/bud-light-big-bang.jpg
      path: work/2018/bud-light-big-bang/videos/bud-light-big-bang
      slug: bud-light-big-bang
      provider: html5
      sources:
          - filename: bud-light-big-bang-1280x720.mp4
            format: mp4
            height: '720'
            size:
            width: '1280'
          - filename: bud-light-big-bang-960x540.mp4
            format: mp4
            height: '540'
            size:
            width: '960'
          - filename: bud-light-big-bang-480x270.ogv
            format: ogv
            height: '270'
            size:
            width: '480'
          - filename: bud-light-big-bang-640x360.mp4
            format: mp4
            height: '360'
            size:
            width: '640'
          - filename: bud-light-big-bang-640x360.webm
            format: webm
            height: '360'
            size:
            width: '640'
          - filename: bud-light-big-bang-480x270.mp4
            format: mp4
            height: '270'
            size:
            width: '480'
      title: Bud Light "Big Bang"
credits:
    - companyName: Nice Shoes
      companySlug: niceshoes
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Kristen Van Fleet
      personSlug: kristen-van-fleet
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Peter McAuley
      personSlug: peter-mcauley
      roleSlug: onset-supervisor
      roleTitle: Onset Supervisor
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Hardave Grewal
      personSlug: hardave-grewal
      roleSlug: nuke
      roleTitle: Nuke Compositor
      visible: true
    - companyName: Nice Shoes
      companySlug: niceshoes
      personName: Lorne Kwechansky
      personSlug: lorne-kwechansky
      roleSlug: fx-td
      roleTitle: CG FX TD
      visible: true
---

Thousands of frames per second produced minutes of footage ultimately compressed into a fun, blistering pace, 30 second spot.

The Bolt camera is a wonderful piece of technology and combined with the Phantom camera we started in a creatively flexible place. The shoot was a spectacle to witness with the incredibly high speed of the rig in close proximity to the talent.

For VFX, CG droplets and beer foam were thanks to a regular collaborator, CG Supervisor Lorne Kwechansky. Hardave helped with much needed comp support.

I ran the flame and developed tools for all three platforms (Nuke, Houdini, Flame) to programmatically rebuild the complex retimes seamlessly in correspondence with overlength CG tracking of the multiple motion control camera, varisped Phantom footage clips. _Say that five times fast._
