---
title: Tetley Revolving Routine
subtitle:
slug: tetley-revolving-routine
date: 2011-07-05
role:
headline: directed by The Perlorians, Soft Citizen for john st.
summary: In this quirky spot out of john st. Toronto, our hero Stacey is clearly stuckin
    a rut. We follow her day in and day out as she goes through the samemotions passing
    the same photocopier guy "Steve", drinking from the same watercooler, until one
    day she discovers Tetley Infusions.
excerpt: In this quirky spot out of john st. Toronto, our hero Stacey is clearly stuckin
    a rut. We follow her day in and day out as she goes through the samemotions passing
    the same photocopier guy "Steve", drinking from the same watercooler, until one
    day she discovers Tetley Infusions.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- comedy
- transitions
albums:
-   cover:
    images:
    -   caption: ''
        filename: tetley-revolving-routine.01.jpg
    -   caption: ''
        filename: tetley-revolving-routine.02.jpg
    -   caption: ''
        filename: tetley-revolving-routine.03.jpg
    -   caption: ''
        filename: tetley-revolving-routine.04.jpg
    -   caption: ''
        filename: tetley-revolving-routine.05.jpg
    -   caption: ''
        filename: tetley-revolving-routine.06.jpg
    path: work/2011/tetley-revolving-routine/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2011/tetley-revolving-routine/videos/tetley-revolving-routine.jpg
    path: work/2011/tetley-revolving-routine/videos/tetley-revolving-routine
    slug: tetley-revolving-routine
    sources:
    -   filename: tetley-revolving-routine-1280x720.mp4
        format: mp4
        height: '720'
        size: 19197142
        width: '1280'
    -   filename: tetley-revolving-routine-960x540.mp4
        format: mp4
        height: '540'
        size: 4684375
        width: '960'
    -   filename: tetley-revolving-routine-640x360.ogv
        format: ogv
        height: '360'
        size: 3652118
        width: '640'
    -   filename: tetley-revolving-routine-640x360.mp4
        format: mp4
        height: '360'
        size: 3227438
        width: '640'
    -   filename: tetley-revolving-routine-640x360.webm
        format: webm
        height: '360'
        size: 2935177
        width: '640'
    -   filename: tetley-revolving-routine-480x270.mp4
        format: mp4
        height: '270'
        size: 2304015
        width: '480'
    title: Tetley Revolving Routine
credits:
-   companyName: Tetley
    companySlug: tetley
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: john st.
    companySlug: john-st
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Stephen Jurisic
    personSlug: stephen-jurisic
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Kyle Lamb
    personSlug: kyle-lamb
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Kurt Mills
    personSlug: kurt-mills
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Dale Giffen
    personSlug: dale-giffen
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Chris Van Dyke
    personSlug: chris-van-dyke
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sarah Brooks
    personSlug: sarah-brooks
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
Dashing teams up once again with The Perlorian Bros. on Tetley's "Revolving
Routine", providing a much needed boost to an otherwise dreary day.

In this quirky spot out of john st. Toronto, our hero Stacey is clearly stuck
in a rut. We follow her day in and day out as she goes through the same
motions passing the same photocopier guy "Steve", drinking from the same water
cooler, until one day she discovers Tetley Infusions. One sip and all of a
sudden the world is completely different. There is a spring in her step, gifts
are being delivered by the mailboy, the photocopier guy is hot and he appears
to have a twin! Everything in the world is right.

Dashing composited this spot using Autodesk Flame to ensure seamless tracking
between the plates, clean integration of the second "Steve" and subtle ramping
to help the comedic timing.