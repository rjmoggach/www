---
title: Hp Constant Change
subtitle:
slug: hp-constant-change
date: 2004-06-01
role:
headline: directed by David Fincher, Anonymous Content
summary: Man walking through an office that is in constant change.
excerpt: Man walking through an office that is in constant change.
published: false
featured: false
categories: []
tags:
- transitions
- cloth
- CG
- tracking
- compositing
- photoreal
- photogrammetry
- environment
albums:
-   cover:
    images:
    -   caption: ''
        filename: hp-constant-change.01.jpg
    -   caption: ''
        filename: hp-constant-change.02.jpg
    -   caption: ''
        filename: hp-constant-change.03.jpg
    -   caption: ''
        filename: hp-constant-change.04.jpg
    path: work/2004/hp-constant-change/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/hp-constant-change/videos/hp-constant-change.jpg
    path: work/2004/hp-constant-change/videos/hp-constant-change
    slug: hp-constant-change
    sources:
    -   filename: hp-constant-change-1280x720.mp4
        format: mp4
        height: '720'
        size: 10475557
        width: '1280'
    -   filename: hp-constant-change-960x540.mp4
        format: mp4
        height: '540'
        size: 7156620
        width: '960'
    -   filename: hp-constant-change-640x360.mp4
        format: mp4
        height: '360'
        size: 4763464
        width: '640'
    -   filename: hp-constant-change-640x360.ogv
        format: ogv
        height: '360'
        size: 4543820
        width: '640'
    -   filename: hp-constant-change-640x360.webm
        format: webm
        height: '360'
        size: 3373604
        width: '640'
    -   filename: hp-constant-change-480x270.mp4
        format: mp4
        height: '270'
        size: 3106576
        width: '480'
    title: Hp Constant Change
credits:
-   companyName: HP
    companySlug: hp
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: David Fincher
    personSlug: david-fincher
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Man walking through an office that is in constant change.