---
title: Pepsi Grip
subtitle:
slug: pepsi-grip
date: 2004-01-15
role:
headline: directed by Stuart Douglas, @radical media
summary: High-rise workers hold on tight while passing along the Pepsi.
excerpt: High-rise workers hold on tight while passing along the Pepsi.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: pepsi-grip.01.jpg
    -   caption: ''
        filename: pepsi-grip.02.jpg
    -   caption: ''
        filename: pepsi-grip.03.jpg
    -   caption: ''
        filename: pepsi-grip.04.jpg
    path: work/2004/pepsi-grip/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/pepsi-grip/videos/pepsi-grip.jpg
    path: work/2004/pepsi-grip/videos/pepsi-grip
    slug: pepsi-grip
    sources:
    -   filename: pepsi-grip.1280x960.mp4
        format: mp4
        height: '960'
        size: 10557242
        width: '1280'
    -   filename: pepsi-grip.960x720.mp4
        format: mp4
        height: '720'
        size: 8942339
        width: '960'
    -   filename: pepsi-grip.640x480.mp4
        format: mp4
        height: '480'
        size: 5322105
        width: '640'
    -   filename: pepsi-grip.640x360.ogv
        format: ogv
        height: '360'
        size: 5223938
        width: '640'
    -   filename: pepsi-grip.640x360.webm
        format: webm
        height: '360'
        size: 4871350
        width: '640'
    -   filename: pepsi-grip.480x360.mp4
        format: mp4
        height: '360'
        size: 4032360
        width: '480'
    title: Pepsi Grip
credits:
-   companyName: Pepsi
    companySlug: pepsi
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Stuart Douglas
    personSlug: stuart-douglas
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
