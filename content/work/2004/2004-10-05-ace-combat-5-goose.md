---
title: Ace Combat 5 Goose
subtitle:
slug: ace-combat-5-goose
date: 2004-10-05
role:
headline: directed by Alain Gourrier, Digital Domain
summary: A flock of geese get a rude awakening from some fighter jets.
excerpt: A flock of geese get a rude awakening from some fighter jets.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: ace-combat-5-goose.01.jpg
    -   caption: ''
        filename: ace-combat-5-goose.02.jpg
    -   caption: ''
        filename: ace-combat-5-goose.03.jpg
    -   caption: ''
        filename: ace-combat-5-goose.04.jpg
    -   caption: ''
        filename: ace-combat-5-goose.05.jpg
    -   caption: ''
        filename: ace-combat-5-goose.06.jpg
    -   caption: ''
        filename: ace-combat-5-goose.07.jpg
    path: work/2004/ace-combat-5-goose/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2004/ace-combat-5-goose/videos/ace-combat-5-goose.jpg
    path: work/2004/ace-combat-5-goose/videos/ace-combat-5-goose
    slug: ace-combat-5-goose
    sources:
    -   filename: ace-combat-5-goose-1280x720.mp4
        format: mp4
        height: '720'
        size: 10530910
        width: '1280'
    -   filename: ace-combat-5-goose-960x540.mp4
        format: mp4
        height: '540'
        size: 7348801
        width: '960'
    -   filename: ace-combat-5-goose-640x360.ogv
        format: ogv
        height: '360'
        size: 4551217
        width: '640'
    -   filename: ace-combat-5-goose-640x360.mp4
        format: mp4
        height: '360'
        size: 4445154
        width: '640'
    -   filename: ace-combat-5-goose-640x360.webm
        format: webm
        height: '360'
        size: 3529187
        width: '640'
    -   filename: ace-combat-5-goose-480x270.mp4
        format: mp4
        height: '270'
        size: 2815085
        width: '480'
    title: Ace Combat 5 Goose
credits:
-   companyName: Tate USA
    companySlug: tate-usa
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Alain Gourrier
    personSlug: alain-gourrier
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
A flock of geese get a rude awakening from some fighter jets.