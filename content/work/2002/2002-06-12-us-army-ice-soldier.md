---
title: Us Army Ice Soldier
subtitle:
slug: us-army-ice-soldier
date: 2002-06-01
role:
headline: directed by Tony Scott, RSA Films
summary: Solitary soldier is never really alone.
excerpt: Solitary soldier is never really alone.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: us-army-ice-soldier.01.jpg
    -   caption: ''
        filename: us-army-ice-soldier.02.jpg
    -   caption: ''
        filename: us-army-ice-soldier.03.jpg
    -   caption: ''
        filename: us-army-ice-soldier.04.jpg
    -   caption: ''
        filename: us-army-ice-soldier.05.jpg
    -   caption: ''
        filename: us-army-ice-soldier.06.jpg
    -   caption: ''
        filename: us-army-ice-soldier.07.jpg
    -   caption: ''
        filename: us-army-ice-soldier.08.jpg
    -   caption: ''
        filename: us-army-ice-soldier.09.jpg
    -   caption: ''
        filename: us-army-ice-soldier.10.jpg
    path: work/2002/us-army-ice-soldier/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/us-army-ice-soldier/videos/us-army-ice-soldier.jpg
    path: work/2002/us-army-ice-soldier/videos/us-army-ice-soldier
    slug: us-army-ice-soldier
    sources:
    -   filename: us-army-ice-soldier-640x360.mp4
        format: mp4
        height: '360'
        size: 29065780
        width: '640'
    title: Us Army Ice Soldier
credits:
-   companyName: US Army
    companySlug: us-army
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Tony Scott
    personSlug: tony-scott
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
-   personName: Paul Cameron
    personSlug: paul-cameron
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
---
This commercial tells the story of the strength of the the individual US Army
Special Forces soldier and the even greater strength of his entire team. It's
an inspiring commercial set in the high rockies which has the lone soldier
scaling a massive mountain to rendezvous with his team. It's a visually
beautiful commercial in which special effects are meant to be invisible and
photo realistic. This was a great opportunity to work with international
director Tony Scott. He's a historic figure who garners much respect for his
filmmaking. As such we had to be meticulous in our execution on all fronts. I
was given the task of enhancing the supplied footage. Some shots involved
inserting the orange flag marker which as an important story point was found
to be too camouflaged in the harsh conditions. Other tasks included,
selectively brightening faces to give the soldiers more personality. The most
difficult task for this job was the actual conform. Editor Skip Chiasson is an
artist in his own right who pushes the envelope for what can be done
editorially. In this 30 second commercial there were over 100 edits!