---
title: Zurich New Dawn
subtitle:
slug: zurich-new-dawn
date: 2002-03-01
role:
headline: directed by Gerard De Thame, GDT Films
summary: When pigs fly...
excerpt: When pigs fly...
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: zurich-new-dawn.01.jpg
    -   caption: ''
        filename: zurich-new-dawn.02.jpg
    -   caption: ''
        filename: zurich-new-dawn.03.jpg
    -   caption: ''
        filename: zurich-new-dawn.04.jpg
    -   caption: ''
        filename: zurich-new-dawn.05.jpg
    -   caption: ''
        filename: zurich-new-dawn.06.jpg
    -   caption: ''
        filename: zurich-new-dawn.07.jpg
    -   caption: ''
        filename: zurich-new-dawn.08.jpg
    -   caption: ''
        filename: zurich-new-dawn.09.jpg
    -   caption: ''
        filename: zurich-new-dawn.10.jpg
    -   caption: ''
        filename: zurich-new-dawn.11.jpg
    -   caption: ''
        filename: zurich-new-dawn.12.jpg
    -   caption: ''
        filename: zurich-new-dawn.13.jpg
    -   caption: ''
        filename: zurich-new-dawn.14.jpg
    -   caption: ''
        filename: zurich-new-dawn.15.jpg
    -   caption: ''
        filename: zurich-new-dawn.16.jpg
    -   caption: ''
        filename: zurich-new-dawn.17.jpg
    -   caption: ''
        filename: zurich-new-dawn.18.jpg
    -   caption: ''
        filename: zurich-new-dawn.19.jpg
    -   caption: ''
        filename: zurich-new-dawn.20.jpg
    -   caption: ''
        filename: zurich-new-dawn.21.jpg
    -   caption: ''
        filename: zurich-new-dawn.22.jpg
    -   caption: ''
        filename: zurich-new-dawn.23.jpg
    -   caption: ''
        filename: zurich-new-dawn.24.jpg
    -   caption: ''
        filename: zurich-new-dawn.25.jpg
    -   caption: ''
        filename: zurich-new-dawn.26.jpg
    -   caption: ''
        filename: zurich-new-dawn.27.jpg
    -   caption: ''
        filename: zurich-new-dawn.28.jpg
    -   caption: ''
        filename: zurich-new-dawn.29.jpg
    -   caption: ''
        filename: zurich-new-dawn.30.jpg
    -   caption: ''
        filename: zurich-new-dawn.31.jpg
    -   caption: ''
        filename: zurich-new-dawn.32.jpg
    -   caption: ''
        filename: zurich-new-dawn.33.jpg
    path: work/2002/zurich-new-dawn/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/zurich-new-dawn/videos/zurich-new-dawn.jpg
    path: work/2002/zurich-new-dawn/videos/zurich-new-dawn
    slug: zurich-new-dawn
    sources:
    -   filename: zurich-new-dawn-320x240.mp4
        format: mp4
        height: '240'
        size: 2725555
        width: '320'
    title: Zurich New Dawn
credits:
-   companyName: Zurich Insurance
    companySlug: zurich-insurance
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: GDT Films
    companySlug: gdt-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Smoke & Mirrors
    companySlug: smoke-mirrors
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
This was a romantic depiction of a farmer in the English countryside working
dilligently to provide an ideal environment for his unique livestock of flying
pigs. It's a dramatic build-up of the preparation the landing strip and the
pigs preparing for their take-off that climaxes in their taxi and eventual
take-off into the sunset. Every shot involved some manner of special effects
to create the ideallic setting and make the pigs... fly! As mentioned above,
every shot in the commercial involved special effects. The entire piece was
shot on a massive stage location outside of London. The interior set needed to
feel like a realistic outdoor coutryside location. As a result the skies and
landscape needed to be inserted and tracked properly to integrate properly.
This was a huge task and was an aesthetic challenge as much as a technical
one. The pigs seen throughout the commercial were not trained to work as a
trio and were therefore combined from three separate passes. Because they
needed to interact in quite close proximity this was a challenge to select the
right footage from hundreds of takes and then rotoscope their motion to blend
the trio together. Finally, the aerial aerobatics the flying pigs perform were
created using entirely CGI pigs. The CGI pigs were tracked and composited into
numerous aerial plates to create a seamless photo-realistic sequence.