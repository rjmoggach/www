---
title: Hertz Clock
subtitle:
slug: hertz-clock
date: 2002-08-01
role:
headline: directed by Michael Karbelnikoff, HKM
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- matte painting
- environment
- compositing
- CG
- clouds
albums:
-   cover:
    images:
    -   caption: ''
        filename: hertz-clock.01.jpg
    -   caption: ''
        filename: hertz-clock.02.jpg
    -   caption: ''
        filename: hertz-clock.03.jpg
    -   caption: ''
        filename: hertz-clock.04.jpg
    path: work/2002/hertz-clock/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/hertz-clock/videos/hertz-clock.jpg
    path: work/2002/hertz-clock/videos/hertz-clock
    slug: hertz-clock
    sources:
    -   filename: hertz-clock.1280x960.mp4
        format: mp4
        height: '960'
        size: 11798061
        width: '1280'
    -   filename: hertz-clock.960x720.mp4
        format: mp4
        height: '720'
        size: 8341711
        width: '960'
    -   filename: hertz-clock.640x480.mp4
        format: mp4
        height: '480'
        size: 6104956
        width: '640'
    -   filename: hertz-clock.640x360.ogv
        format: ogv
        height: '360'
        size: 5270363
        width: '640'
    -   filename: hertz-clock.640x360.webm
        format: webm
        height: '360'
        size: 4687034
        width: '640'
    -   filename: hertz-clock.480x360.mp4
        format: mp4
        height: '360'
        size: 3810714
        width: '480'
    title: Hertz Clock
credits:
-   companyName: Hertz
    companySlug: hertz
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: HKM Productions
    companySlug: hkm-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Michael Karbelnikoff
    personSlug: michael-karbelnikoff
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
