---
title: The Music Take The Long Road
subtitle:
slug: the-music-take-the-long-road
date: 2002-09-01
role:
headline: directed by David Slade, RSA Films
summary: The Music is blown off their feet by an unseen force.
excerpt: The Music is blown off their feet by an unseen force.
published: false
featured: false
categories:
- Music Videos
tags:
- tracking
- rig removal
- weather
albums:
-   cover:
    images:
    -   caption: ''
        filename: the-music-take-the-long-road.01.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.02.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.03.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.04.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.05.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.06.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.07.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.08.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.09.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.10.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.11.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.12.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.13.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.14.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.15.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.16.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.17.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.18.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.19.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.20.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.21.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.22.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.23.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.24.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.25.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.26.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.27.jpg
    -   caption: ''
        filename: the-music-take-the-long-road.28.jpg
    path: work/2002/the-music-take-the-long-road/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2002/the-music-take-the-long-road/videos/the-music-take-the-long-road.jpg
    path: work/2002/the-music-take-the-long-road/videos/the-music-take-the-long-road
    slug: the-music-take-the-long-road
    sources:
    -   filename: the-music-take-the-long-road-384x288.mp4
        format: mp4
        height: '288'
        size: 14755601
        width: '384'
    title: The Music Take The Long Road
credits:
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: true
-   personName: David Slade
    personSlug: david-slade
    roleSlug: director
    roleTitle: Director
    visible: true
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: true
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
---
## The Music is blown off their feet by an unseen force.