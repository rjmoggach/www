---
title: Hp Ftd Flowers
subtitle:
slug: hp-ftd-flowers
date: 2003-08-15
role:
headline: directed by Andrew Douglas, Anonymous Content
summary: Flowers growing around the world illustrate HP's worldwide connectivity.
excerpt: Flowers growing around the world illustrate HP's worldwide connectivity.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: hp-ftd-flowers.01.jpg
    -   caption: ''
        filename: hp-ftd-flowers.02.jpg
    -   caption: ''
        filename: hp-ftd-flowers.03.jpg
    -   caption: ''
        filename: hp-ftd-flowers.04.jpg
    -   caption: ''
        filename: hp-ftd-flowers.05.jpg
    path: work/2003/hp-ftd-flowers/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/hp-ftd-flowers/videos/hp-ftd-flowers.jpg
    path: work/2003/hp-ftd-flowers/videos/hp-ftd-flowers
    slug: hp-ftd-flowers
    sources:
    -   filename: hp-ftd-flowers-1280x720.mp4
        format: mp4
        height: '720'
        size: 21106948
        width: '1280'
    -   filename: hp-ftd-flowers-960x540.mp4
        format: mp4
        height: '540'
        size: 14016838
        width: '960'
    -   filename: hp-ftd-flowers-640x360.mp4
        format: mp4
        height: '360'
        size: 8642355
        width: '640'
    -   filename: hp-ftd-flowers-640x360.ogv
        format: ogv
        height: '360'
        size: 7467766
        width: '640'
    -   filename: hp-ftd-flowers-640x360.webm
        format: webm
        height: '360'
        size: 6253443
        width: '640'
    -   filename: hp-ftd-flowers-480x270.mp4
        format: mp4
        height: '270'
        size: 5451977
        width: '480'
    title: Hp Ftd Flowers
credits:
-   companyName: HP
    companySlug: hp
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Andrew Douglas
    personSlug: andrew-douglas
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Digital Domain
    companySlug: digital-domain
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: dfx-supervisor
    roleTitle: DFX Supervisor
    visible: false
---
To tell the story of FTD's use of HP technology to speed the delivery of
international flower orders, tulips were portrayed stampeding across the world
from Europe to Asia. Massive herds of CGI tulips were composited storming
across different landscapes, wreaking a bit of havoc along the way. We had the
task of creating the massive swarms of tulips that grow across the globe. CG
artists at Digital Domain created hundreds of thousands of quickly growing
tulips and supplied the compositing team with tens of layers of elements.
Compositors then took these elements and combined them seamlessly with the
live action footage. The challenges included developing a look for the tulips
that was super-realistic. As the motion was not based in reality this was a
huge challenge. A combination of CG elements and live action dust elements was
used to blend the real and surreal. All shots were shot with a 'wild' camera
which meant heavy 3D tracking was required throughout. The 1 minute commercial
was finished with 12 bit colourspace to maximize the quality and allow us to
more easily integrate the CG elements. This in itself presented a problem
given the huge amounts of data that needed to move seamlessly between the
artists on a regular basis.