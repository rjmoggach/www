---
title: Pepsi Foot Battle
subtitle:
slug: pepsi-foot-battle
date: 2003-09-05
role:
headline: directed by Tarsem, @radical media
summary: Medieval heros and football stars save a village from Pepsi marauders.
excerpt: Medieval heros and football stars save a village from Pepsi marauders.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: pepsi-foot-battle.01.jpg
    -   caption: ''
        filename: pepsi-foot-battle.02.jpg
    -   caption: ''
        filename: pepsi-foot-battle.03.jpg
    -   caption: ''
        filename: pepsi-foot-battle.04.jpg
    -   caption: ''
        filename: pepsi-foot-battle.05.jpg
    -   caption: ''
        filename: pepsi-foot-battle.06.jpg
    -   caption: ''
        filename: pepsi-foot-battle.07.jpg
    -   caption: ''
        filename: pepsi-foot-battle.08.jpg
    -   caption: ''
        filename: pepsi-foot-battle.09.jpg
    path: work/2003/pepsi-foot-battle/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/pepsi-foot-battle/videos/pepsi-foot-battle.jpg
    path: work/2003/pepsi-foot-battle/videos/pepsi-foot-battle
    slug: pepsi-foot-battle
    sources:
    -   filename: pepsi-foot-battle.960x720.mp4
        format: mp4
        height: '720'
        size: 45407227
        width: '960'
    -   filename: pepsi-foot-battle.1280x960.mp4
        format: mp4
        height: '960'
        size: 42684452
        width: '1280'
    -   filename: pepsi-foot-battle.640x480.mp4
        format: mp4
        height: '480'
        size: 21633139
        width: '640'
    -   filename: pepsi-foot-battle.480x360.mp4
        format: mp4
        height: '360'
        size: 21510211
        width: '480'
    -   filename: pepsi-foot-battle.640x360.ogv
        format: ogv
        height: '360'
        size: 21140491
        width: '640'
    -   filename: pepsi-foot-battle.640x360.webm
        format: webm
        height: '360'
        size: 21065669
        width: '640'
    title: Pepsi Foot Battle
credits:
-   companyName: Pepsi
    companySlug: pepsi
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: '@Radical Media'
    companySlug: radical-media
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'Tarsem '
    personSlug: tarsem
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
