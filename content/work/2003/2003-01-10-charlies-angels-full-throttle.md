---
title: Charlies Angels |"Full Throttle"
subtitle: 'Full Throttle'
slug: charlies-angels-full-throttle
date: 2003-01-01
role:
headline: directed by McG, Sony Pictures
summary: The girls grapple with a car flying through the streets of LA.
excerpt: The girls grapple with a car flying through the streets of LA.
published: true
featured: false
categories:
    - Feature Films
tags:
    - automotive
albums:
    - cover:
      images:
          - caption: ''
            filename: charlies-angels-full-throttle.01.jpg
          - caption: ''
            filename: charlies-angels-full-throttle.02.jpg
          - caption: ''
            filename: charlies-angels-full-throttle.03.jpg
          - caption: ''
            filename: charlies-angels-full-throttle.04.jpg
      path: work/2003/charlies-angels-full-throttle/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2003/charlies-angels-full-throttle/videos/charlies-angels-full-throttle.jpg
      path: work/2003/charlies-angels-full-throttle/videos/charlies-angels-full-throttle
      slug: charlies-angels-full-throttle
      sources:
          - filename: charlies-angels-full-throttle-1280x720.mp4
            format: mp4
            height: '720'
            size: 29152651
            width: '1280'
          - filename: charlies-angels-full-throttle-960x540.mp4
            format: mp4
            height: '540'
            size: 24798280
            width: '960'
          - filename: charlies-angels-full-throttle-640x360.ogv
            format: ogv
            height: '360'
            size: 14794073
            width: '640'
          - filename: charlies-angels-full-throttle-640x360.mp4
            format: mp4
            height: '360'
            size: 14741613
            width: '640'
          - filename: charlies-angels-full-throttle-640x360.webm
            format: webm
            height: '360'
            size: 14733196
            width: '640'
          - filename: charlies-angels-full-throttle-480x270.mp4
            format: mp4
            height: '270'
            size: 11233028
            width: '480'
      title: Charlies Angels Full Throttle
credits:
    - personName: 'McG '
      personSlug: mcg
      roleSlug: director
      roleTitle: Director
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: false
---

For one of the climactic closing sequence of McG's most recent Charlie's
Angels film, we were given the task of creating an over-the-top car crash
involving a beatiful classic car spinning in slow motion through downtown Los
Angeles as the super crime-fighting angels are thrown off into the street. It
was a heavily choreographed sequence that required months of preparation and
planning. Thankfully, the director came to us looking for a unique approach
and wanted us to think outside of the box and be as creative as possible. Our
first task was to choreograph the action as meticulously as possible. We
created an exact 3D representation of the four blocks of downtown Los Angeles
and the car to previsualize the live action that would be necessary to create
the sequence. This accurate CG representation laid the framework for the
massive live action shoots. Once we had the footage scanned we had the task of
recreating the street scene and putting the live action crash into the
exaggerated environment. Some environments and elements including the entire
car were created in CG while others were created in flame as tracked
environments. It was a large project for a relatively small team of artists
but thoroughly enjoyable and challenging.
