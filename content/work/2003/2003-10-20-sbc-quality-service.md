---
title: Sbc Quality Service
subtitle:
slug: sbc-quality-service
date: 2003-08-01
role:
headline: directed by Andrew Douglas, Anonymous Content
summary: SBC's hardware is shown building and repairing itself without any humaninvolvment.
excerpt: SBC's hardware is shown building and repairing itself without any humaninvolvment.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: sbc-quality-service.01.jpg
    -   caption: ''
        filename: sbc-quality-service.02.jpg
    -   caption: ''
        filename: sbc-quality-service.03.jpg
    -   caption: ''
        filename: sbc-quality-service.04.jpg
    path: work/2003/sbc-quality-service/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/sbc-quality-service/videos/sbc-quality-service.jpg
    path: work/2003/sbc-quality-service/videos/sbc-quality-service
    slug: sbc-quality-service
    sources:
    -   filename: sbc-quality-service.1280x960.mp4
        format: mp4
        height: '960'
        size: 10981004
        width: '1280'
    -   filename: sbc-quality-service.960x720.mp4
        format: mp4
        height: '720'
        size: 8313378
        width: '960'
    -   filename: sbc-quality-service.640x480.mp4
        format: mp4
        height: '480'
        size: 5550743
        width: '640'
    -   filename: sbc-quality-service.640x360.ogv
        format: ogv
        height: '360'
        size: 5163474
        width: '640'
    -   filename: sbc-quality-service.640x360.webm
        format: webm
        height: '360'
        size: 3765938
        width: '640'
    -   filename: sbc-quality-service.480x360.mp4
        format: mp4
        height: '360'
        size: 3170057
        width: '480'
    title: Sbc Quality Service
credits:
-   companyName: SBC
    companySlug: sbc
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Andrew Douglas
    personSlug: andrew-douglas
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
