---
title: Halifax Bollywood
subtitle:
slug: halifax-bollywood
date: 2003-04-07
role:
headline: directed by Tarsem, @radical media
summary: Halifax Bank of Scotland tellers create a Bollywood epic.
excerpt: Halifax Bank of Scotland tellers create a Bollywood epic.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: halifax-bollywood.01.jpg
    -   caption: ''
        filename: halifax-bollywood.02.jpg
    -   caption: ''
        filename: halifax-bollywood.03.jpg
    -   caption: ''
        filename: halifax-bollywood.04.jpg
    -   caption: ''
        filename: halifax-bollywood.05.jpg
    path: work/2003/halifax-bollywood/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2003/halifax-bollywood/videos/halifax-bollywood.jpg
    path: work/2003/halifax-bollywood/videos/halifax-bollywood
    slug: halifax-bollywood
    sources:
    -   filename: halifax-bollywood-960x540.mp4
        format: mp4
        height: '540'
        size: 23277442
        width: '960'
    -   filename: halifax-bollywood-1280x720.mp4
        format: mp4
        height: '720'
        size: 20983382
        width: '1280'
    -   filename: halifax-bollywood-640x360.mp4
        format: mp4
        height: '360'
        size: 10646954
        width: '640'
    -   filename: halifax-bollywood-480x270.mp4
        format: mp4
        height: '270'
        size: 10545460
        width: '480'
    -   filename: halifax-bollywood-640x360.ogv
        format: ogv
        height: '360'
        size: 10512363
        width: '640'
    -   filename: halifax-bollywood-640x360.webm
        format: webm
        height: '360'
        size: 10476592
        width: '640'
    title: Halifax Bollywood
credits:
-   companyName: Halifax / Bank of Scotland
    companySlug: halifax-bank-scotland
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Play Productions
    companySlug: play
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
Halifax's advertising has always had a tongue in cheek humour that lends
itself to the Bollywood theme of this commercial. Director Tarsem Singh
created an epic story of British bank tellers starring in a stereotypical
Indian musical. It's a hilarious take on a classic concept that was full of
visual effects used to enhance the realism and increase the scale of the
production. I had the task of creating hundreds of dancers where there were
only tens before using multiple passes of live action. Skies were replaced and
colour corrected throughout to enhance the kitchsyness of the commercial. I
also created billboards from the live action talent that were made to look
like sloppily drawn Bollywood film covers. It was a great experience and my
first of many jobs with director Tarsem.