---
title: VH1 | The Divas "Unsilent Night"
subtitle: The Divas "Unsilent Night"
slug: vh1-divas-unsilent-night
date: 2016-12-13
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: vh1-divas-unsilent-night.01.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.02.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.03.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.04.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.05.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.06.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.07.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.08.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.09.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.10.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.11.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.12.jpg
          - caption: ''
            filename: vh1-divas-unsilent-night.13.jpg
      path: work/2016/vh1-divas-unsilent-night/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2016/vh1-divas-unsilent-night/videos/vh1-divas-unsilent-night
      slug: vh1-divas-unsilent-night
      provider: html5
      sources:
          - filename: vh1-divas-unsilent-night-1280x720.mp4
            format: mp4
            height: '720'
            size: 4003891
            width: '1280'
          - filename: vh1-divas-unsilent-night-960x540.mp4
            format: mp4
            height: '540'
            size: 3965448
            width: '960'
          - filename: vh1-divas-unsilent-night-640x360.mp4
            format: mp4
            height: '360'
            size: 3927955
            width: '640'
          - filename: vh1-divas-unsilent-night-480x270.mp4
            format: mp4
            height: '270'
            size: 3917399
            width: '480'
          - filename: vh1-divas-unsilent-night-640x360.ogv
            format: ogv
            height: '360'
            size: 3772261
            width: '640'
          - filename: vh1-divas-unsilent-night-640x360.webm
            format: webm
            height: '360'
            size: 1331653
            width: '640'
      title: VH1 Divas "Unsilent Night"
credits: []
---
