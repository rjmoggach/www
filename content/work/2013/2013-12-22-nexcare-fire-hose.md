---
title: Nexcare Fire Hose
subtitle:
slug: nexcare-fire-hose
date: 2013-12-24
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: nexcare-fire-hose.01.jpg
    -   caption: ''
        filename: nexcare-fire-hose.02.jpg
    -   caption: ''
        filename: nexcare-fire-hose.03.jpg
    -   caption: ''
        filename: nexcare-fire-hose.04.jpg
    -   caption: ''
        filename: nexcare-fire-hose.05.jpg
    -   caption: ''
        filename: nexcare-fire-hose.06.jpg
    path: work/2013/nexcare-fire-hose/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/nexcare-fire-hose/videos/nexcare-fire-hose.jpg
    path: work/2013/nexcare-fire-hose/videos/nexcare-fire-hose
    slug: nexcare-fire-hose
    sources:
    -   filename: nexcare-firehose-dirTitles-1280x720.mp4
        format: mp4
        height: '720'
        size: 7316455
        width: '1280'
    -   filename: nexcare-firehose-dirTitles-960x540.mp4
        format: mp4
        height: '540'
        size: 5403535
        width: '960'
    -   filename: nexcare-firehose-dirTitles-640x360.ogv
        format: ogv
        height: '360'
        size: 4077908
        width: '640'
    -   filename: nexcare-firehose-dirTitles-640x360.webm
        format: webm
        height: '360'
        size: 3739956
        width: '640'
    -   filename: nexcare-firehose-dirTitles-640x360.mp4
        format: mp4
        height: '360'
        size: 3454442
        width: '640'
    -   filename: nexcare-firehose-dirTitles-480x270.mp4
        format: mp4
        height: '270'
        size: 2332710
        width: '480'
    title: Nexcare Fire Hose
credits: []
---
