---
title: Scion Slide
subtitle:
slug: scion-slide
date: 2013-11-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: scion-slide.01.jpg
    -   caption: ''
        filename: scion-slide.02.jpg
    -   caption: ''
        filename: scion-slide.03.jpg
    -   caption: ''
        filename: scion-slide.04.jpg
    -   caption: ''
        filename: scion-slide.05.jpg
    path: work/2013/scion-slide/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/scion-slide/videos/scion-slide.jpg
    path: work/2013/scion-slide/videos/scion-slide
    slug: scion-slide
    sources:
    -   filename: scion-slide-1280x720.mp4
        format: mp4
        height: '720'
        size: 5680435
        width: '1280'
    -   filename: scion-slide-960x540.mp4
        format: mp4
        height: '540'
        size: 4644167
        width: '960'
    -   filename: scion-slide-640x360.ogv
        format: ogv
        height: '360'
        size: 3771475
        width: '640'
    -   filename: scion-slide-640x360.webm
        format: webm
        height: '360'
        size: 3573914
        width: '640'
    -   filename: scion-slide-640x360.mp4
        format: mp4
        height: '360'
        size: 3210911
        width: '640'
    -   filename: scion-slide-480x270.mp4
        format: mp4
        height: '270'
        size: 2292621
        width: '480'
    title: Scion Slide
credits: []
---
