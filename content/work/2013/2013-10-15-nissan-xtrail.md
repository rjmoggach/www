---
title: Nissan Xtrail
subtitle:
slug: nissan-xtrail
date: 2013-10-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- automotive
albums:
-   cover:
    images:
    -   caption: ''
        filename: nissan-xtrail.01.jpg
    -   caption: ''
        filename: nissan-xtrail.02.jpg
    -   caption: ''
        filename: nissan-xtrail.03.jpg
    -   caption: ''
        filename: nissan-xtrail.04.jpg
    -   caption: ''
        filename: nissan-xtrail.05.jpg
    -   caption: ''
        filename: nissan-xtrail.06.jpg
    -   caption: ''
        filename: nissan-xtrail.07.jpg
    -   caption: ''
        filename: nissan-xtrail.08.jpg
    -   caption: ''
        filename: nissan-xtrail.09.jpg
    -   caption: ''
        filename: nissan-xtrail.10.jpg
    path: work/2013/nissan-xtrail/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/nissan-xtrail/videos/nissan-xtrail.jpg
    path: work/2013/nissan-xtrail/videos/nissan-xtrail
    slug: nissan-xtrail
    sources:
    -   filename: nissan-xtrail-1280x720.mp4
        format: mp4
        height: '720'
        size: 15432763
        width: '1280'
    -   filename: nissan-xtrail-960x540.mp4
        format: mp4
        height: '540'
        size: 10753304
        width: '960'
    -   filename: nissan-xtrail-640x360.webm
        format: webm
        height: '360'
        size: 6200417
        width: '640'
    -   filename: nissan-xtrail-640x360.mp4
        format: mp4
        height: '360'
        size: 6157720
        width: '640'
    -   filename: nissan-xtrail-640x360.ogv
        format: ogv
        height: '360'
        size: 6104634
        width: '640'
    -   filename: nissan-xtrail-480x270.mp4
        format: mp4
        height: '270'
        size: 3942010
        width: '480'
    title: Nissan Xtrail
credits: []
---
