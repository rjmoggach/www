---
title: Levis Made Of Progress
subtitle:
slug: levis-made-of-progress
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: levis-made-of-progress.01.jpg
    -   caption: ''
        filename: levis-made-of-progress.02.jpg
    -   caption: ''
        filename: levis-made-of-progress.03.jpg
    -   caption: ''
        filename: levis-made-of-progress.04.jpg
    -   caption: ''
        filename: levis-made-of-progress.05.jpg
    path: work/2013/levis-made-of-progress/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/levis-made-of-progress/videos/levis-made-of-progress.jpg
    path: work/2013/levis-made-of-progress/videos/levis-made-of-progress
    slug: levis-made-of-progress
    sources:
    -   filename: levis-made-of-progress-1280x720.mp4
        format: mp4
        height: '720'
        size: 22212668
        width: '1280'
    -   filename: levis-made-of-progress-960x540.mp4
        format: mp4
        height: '540'
        size: 16564655
        width: '960'
    -   filename: levis-made-of-progress-640x360.mp4
        format: mp4
        height: '360'
        size: 9733450
        width: '640'
    -   filename: levis-made-of-progress-640x360.ogv
        format: ogv
        height: '360'
        size: 8941210
        width: '640'
    -   filename: levis-made-of-progress-640x360.webm
        format: webm
        height: '360'
        size: 8194699
        width: '640'
    -   filename: levis-made-of-progress-480x270.mp4
        format: mp4
        height: '270'
        size: 6192175
        width: '480'
    title: Levis Made Of Progress
credits: []
---
