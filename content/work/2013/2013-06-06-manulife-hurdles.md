---
title: Manulife Hurdles
subtitle:
slug: manulife-hurdles
date: 2013-06-01
role:
headline: ''
summary: directed by Bo Platt, Corner Store, for DDB Canada
excerpt: directed by Bo Platt, Corner Store, for DDB Canada
published: false
featured: false
categories:
- Advertising
tags:
- colour
- compositing
- environment
- matte painting
- flares
- surreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: manulife-hurdles.01.jpg
    -   caption: ''
        filename: manulife-hurdles.02.jpg
    -   caption: ''
        filename: manulife-hurdles.03.jpg
    -   caption: ''
        filename: manulife-hurdles.04.jpg
    -   caption: ''
        filename: manulife-hurdles.05.jpg
    -   caption: ''
        filename: manulife-hurdles.06.jpg
    -   caption: ''
        filename: manulife-hurdles.07.jpg
    -   caption: ''
        filename: manulife-hurdles.08.jpg
    -   caption: ''
        filename: manulife-hurdles.09.jpg
    -   caption: ''
        filename: manulife-hurdles.10.jpg
    path: work/2013/manulife-hurdles/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2013/manulife-hurdles/videos/manulife-hurdles.jpg
    path: work/2013/manulife-hurdles/videos/manulife-hurdles
    slug: manulife-hurdles
    sources:
    -   filename: manulife-hurdles-1280x720.mp4
        format: mp4
        height: '720'
        size: 4251940
        width: '1280'
    -   filename: manulife-hurdles-960x540.mp4
        format: mp4
        height: '540'
        size: 3275521
        width: '960'
    -   filename: manulife-hurdles-640x360.ogv
        format: ogv
        height: '360'
        size: 2307326
        width: '640'
    -   filename: manulife-hurdles-640x360.mp4
        format: mp4
        height: '360'
        size: 2215595
        width: '640'
    -   filename: manulife-hurdles-640x360.webm
        format: webm
        height: '360'
        size: 1673436
        width: '640'
    -   filename: manulife-hurdles-480x270.mp4
        format: mp4
        height: '270'
        size: 1613188
        width: '480'
    title: Manulife Hurdles
credits:
-   companyName: The Corner Store
    companySlug: corner-store
    personName: Bo Platt
    personSlug: bo-platt
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Roman Vasyanov
    personSlug: roman-vasyanov
    roleSlug: dp
    roleTitle: Director of Photography
    visible: true
-   companyName: Rooster
    companySlug: rooster
    personName: Marc Langley
    personSlug: marc-langley
    roleSlug: editor
    roleTitle: Editor
    visible: true
-   companyName: Dashing Collective
    companySlug: dshng
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: true
-   companyName: Dashing Collective
    companySlug: dshng
    personName: Mary Anne Ledesma
    personSlug: mary-anne-ledesma
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: true
-   companyName: Dashing Collective
    companySlug: dshng
    personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: true
---
