---
title: Chick Fil A Rooftop
subtitle:
slug: chick-fil-a-rooftop
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: chick-fil-a-rooftop.01.jpg
    -   caption: ''
        filename: chick-fil-a-rooftop.02.jpg
    -   caption: ''
        filename: chick-fil-a-rooftop.03.jpg
    -   caption: ''
        filename: chick-fil-a-rooftop.04.jpg
    -   caption: ''
        filename: chick-fil-a-rooftop.05.jpg
    -   caption: ''
        filename: chick-fil-a-rooftop.06.jpg
    path: work/2007/chick-fil-a-rooftop/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2007/chick-fil-a-rooftop/videos/chick-fil-a-rooftop.jpg
    path: work/2007/chick-fil-a-rooftop/videos/chick-fil-a-rooftop
    slug: chick-fil-a-rooftop
    sources:
    -   filename: chick-fil-a-rooftop-1280x720.mp4
        format: mp4
        height: '720'
        size: 10380260
        width: '1280'
    -   filename: chick-fil-a-rooftop-960x540.mp4
        format: mp4
        height: '540'
        size: 5948361
        width: '960'
    -   filename: chick-fil-a-rooftop-640x360.mp4
        format: mp4
        height: '360'
        size: 4503932
        width: '640'
    -   filename: chick-fil-a-rooftop-640x360.ogv
        format: ogv
        height: '360'
        size: 4228282
        width: '640'
    -   filename: chick-fil-a-rooftop-480x270.mp4
        format: mp4
        height: '270'
        size: 2963455
        width: '480'
    -   filename: chick-fil-a-rooftop-640x360.webm
        format: webm
        height: '360'
        size: 2793305
        width: '640'
    title: Chick Fil A Rooftop
credits: []
---
