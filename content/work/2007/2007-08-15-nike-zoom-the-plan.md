---
title: Nike Zoom | "The Plan"
subtitle: 'The Plan'
slug: nike-zoom-the-plan
date: 2007-08-15
role:
headline: directed by Michael Mann, Alturas Films
summary: Ladainian Tomlinson sees the play before it happens.
excerpt: Ladainian Tomlinson sees the play before it happens.
published: true
featured: true
categories:
    - Advertising
tags:
    - sports
    - crowds
    - environment
    - compositing
    - set extensions
    - tracking
albums:
    - cover:
      images:
          - caption: ''
            filename: nike-zoom-the-plan.01.jpg
          - caption: ''
            filename: nike-zoom-the-plan.02.jpg
          - caption: ''
            filename: nike-zoom-the-plan.03.jpg
          - caption: ''
            filename: nike-zoom-the-plan.04.jpg
          - caption: ''
            filename: nike-zoom-the-plan.05.jpg
          - caption: ''
            filename: nike-zoom-the-plan.06.jpg
      path: work/2007/nike-zoom-the-plan/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2007/nike-zoom-the-plan/videos/nike-zoom-the-plan.jpg
      path: work/2007/nike-zoom-the-plan/videos/nike-zoom-the-plan
      slug: nike-zoom-the-plan
      sources:
          - filename: nike-zoom-the-plan-960x540.mp4
            format: mp4
            height: '540'
            size: 23298685
            width: '960'
          - filename: nike-zoom-the-plan-1280x720.mp4
            format: mp4
            height: '720'
            size: 21020232
            width: '1280'
          - filename: nike-zoom-the-plan-640x360.mp4
            format: mp4
            height: '360'
            size: 10668137
            width: '640'
          - filename: nike-zoom-the-plan-640x360.ogv
            format: ogv
            height: '360'
            size: 10500385
            width: '640'
          - filename: nike-zoom-the-plan-640x360.webm
            format: webm
            height: '360'
            size: 10239513
            width: '640'
          - filename: nike-zoom-the-plan-480x270.mp4
            format: mp4
            height: '270'
            size: 8774489
            width: '480'
      title: Nike Zoom The Plan
credits:
    - companyName: Nike
      companySlug: nike
      roleSlug: client
      roleTitle: Client
      visible: false
    - companyName: Wieden + Kennedy
      companySlug: wk
      roleSlug: agency
      roleTitle: Advertising Agency
      visible: false
    - companyName: Alturas Films
      companySlug: alturas-films
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Michael Mann
      personSlug: michael-mann
      roleSlug: director
      roleTitle: Director
      visible: false
    - companyName: Asylum Visual Effects
      companySlug: asylumfx
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: false
    - personName: Mark Kurtz
      personSlug: mark-kurtz
      roleSlug: vfx-producer
      roleTitle: VFX Producer
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: false
    - personName: Sean Faden
      personSlug: sean-faden
      roleSlug: cg-sup
      roleTitle: CG Supervisor
      visible: false
    - personName: Denis Gauthier
      personSlug: denis-gauthier
      roleSlug: cg-lighting
      roleTitle: CG Lighting
      visible: false
---

Ladainian Tomlinson sees the play before it happens.
