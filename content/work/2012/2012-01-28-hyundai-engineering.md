---
title: Hyundai | "Engineering"
subtitle: Engineering
slug: hyundai-engineering
date: 2012-01-28
role: 
headline: ''
summary: Dashing shows off the miraculous inner-engineering of Hyundai's vehicles
  as the cars cruise around a massive interior test track in a new five-spot campaign
  for Innocean Worldwide.
excerpt: 
published: true
featured: true
categories:
- Advertising
tags:
- automotive
- VFX
- CG
- animation
- effects
- environment
- compositing
- design
- photoreal
- colour
albums:
- cover: 
  images:
  - caption: ''
    filename: hyundai-engineering.01.jpg
  - caption: ''
    filename: hyundai-engineering.02.jpg
  - caption: ''
    filename: hyundai-engineering.03.jpg
  - caption: ''
    filename: hyundai-engineering.04.jpg
  - caption: ''
    filename: hyundai-engineering.05.jpg
  - caption: ''
    filename: hyundai-engineering.06.jpg
  - caption: ''
    filename: hyundai-engineering.07.jpg
  - caption: ''
    filename: hyundai-engineering.08.jpg
  - caption: ''
    filename: hyundai-engineering.09.jpg
  - caption: ''
    filename: hyundai-engineering.10.jpg
  path: work/2012/hyundai-engineering/images/stills
  slug: stills
  title: Stills
videos:
- caption: ''
  cover: work/2012/hyundai-engineering/videos/hyundai-engineering-elantra.jpg
  path: work/2012/hyundai-engineering/videos/hyundai-engineering-elantra
  slug: hyundai-engineering-elantra
  sources:
  - filename: hyundai-engineering-elantra-1280x720.mp4
    format: mp4
    height: '720'
    size: 19031774
    width: '1280'
  - filename: hyundai-engineering-elantra-960x540.mp4
    format: mp4
    height: '540'
    size: 5703444
    width: '960'
  - filename: hyundai-engineering-elantra-640x360.ogv
    format: ogv
    height: '360'
    size: 4713988
    width: '640'
  - filename: hyundai-engineering-elantra-640x360.webm
    format: webm
    height: '360'
    size: 3876143
    width: '640'
  - filename: hyundai-engineering-elantra-640x360.mp4
    format: mp4
    height: '360'
    size: 3849311
    width: '640'
  - filename: hyundai-engineering-elantra-480x270.mp4
    format: mp4
    height: '270'
    size: 2681913
    width: '480'
  title: Hyundai Engineering Elantra
- caption: ''
  cover: work/2012/hyundai-engineering/videos/hyundai-engineering-genesis.jpg
  path: work/2012/hyundai-engineering/videos/hyundai-engineering-genesis
  slug: hyundai-engineering-genesis
  sources:
  - filename: hyundai-engineering-genesis-1280x720.mp4
    format: mp4
    height: '720'
    size: 20551872
    width: '1280'
  - filename: hyundai-engineering-genesis-960x540.mp4
    format: mp4
    height: '540'
    size: 5498524
    width: '960'
  - filename: hyundai-engineering-genesis-640x360.ogv
    format: ogv
    height: '360'
    size: 3994677
    width: '640'
  - filename: hyundai-engineering-genesis-640x360.webm
    format: webm
    height: '360'
    size: 3598579
    width: '640'
  - filename: hyundai-engineering-genesis-640x360.mp4
    format: mp4
    height: '360'
    size: 3391863
    width: '640'
  - filename: hyundai-engineering-genesis-480x270.mp4
    format: mp4
    height: '270'
    size: 2305406
    width: '480'
  title: Hyundai Engineering Genesis
- caption: ''
  cover: work/2012/hyundai-engineering/videos/hyundai-engineering-sonata-hybrid.jpg
  path: work/2012/hyundai-engineering/videos/hyundai-engineering-sonata-hybrid
  slug: hyundai-engineering-sonata-hybrid
  sources:
  - filename: hyundai-engineering-sonata-hybrid-1280x720.mp4
    format: mp4
    height: '720'
    size: 19214729
    width: '1280'
  - filename: hyundai-engineering-sonata-hybrid-960x540.mp4
    format: mp4
    height: '540'
    size: 4275439
    width: '960'
  - filename: hyundai-engineering-sonata-hybrid-640x360.ogv
    format: ogv
    height: '360'
    size: 3837392
    width: '640'
  - filename: hyundai-engineering-sonata-hybrid-640x360.webm
    format: webm
    height: '360'
    size: 3081293
    width: '640'
  - filename: hyundai-engineering-sonata-hybrid-640x360.mp4
    format: mp4
    height: '360'
    size: 2968463
    width: '640'
  - filename: hyundai-engineering-sonata-hybrid-480x270.mp4
    format: mp4
    height: '270'
    size: 2121372
    width: '480'
  title: Hyundai Engineering Sonata Hybrid
- caption: ''
  cover: work/2012/hyundai-engineering/videos/hyundai-engineering-tucson.jpg
  path: work/2012/hyundai-engineering/videos/hyundai-engineering-tucson
  slug: hyundai-engineering-tucson
  sources:
  - filename: hyundai-engineering-tucson-1280x720.mp4
    format: mp4
    height: '720'
    size: 18865888
    width: '1280'
  - filename: hyundai-engineering-tucson-960x540.mp4
    format: mp4
    height: '540'
    size: 5319915
    width: '960'
  - filename: hyundai-engineering-tucson-640x360.ogv
    format: ogv
    height: '360'
    size: 4322242
    width: '640'
  - filename: hyundai-engineering-tucson-640x360.webm
    format: webm
    height: '360'
    size: 3632871
    width: '640'
  - filename: hyundai-engineering-tucson-640x360.mp4
    format: mp4
    height: '360'
    size: 3489322
    width: '640'
  - filename: hyundai-engineering-tucson-480x270.mp4
    format: mp4
    height: '270'
    size: 2436575
    width: '480'
  title: Hyundai Engineering Tucson
- caption: ''
  cover: work/2012/hyundai-engineering/videos/hyundai-engineering-veloster.jpg
  path: work/2012/hyundai-engineering/videos/hyundai-engineering-veloster
  slug: hyundai-engineering-veloster
  sources:
  - filename: hyundai-engineering-veloster-1280x720.mp4
    format: mp4
    height: '720'
    size: 18395818
    width: '1280'
  - filename: hyundai-engineering-veloster-960x540.mp4
    format: mp4
    height: '540'
    size: 7215117
    width: '960'
  - filename: hyundai-engineering-veloster-640x360.ogv
    format: ogv
    height: '360'
    size: 6001515
    width: '640'
  - filename: hyundai-engineering-veloster-640x360.webm
    format: webm
    height: '360'
    size: 4992743
    width: '640'
  - filename: hyundai-engineering-veloster-640x360.mp4
    format: mp4
    height: '360'
    size: 4740272
    width: '640'
  - filename: hyundai-engineering-veloster-480x270.mp4
    format: mp4
    height: '270'
    size: 3253608
    width: '480'
  title: Hyundai Engineering Veloster
credits:
- companyName: Hyundai Canada
  companySlug: hyundai-canada
  roleSlug: client
  roleTitle: Client
  visible: false
  role: Client
- companyName: Innocean Worldwide Canada
  companySlug: innocean-worldwide-canada
  roleSlug: agency
  roleTitle: Advertising Agency
  visible: false
  role: Agency
- personName: Gary Westgate
  personSlug: gary-westgate
  roleSlug: agency-cd
  roleTitle: Creative Director
  visible: false
  role: "--Agency/Executive Creative Director"
- personName: Gary Holme
  personSlug: gary-holme
  roleSlug: agency-art-director
  roleTitle: Art Director
  visible: false
  role: "--Agency/Art Director"
- personName: Nelson Quintal
  personSlug: nelson-quintal
  roleSlug: agency-copywriter
  roleTitle: Copywriter
  visible: false
  role: "--Agency/Copywriter"
- personName: Alina Prussky
  personSlug: alina-prussky
  roleSlug: agency-producer
  roleTitle: Agency Producer
  visible: false
  role: "--Agency/Senior Producer"
- companyName: Radke / MJZ
  companySlug: radke-mjz
  roleSlug: production
  roleTitle: Production Company
  visible: false
  role: Production
- personName: Rob Leggatt
  personSlug: rob-leggatt
  roleSlug: director
  roleTitle: Director
  visible: false
  role: "--Production/Director"
- personName: Claudio Miranda
  personSlug: claudio-miranda
  roleSlug: dp
  roleTitle: Director of Photography
  visible: false
  role: "--Production/Cinematographer"
- personName: Scott Mackenzie
  personSlug: scott-mackenzie
  roleSlug: edit-ep
  roleTitle: Executive Producer
  visible: false
- personName: Mark Hall
  personSlug: mark-hall
  roleSlug: line-producer
  roleTitle: Line Producer
  visible: false
- companyName: School Editing
  companySlug: school-editing
  roleSlug: edit
  roleTitle: Editorial
  visible: false
- personName: Chris Van Dyke
  personSlug: chris-van-dyke
  roleSlug: editor
  roleTitle: Editor
  visible: false
- personName: Sarah Brooks
  personSlug: sarah-brooks
  roleSlug: edit-ep
  roleTitle: Executive Producer
  visible: false
- companyName: Dashing Collective
  companySlug: dshng
  roleSlug: vfx
  roleTitle: Visual Effects
  visible: false
- personName: Robert Moggach
  personSlug: robert-moggach
  roleSlug: vfx-supervisor
  roleTitle: VFX Supervisor
  visible: false
- personName: Mary Anne Ledesma
  personSlug: mary-anne-ledesma
  roleSlug: vfx-producer
  roleTitle: VFX Producer
  visible: false
- personName: Sebastian Bilbao
  personSlug: sebastian-bilbao
  roleSlug: cg-sup
  roleTitle: CG Supervisor
  visible: false
- personName: Steward Burris
  personSlug: steward-burris
  roleSlug: anim-sup
  roleTitle: Animation Supervisor
  visible: false
- personName: Scott Metzger
  personSlug: scott-metzger
  roleSlug: cg-lighting
  roleTitle: CG Lighting
  visible: false

---
Dashing shows off the miraculous inner-engineering of Hyundai's vehicles as
the cars cruise around a massive interior test track in a new five-spot
campaign for Innocean Worldwide. Our bespoke approach to Visual Effects
brought together some of the best talent from around the globe to develop and
execute a gargantuan multi-disciplined effort.

Each spot features a Hyundai vehicle cruising through a surreally lit
postmodern test facility of giant proportions as intricate graphics showing
off the automobiles' machinations spring onto the screen. Veloster displays
the car's dual-clutch eco-shift system as it travels through a sheet of liquid
light, Sonata shows off the car's hybrid technology as it travels thousands of
miles, and Tucson examines the inner workings of the SUV's suspension as it
glides over a bumpy road. The vehicles travel towards a light and, as though
appearing in another dimension, emerge from a suburban garage door, as
narrator Jeff Bridges, summarizes the journey, "Just one of the engineering
advances we've brought from our test facility to yours."

It was inspiring to create something conceptually strong that needed to be
real and functional, without seeming too technical, sci-fi or overly designed,
as many car spots can be. Director Rob Leggatt (Radke Films/MJZ) brought
together an impressive team to design the foundation of this world we were
creating. Production Designer John Beard (Brazil, Last Temptation of Christ)
and Director of Photography Claudio Miranda (Life of Pi, Tron, Benjamin
Button) confidently set the tone early on. Dashing was responsible for
creating the entire world around these cars. Having a truly filmic foundation
meant the CG needed to match that natural, cinematic energy.

Rather than hunker down in a technology bunker with a massive team to push out
the heavy-duty effects, we employ a more dynamic, agile approach by uniting a
highly-skilled onsite team of digital craftsmen with similarly skilled artists
working remotely around the world. There's alot of hype about this new
paradigm of VFX production, but the reality is there hasn't been a real
working example in advertising and certainly not at this scale. Small teams of
creative experts truly can deliver incredible results if you give them the
right tools. This project distilled the fact that exceptional, creative visual
effects depend on the individual artists, the digital craftsmen &mdash; not a big
established brand. We were fortunate to have our dream team, assembled from
years of experience working worldwide.

Faced with a tight timeframe for a large project, we knew we would have to
streamline the process. Previs and storyboarding took place simultaneously in
studio and on location and continued during the seven day shoot. We had
dedicated tracking and previs teams on set. Editor Chris Van Dyke (School) was
with us as well. Because we shot digitally, we were able to have edits
approved and tracking and rotoscoping started within days and even hours of
the shoot. The team was assembled from artists worldwide based on their
individual expertise and as needed we brought them to the production to
guarantee a smooth collaboration. From plates to delivery, we delivered almost
100 shots of full CGI environments, design and animation in 8 weeks.