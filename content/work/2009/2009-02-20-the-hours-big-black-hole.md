---
title: The Hours Big Black Hole
subtitle:
slug: the-hours-big-black-hole
date: 2009-02-20
role:
headline: directed by Tony Kaye
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: the-hours-big-black-hole.01.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.02.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.03.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.04.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.05.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.06.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.07.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.08.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.09.jpg
    -   caption: ''
        filename: the-hours-big-black-hole.10.jpg
    path: work/2009/the-hours-big-black-hole/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/the-hours-big-black-hole/videos/the-hours-big-black-hole.jpg
    path: work/2009/the-hours-big-black-hole/videos/the-hours-big-black-hole
    slug: the-hours-big-black-hole
    sources:
    -   filename: the-hours-big-black-hole-384x216.mp4
        format: mp4
        height: '216'
        size: 10872907
        width: '384'
    title: The Hours Big Black Hole
credits:
-   personName: Tony Kaye
    personSlug: tony-kaye
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Method Studios
    companySlug: method-studios
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
