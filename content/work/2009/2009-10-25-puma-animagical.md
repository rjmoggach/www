---
title: Puma Animagical
subtitle:
slug: puma-animagical
date: 2009-10-25
role:
headline: directed by Hype Williams
summary: Usain Bolt dancing with hip hop video starlett as their clothes fall off
    asliquid.
excerpt: Usain Bolt dancing with hip hop video starlett as their clothes fall off
    asliquid.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- clothing
- CG
- photoreal
- fluids
- liquids
- cloth
albums:
-   cover:
    images:
    -   caption: ''
        filename: puma-animagical.01.jpg
    -   caption: ''
        filename: puma-animagical.02.jpg
    -   caption: ''
        filename: puma-animagical.03.jpg
    -   caption: ''
        filename: puma-animagical.04.jpg
    -   caption: ''
        filename: puma-animagical.05.jpg
    -   caption: ''
        filename: puma-animagical.06.jpg
    path: work/2009/puma-animagical/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/puma-animagical/videos/puma-animagical.jpg
    path: work/2009/puma-animagical/videos/puma-animagical
    slug: puma-animagical
    sources:
    -   filename: puma-animagical-960x540.mp4
        format: mp4
        height: '540'
        size: 5807280
        width: '960'
    -   filename: puma-animagical-1280x720.mp4
        format: mp4
        height: '720'
        size: 5114029
        width: '1280'
    -   filename: puma-animagical-640x360.mp4
        format: mp4
        height: '360'
        size: 2667162
        width: '640'
    -   filename: puma-animagical-640x360.ogv
        format: ogv
        height: '360'
        size: 2652401
        width: '640'
    -   filename: puma-animagical-640x360.webm
        format: webm
        height: '360'
        size: 2636741
        width: '640'
    -   filename: puma-animagical-480x270.mp4
        format: mp4
        height: '270'
        size: 2535104
        width: '480'
    title: Puma Animagical
credits:
-   companyName: Puma
    companySlug: puma
    roleSlug: client
    roleTitle: Client
    visible: false
-   personName: Hype Williams
    personSlug: hype-williams
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
Usain Bolt dancing with hip hop video starlett as their clothes fall off as
liquid.