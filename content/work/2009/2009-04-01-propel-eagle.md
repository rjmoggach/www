---
title: Propel Eagle
subtitle:
slug: propel-eagle
date: 2009-04-01
role:
headline: directed by Andrew Douglas, Anonymous Content
summary: an eagle forms from water floating languidly through a cityscape
excerpt: an eagle forms from water floating languidly through a cityscape
published: false
featured: false
categories:
- Advertising
tags:
- food
- VFX
- compositing
- photoreal
- liquids
- CG
albums:
-   cover:
    images:
    -   caption: ''
        filename: propel-eagle.01.jpg
    -   caption: ''
        filename: propel-eagle.02.jpg
    -   caption: ''
        filename: propel-eagle.03.jpg
    -   caption: ''
        filename: propel-eagle.04.jpg
    path: work/2009/propel-eagle/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2009/propel-eagle/videos/propel-eagle.jpg
    path: work/2009/propel-eagle/videos/propel-eagle
    slug: propel-eagle
    sources:
    -   filename: propel-eagle-960x540.mp4
        format: mp4
        height: '540'
        size: 11871406
        width: '960'
    -   filename: propel-eagle-1280x720.mp4
        format: mp4
        height: '720'
        size: 10705322
        width: '1280'
    -   filename: propel-eagle-640x360.mp4
        format: mp4
        height: '360'
        size: 5433474
        width: '640'
    -   filename: propel-eagle-640x360.ogv
        format: ogv
        height: '360'
        size: 5248432
        width: '640'
    -   filename: propel-eagle-640x360.webm
        format: webm
        height: '360'
        size: 5228279
        width: '640'
    -   filename: propel-eagle-480x270.mp4
        format: mp4
        height: '270'
        size: 4878443
        width: '480'
    title: Propel Eagle
credits:
-   companyName: Propel
    companySlug: propel
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Anonymous Content
    companySlug: anonymous-content
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Andrew Douglas
    personSlug: andrew-douglas
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Method Studios
    companySlug: method-studios
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Alex Frisch
    personSlug: alex-frisch
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: inferno-artist
    roleTitle: Inferno Artist
    visible: false
---
an eagle forms from water floating languidly through a cityscape