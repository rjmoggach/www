---
title: Sony Ps3 White Room
subtitle:
slug: sony-ps3-white-room
date: 2006-10-01
role:
headline: directed by Rupert Sanders, MJZ
summary: Toy doll cries for mama PlayStation.
excerpt: Toy doll cries for mama PlayStation.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- CG
- characters
- compositing
- video game
albums:
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-baby.01.jpg
    -   caption: ''
        filename: sony-ps3-white-room-baby.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-baby.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-baby.04.jpg
    -   caption: ''
        filename: sony-ps3-white-room-baby.05.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-baby-stills
    slug: sony-ps3-white-room-baby-stills
    title: Sony Ps3 White Room Baby Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-drip.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-drip.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-drip.04.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-drip-stills
    slug: sony-ps3-white-room-drip-stills
    title: Sony Ps3 White Room Drip Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-eggs.01.jpg
    -   caption: ''
        filename: sony-ps3-white-room-eggs.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-eggs.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-eggs.04.jpg
    -   caption: ''
        filename: sony-ps3-white-room-eggs.05.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-eggs-stills
    slug: sony-ps3-white-room-eggs-stills
    title: Sony Ps3 White Room Eggs Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-motorstorm.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-motorstorm.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-motorstorm.04.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-motorstorm-stills
    slug: sony-ps3-white-room-motorstorm-stills
    title: Sony Ps3 White Room Motorstorm Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-plant.01.jpg
    -   caption: ''
        filename: sony-ps3-white-room-plant.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-plant.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-plant.04.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-plant-stills
    slug: sony-ps3-white-room-plant-stills
    title: Sony Ps3 White Room Plant Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-resistance.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-resistance.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-resistance.04.jpg
    -   caption: ''
        filename: sony-ps3-white-room-resistance.05.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-resistance-stills
    slug: sony-ps3-white-room-resistance-stills
    title: Sony Ps3 White Room Resistance Stills
-   cover:
    images:
    -   caption: ''
        filename: sony-ps3-white-room-rubik.01.jpg
    -   caption: ''
        filename: sony-ps3-white-room-rubik.02.jpg
    -   caption: ''
        filename: sony-ps3-white-room-rubik.03.jpg
    -   caption: ''
        filename: sony-ps3-white-room-rubik.04.jpg
    -   caption: ''
        filename: sony-ps3-white-room-rubik.05.jpg
    -   caption: ''
        filename: sony-ps3-white-room-rubik.06.jpg
    path: work/2006/sony-ps3-white-room/images/sony-ps3-white-room-rubik-stills
    slug: sony-ps3-white-room-rubik-stills
    title: Sony Ps3 White Room Rubik Stills
videos:
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-baby.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-baby
    slug: sony-ps3-white-room-baby
    sources:
    -   filename: sony-ps3-white-room-baby-1280x720.mp4
        format: mp4
        height: '720'
        size: 8377241
        width: '1280'
    -   filename: sony-ps3-white-room-baby-960x540.mp4
        format: mp4
        height: '540'
        size: 3794523
        width: '960'
    -   filename: sony-ps3-white-room-baby-640x360.mp4
        format: mp4
        height: '360'
        size: 1687206
        width: '640'
    -   filename: sony-ps3-white-room-baby-640x360.ogv
        format: ogv
        height: '360'
        size: 1443259
        width: '640'
    -   filename: sony-ps3-white-room-baby-640x360.webm
        format: webm
        height: '360'
        size: 1019611
        width: '640'
    -   filename: sony-ps3-white-room-baby-480x270.mp4
        format: mp4
        height: '270'
        size: 974813
        width: '480'
    title: Sony Ps3 White Room Baby
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-drip.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-drip
    slug: sony-ps3-white-room-drip
    sources:
    -   filename: sony-ps3-white-room-drip-1280x720.mp4
        format: mp4
        height: '720'
        size: 10605523
        width: '1280'
    -   filename: sony-ps3-white-room-drip-960x540.mp4
        format: mp4
        height: '540'
        size: 9624017
        width: '960'
    -   filename: sony-ps3-white-room-drip-640x360.mp4
        format: mp4
        height: '360'
        size: 5403545
        width: '640'
    -   filename: sony-ps3-white-room-drip-640x360.ogv
        format: ogv
        height: '360'
        size: 5226753
        width: '640'
    -   filename: sony-ps3-white-room-drip-640x360.webm
        format: webm
        height: '360'
        size: 4742692
        width: '640'
    -   filename: sony-ps3-white-room-drip-480x270.mp4
        format: mp4
        height: '270'
        size: 3303458
        width: '480'
    title: Sony Ps3 White Room Drip
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-eggs.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-eggs
    slug: sony-ps3-white-room-eggs
    sources:
    -   filename: sony-ps3-white-room-eggs-1280x720.mp4
        format: mp4
        height: '720'
        size: 8390772
        width: '1280'
    -   filename: sony-ps3-white-room-eggs-960x540.mp4
        format: mp4
        height: '540'
        size: 4043960
        width: '960'
    -   filename: sony-ps3-white-room-eggs-640x360.mp4
        format: mp4
        height: '360'
        size: 1951315
        width: '640'
    -   filename: sony-ps3-white-room-eggs-640x360.ogv
        format: ogv
        height: '360'
        size: 1880133
        width: '640'
    -   filename: sony-ps3-white-room-eggs-640x360.webm
        format: webm
        height: '360'
        size: 1309197
        width: '640'
    -   filename: sony-ps3-white-room-eggs-480x270.mp4
        format: mp4
        height: '270'
        size: 1164995
        width: '480'
    title: Sony Ps3 White Room Eggs
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-motorstorm.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-motorstorm
    slug: sony-ps3-white-room-motorstorm
    sources:
    -   filename: sony-ps3-white-room-motorstorm-1280x720.mp4
        format: mp4
        height: '720'
        size: 10305402
        width: '1280'
    -   filename: sony-ps3-white-room-motorstorm-960x540.mp4
        format: mp4
        height: '540'
        size: 9155778
        width: '960'
    -   filename: sony-ps3-white-room-motorstorm-640x360.mp4
        format: mp4
        height: '360'
        size: 5305612
        width: '640'
    -   filename: sony-ps3-white-room-motorstorm-640x360.ogv
        format: ogv
        height: '360'
        size: 5228177
        width: '640'
    -   filename: sony-ps3-white-room-motorstorm-640x360.webm
        format: webm
        height: '360'
        size: 4399768
        width: '640'
    -   filename: sony-ps3-white-room-motorstorm-480x270.mp4
        format: mp4
        height: '270'
        size: 3257899
        width: '480'
    title: Sony Ps3 White Room Motorstorm
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-plant.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-plant
    slug: sony-ps3-white-room-plant
    sources:
    -   filename: sony-ps3-white-room-plant-1280x720.mp4
        format: mp4
        height: '720'
        size: 10695970
        width: '1280'
    -   filename: sony-ps3-white-room-plant-960x540.mp4
        format: mp4
        height: '540'
        size: 7513463
        width: '960'
    -   filename: sony-ps3-white-room-plant-640x360.mp4
        format: mp4
        height: '360'
        size: 3919838
        width: '640'
    -   filename: sony-ps3-white-room-plant-640x360.ogv
        format: ogv
        height: '360'
        size: 3720510
        width: '640'
    -   filename: sony-ps3-white-room-plant-640x360.webm
        format: webm
        height: '360'
        size: 3061022
        width: '640'
    -   filename: sony-ps3-white-room-plant-480x270.mp4
        format: mp4
        height: '270'
        size: 2401817
        width: '480'
    title: Sony Ps3 White Room Plant
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-resistance.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-resistance
    slug: sony-ps3-white-room-resistance
    sources:
    -   filename: sony-ps3-white-room-resistance-1280x720.mp4
        format: mp4
        height: '720'
        size: 10379060
        width: '1280'
    -   filename: sony-ps3-white-room-resistance-960x540.mp4
        format: mp4
        height: '540'
        size: 7844071
        width: '960'
    -   filename: sony-ps3-white-room-resistance-640x360.mp4
        format: mp4
        height: '360'
        size: 4397182
        width: '640'
    -   filename: sony-ps3-white-room-resistance-640x360.ogv
        format: ogv
        height: '360'
        size: 4210313
        width: '640'
    -   filename: sony-ps3-white-room-resistance-640x360.webm
        format: webm
        height: '360'
        size: 3257679
        width: '640'
    -   filename: sony-ps3-white-room-resistance-480x270.mp4
        format: mp4
        height: '270'
        size: 2733554
        width: '480'
    title: Sony Ps3 White Room Resistance
-   caption: ''
    cover: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-rubik.jpg
    path: work/2006/sony-ps3-white-room/videos/sony-ps3-white-room-rubik
    slug: sony-ps3-white-room-rubik
    sources:
    -   filename: sony-ps3-white-room-rubik-1280x720.mp4
        format: mp4
        height: '720'
        size: 7470455
        width: '1280'
    -   filename: sony-ps3-white-room-rubik-960x540.mp4
        format: mp4
        height: '540'
        size: 3404755
        width: '960'
    -   filename: sony-ps3-white-room-rubik-640x360.mp4
        format: mp4
        height: '360'
        size: 1633885
        width: '640'
    -   filename: sony-ps3-white-room-rubik-640x360.ogv
        format: ogv
        height: '360'
        size: 1449388
        width: '640'
    -   filename: sony-ps3-white-room-rubik-640x360.webm
        format: webm
        height: '360'
        size: 1005363
        width: '640'
    -   filename: sony-ps3-white-room-rubik-480x270.mp4
        format: mp4
        height: '270'
        size: 931549
        width: '480'
    title: Sony Ps3 White Room Rubik
credits:
-   companyName: Sony
    companySlug: sony
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Rupert Sanders
    personSlug: rupert-sanders
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
An eerie little doll comes alive and is overwhelmed by all that the
PlayStation 3 has to offer. Most of this spot was in camera. We did alot of
cleanup, added in reflections of the game footage, and made the PS3 levitate
off the ground for the end tag.