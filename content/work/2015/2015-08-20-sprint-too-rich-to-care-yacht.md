---
title: Sprint
subtitle: Too Rich To Care "Yacht"
slug: sprint-too-rich-to-care-yacht
date: 2015-08-20
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: sprint-too-rich-to-care-yacht.01.jpg
          - caption: ''
            filename: sprint-too-rich-to-care-yacht.02.jpg
          - caption: ''
            filename: sprint-too-rich-to-care-yacht.03.jpg
          - caption: ''
            filename: sprint-too-rich-to-care-yacht.04.jpg
          - caption: ''
            filename: sprint-too-rich-to-care-yacht.05.jpg
      path: work/2015/sprint-too-rich-to-care-yacht/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/sprint-too-rich-to-care-yacht/videos/sprint-too-rich-to-care-yacht.jpg
      path: work/2015/sprint-too-rich-to-care-yacht/videos/sprint-too-rich-to-care-yacht
      slug: sprint-too-rich-to-care-yacht
      provider: html5
      sources:
          - filename: sprint-too-rich-to-care-yacht-1280x720.mp4
            format: mp4
            height: '720'
            size: 7955030
            width: '1280'
          - filename: sprint-too-rich-to-care-yacht-960x540.mp4
            format: mp4
            height: '540'
            size: 6327716
            width: '960'
          - filename: sprint-too-rich-to-care-yacht-640x360.webm
            format: webm
            height: '360'
            size: 5432373
            width: '640'
          - filename: sprint-too-rich-to-care-yacht-640x360.ogv
            format: ogv
            height: '360'
            size: 5430749
            width: '640'
          - filename: sprint-too-rich-to-care-yacht-640x360.mp4
            format: mp4
            height: '360'
            size: 4270745
            width: '640'
          - filename: sprint-too-rich-to-care-yacht-480x270.mp4
            format: mp4
            height: '270'
            size: 2943878
            width: '480'
      title: Sprint Too Rich To Care Yacht
credits: []
---
