---
title: Carls Jr.
subtitle: Ice Hotel
slug: carls-jr-ice-hotel
date: 2015-06-05
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: carls-jr-ice-hotel.01.jpg
          - caption: ''
            filename: carls-jr-ice-hotel.02.jpg
          - caption: ''
            filename: carls-jr-ice-hotel.03.jpg
          - caption: ''
            filename: carls-jr-ice-hotel.04.jpg
          - caption: ''
            filename: carls-jr-ice-hotel.05.jpg
      path: work/2015/carls-jr-ice-hotel/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2015/carls-jr-ice-hotel/videos/carls-jr-ice-hotel-before
      slug: carls-jr-ice-hotel-before
      provider: html5
      sources:
          - filename: carls-jr-ice-hotel-before-1280x720.mp4
            format: mp4
            height: '720'
            size: 1408148
            width: '1280'
          - filename: carls-jr-ice-hotel-before-960x540.mp4
            format: mp4
            height: '540'
            size: 1077581
            width: '960'
          - filename: carls-jr-ice-hotel-before-480x270.ogv
            format: ogv
            height: '270'
            size: 749818
            width: '480'
          - filename: carls-jr-ice-hotel-before-640x360.mp4
            format: mp4
            height: '360'
            size: 679557
            width: '640'
          - filename: carls-jr-ice-hotel-before-640x360.webm
            format: webm
            height: '360'
            size: 662767
            width: '640'
          - filename: carls-jr-ice-hotel-before-480x270.mp4
            format: mp4
            height: '270'
            size: 418255
            width: '480'
      title: Carls Jr Ice Hotel Before
credits:
    - companyName: JAMM
      companySlug: jamm
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Asher Edwards
      personSlug: asher-edwards
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Jake Montgomery
      personSlug: jake-montgomery
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Andy Boyd
      personSlug: andy-boyd
      roleSlug: cg-sup
      roleTitle: CG Supervisor
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
---
