---
title: Casio | G-Shock "S-Series"
subtitle: G-Shock "S-Series"
slug: casio-gshock
date: 2015-09-01
role:
headline: Beautifully organic hexagonal deconstruction and reconstruction of Casio
    Women's G-Shock line.
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - CG
    - VFX
    - animation
    - CG
    - effects
    - design
    - compositing
    - colour
albums:
    - cover:
      images:
          - caption: ''
            filename: casio-gshock.01.jpg
          - caption: ''
            filename: casio-gshock.02.jpg
          - caption: ''
            filename: casio-gshock.03.jpg
          - caption: ''
            filename: casio-gshock.04.jpg
          - caption: ''
            filename: casio-gshock.05.jpg
          - caption: ''
            filename: casio-gshock.06.jpg
          - caption: ''
            filename: casio-gshock.07.jpg
          - caption: ''
            filename: casio-gshock.08.jpg
          - caption: ''
            filename: casio-gshock.09.jpg
          - caption: ''
            filename: casio-gshock.10.jpg
          - caption: ''
            filename: casio-gshock.11.jpg
          - caption: ''
            filename: casio-gshock.12.jpg
      path: work/2015/casio-gshock/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/casio-gshock/videos/casio-gshock.jpg
      path: work/2015/casio-gshock/videos/casio-gshock
      slug: casio-gshock
      provider: html5
      sources:
          - filename: casio-gshock-1920x1080.mp4
            format: mp4
            height: '1080'
            size: 9212732
            width: '1920'
          - filename: casio-gshock-1280x720.mp4
            format: mp4
            height: '720'
            size: 9212732
            width: '1280'
          - filename: casio-gshock-640x360.webm
            format: webm
            height: '360'
            size: 4784371
            width: '640'
          - filename: casio-gshock-640x360.mp4
            format: mp4
            height: '360'
            size: 4467261
            width: '640'
          - filename: casio-gshock-640x360.ogv
            format: ogv
            height: '360'
            size: 4305591
            width: '640'
          - filename: casio-gshock-480x270.mp4
            format: mp4
            height: '270'
            size: 3215199
            width: '480'
      title: Casio Gshock
credits:
    - companyName: Logan
      companySlug: logan
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: Logan
      companySlug: logan
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: Logan
      companySlug: logan
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - companyName: Logan
      companySlug: logan
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: colorist
      roleTitle: Colorist
      visible: true
---

A beautiful spot showcasing Casio's newest line of G-Shock S Series watches for women. I was brought on by Logan to lead the team as VFX Supervisor in collaboration with Design Director Ara Devejian. Photoreal was the goal for this macro style CGI so the focus was Nuke from the onset with Flame for final finish.
