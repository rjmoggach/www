---
title: Toyota | "Journey"
subtitle: 'Journey'
slug: toyota-journey
date: 2015-07-11
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags:
    - effects
    - colour
    - compositing
    - set extensions
    - VFX
    - automotive
albums:
    - cover:
      images:
          - caption: ''
            filename: toyota-journey.01.jpg
          - caption: ''
            filename: toyota-journey.02.jpg
          - caption: ''
            filename: toyota-journey.03.jpg
          - caption: ''
            filename: toyota-journey.04.jpg
          - caption: ''
            filename: toyota-journey.05.jpg
          - caption: ''
            filename: toyota-journey.06.jpg
      path: work/2015/toyota-journey/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2015/toyota-journey/images/stills/toyota-journey.01.jpg
      path: work/2015/toyota-journey/videos/toyota-journey
      provider: html5
      slug: toyota-journey
      sources:
          - filename: toyota-journey-1280x720.mp4
            format: mp4
            height: '720'
            size: 5689533
            width: '1280'
          - filename: toyota-journey-960x540.mp4
            format: mp4
            height: '540'
            size: 4309605
            width: '960'
          - filename: toyota-journey-640x360.mp4
            format: mp4
            height: '360'
            size: 2826893
            width: '640'
          - filename: toyota-journey-640x360.ogv
            format: ogv
            height: '360'
            size: 2592979
            width: '640'
          - filename: toyota-journey-640x360.webm
            format: webm
            height: '360'
            size: 2546560
            width: '640'
          - filename: toyota-journey-480x270.mp4
            format: mp4
            height: '270'
            size: 1945878
            width: '480'
      title: Toyota Journey
credits:
    - companyName: The Mission
      companySlug: mission
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Michael Pardee
      personSlug: michael-pardee
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Ryan Meredith
      personSlug: ryan-meredith
      roleSlug: vfx-producer
      roleTitle: VFX Producer
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: David Stern
      personSlug: david-stern
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
    - companyName: The Mission
      companySlug: mission
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: colorist
      roleTitle: Colorist
      visible: true
---
