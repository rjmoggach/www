---
title: Prada Thunder Perfect Mind
subtitle:
slug: prada-thunder-perfect-mind
date: 2005-06-02
role:
headline: directed by Jordan Scott & Ridley Scott, RSA Films
summary:
excerpt:
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- fashion
albums:
-   cover:
    images:
    -   caption: ''
        filename: prada-thunder-perfect-mind.01.jpg
    -   caption: ''
        filename: prada-thunder-perfect-mind.02.jpg
    -   caption: ''
        filename: prada-thunder-perfect-mind.03.jpg
    -   caption: ''
        filename: prada-thunder-perfect-mind.04.jpg
    -   caption: ''
        filename: prada-thunder-perfect-mind.05.jpg
    -   caption: ''
        filename: prada-thunder-perfect-mind.06.jpg
    path: work/2005/prada-thunder-perfect-mind/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/prada-thunder-perfect-mind/videos/prada-thunder-perfect-mind.jpg
    path: work/2005/prada-thunder-perfect-mind/videos/prada-thunder-perfect-mind
    slug: prada-thunder-perfect-mind
    sources:
    -   filename: prada-thunder-perfect-mind-1280x720.mp4
        format: mp4
        height: '720'
        size: 83822520
        width: '1280'
    -   filename: prada-thunder-perfect-mind-960x540.mp4
        format: mp4
        height: '540'
        size: 53699486
        width: '960'
    -   filename: prada-thunder-perfect-mind-640x360.mp4
        format: mp4
        height: '360'
        size: 34863227
        width: '640'
    -   filename: prada-thunder-perfect-mind-640x360.ogv
        format: ogv
        height: '360'
        size: 32928490
        width: '640'
    -   filename: prada-thunder-perfect-mind-480x270.mp4
        format: mp4
        height: '270'
        size: 23160973
        width: '480'
    -   filename: prada-thunder-perfect-mind-640x360.webm
        format: webm
        height: '360'
        size: 22323278
        width: '640'
    title: Prada Thunder Perfect Mind
credits:
-   companyName: RSA Films
    companySlug: rsa-films
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Ridley Scott
    personSlug: ridley-scott
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Jordan Scott
    personSlug: jordan-scott
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
