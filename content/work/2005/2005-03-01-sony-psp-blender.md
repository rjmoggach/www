---
title: Sony Psp Blender
subtitle:
slug: sony-psp-blender
date: 2005-03-01
role:
headline: directed by Alain Gourrier, Digital Domain
summary: Man walking down the street showcases the all-emcompassing gamut the PSPprovides.
excerpt: Man walking down the street showcases the all-emcompassing gamut the PSPprovides.
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: sony-psp-blender.01.jpg
    -   caption: ''
        filename: sony-psp-blender.02.jpg
    -   caption: ''
        filename: sony-psp-blender.03.jpg
    -   caption: ''
        filename: sony-psp-blender.04.jpg
    -   caption: ''
        filename: sony-psp-blender.05.jpg
    -   caption: ''
        filename: sony-psp-blender.06.jpg
    path: work/2005/sony-psp-blender/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/sony-psp-blender/videos/sony-psp-blender.jpg
    path: work/2005/sony-psp-blender/videos/sony-psp-blender
    slug: sony-psp-blender
    sources:
    -   filename: sony-psp-blender-1280x720.mp4
        format: mp4
        height: '720'
        size: 10294707
        width: '1280'
    -   filename: sony-psp-blender-960x540.mp4
        format: mp4
        height: '540'
        size: 6878930
        width: '960'
    -   filename: sony-psp-blender-640x360.ogv
        format: ogv
        height: '360'
        size: 4952626
        width: '640'
    -   filename: sony-psp-blender-640x360.mp4
        format: mp4
        height: '360'
        size: 4371926
        width: '640'
    -   filename: sony-psp-blender-640x360.webm
        format: webm
        height: '360'
        size: 3422858
        width: '640'
    -   filename: sony-psp-blender-480x270.mp4
        format: mp4
        height: '270'
        size: 2945045
        width: '480'
    title: Sony Psp Blender
credits:
-   companyName: Sony
    companySlug: sony
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Tate USA
    companySlug: tate-usa
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Alain Gourrier
    personSlug: alain-gourrier
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
Man walking down the street showcases the all-emcompassing gamut the PSP
provides.