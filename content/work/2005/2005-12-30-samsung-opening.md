---
title: Samsung Opening
subtitle:
slug: samsung-opening
date: 2005-12-30
role:
headline: directed by Gerard De Thame, HSI
summary: Samsung phone owners salute the Olympic torch.
excerpt: Samsung phone owners salute the Olympic torch.
published: false
featured: false
categories:
- Advertising
tags:
- crowds
- environment
- set extensions
- tracking
- cellular
albums:
-   cover:
    images:
    -   caption: ''
        filename: samsung-opening.01.jpg
    -   caption: ''
        filename: samsung-opening.02.jpg
    -   caption: ''
        filename: samsung-opening.03.jpg
    -   caption: ''
        filename: samsung-opening.04.jpg
    -   caption: ''
        filename: samsung-opening.05.jpg
    -   caption: ''
        filename: samsung-opening.06.jpg
    path: work/2005/samsung-opening/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2005/samsung-opening/videos/samsung-opening.jpg
    path: work/2005/samsung-opening/videos/samsung-opening
    slug: samsung-opening
    sources:
    -   filename: samsung-opening-1280x720.mp4
        format: mp4
        height: '720'
        size: 24676989
        width: '1280'
    -   filename: samsung-opening-960x540.mp4
        format: mp4
        height: '540'
        size: 19645123
        width: '960'
    -   filename: samsung-opening-640x360.mp4
        format: mp4
        height: '360'
        size: 12332232
        width: '640'
    -   filename: samsung-opening-640x360.ogv
        format: ogv
        height: '360'
        size: 11382824
        width: '640'
    -   filename: samsung-opening-640x360.webm
        format: webm
        height: '360'
        size: 9110190
        width: '640'
    -   filename: samsung-opening-480x270.mp4
        format: mp4
        height: '270'
        size: 7689572
        width: '480'
    title: Samsung Opening
credits:
-   companyName: Samsung
    companySlug: samsung
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: HSI Productions
    companySlug: hsi-productions
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Gerard de Thame
    personSlug: gerard-de-thame
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Asylum Visual Effects
    companySlug: asylumfx
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Sean Faden
    personSlug: sean-faden
    roleSlug: cg-sup
    roleTitle: CG Supervisor
    visible: false
---
Samsung phone owners salute the Olympic torch.