---
title: Rice Krispies Now Were Talking
subtitle:
slug: rice-krispies-now-were-talking
date: 2000-12-15
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: rice-krispies-dino.01.jpg
    -   caption: ''
        filename: rice-krispies-dino.02.jpg
    -   caption: ''
        filename: rice-krispies-dino.03.jpg
    -   caption: ''
        filename: rice-krispies-dino.04.jpg
    path: work/2005/rice-krispies-now-were-talking/images/rice-krispies-dino-stills
    slug: rice-krispies-dino-stills
    title: Rice Krispies Dino Stills
-   cover:
    images:
    -   caption: ''
        filename: rice-krispies-meadow.01.jpg
    -   caption: ''
        filename: rice-krispies-meadow.02.jpg
    -   caption: ''
        filename: rice-krispies-meadow.03.jpg
    -   caption: ''
        filename: rice-krispies-meadow.04.jpg
    -   caption: ''
        filename: rice-krispies-meadow.05.jpg
    path: work/2005/rice-krispies-now-were-talking/images/rice-krispies-meadow-stills
    slug: rice-krispies-meadow-stills
    title: Rice Krispies Meadow Stills
videos:
-   caption: ''
    cover: work/2005/rice-krispies-now-were-talking/videos/rice-krispies-dino.jpg
    path: work/2005/rice-krispies-now-were-talking/videos/rice-krispies-dino
    slug: rice-krispies-dino
    sources:
    -   filename: rice-krispies-dino-1280x720.mp4
        format: mp4
        height: '720'
        size: 10390563
        width: '1280'
    -   filename: rice-krispies-dino-960x540.mp4
        format: mp4
        height: '540'
        size: 9054333
        width: '960'
    -   filename: rice-krispies-dino-640x360.ogv
        format: ogv
        height: '360'
        size: 5306098
        width: '640'
    -   filename: rice-krispies-dino-640x360.mp4
        format: mp4
        height: '360'
        size: 5286814
        width: '640'
    -   filename: rice-krispies-dino-640x360.webm
        format: webm
        height: '360'
        size: 4869710
        width: '640'
    -   filename: rice-krispies-dino-480x270.mp4
        format: mp4
        height: '270'
        size: 3707360
        width: '480'
    title: Rice Krispies Dino
-   caption: ''
    cover: work/2005/rice-krispies-now-were-talking/videos/rice-krispies-meadow.jpg
    path: work/2005/rice-krispies-now-were-talking/videos/rice-krispies-meadow
    slug: rice-krispies-meadow
    sources:
    -   filename: rice-krispies-meadow-1280x720.mp4
        format: mp4
        height: '720'
        size: 10450768
        width: '1280'
    -   filename: rice-krispies-meadow-960x540.mp4
        format: mp4
        height: '540'
        size: 9279219
        width: '960'
    -   filename: rice-krispies-meadow-640x360.mp4
        format: mp4
        height: '360'
        size: 5331659
        width: '640'
    -   filename: rice-krispies-meadow-640x360.ogv
        format: ogv
        height: '360'
        size: 5318258
        width: '640'
    -   filename: rice-krispies-meadow-640x360.webm
        format: webm
        height: '360'
        size: 5159693
        width: '640'
    -   filename: rice-krispies-meadow-480x270.mp4
        format: mp4
        height: '270'
        size: 3947344
        width: '480'
    title: Rice Krispies Meadow
credits: []
---
