---
title: The Test | Titles
subtitle: Titles
slug:
date: 2000-09-01
role:
headline:
summary:
excerpt:
published: true
featured: true
categories:
    - Feature Films
tags:
    - Design
    - Title Design
    - Compositing
albums:
    - cover:
      images:
          - caption: ''
            filename: the-test-titles.01.jpg
          - caption: ''
            filename: the-test-titles.02.jpg
          - caption: ''
            filename: the-test-titles.03.jpg
          - caption: ''
            filename: the-test-titles.04.jpg
          - caption: ''
            filename: the-test-titles.05.jpg
          - caption: ''
            filename: the-test-titles.06.jpg
          - caption: ''
            filename: the-test-titles.07.jpg
          - caption: ''
            filename: the-test-titles.08.jpg
          - caption: ''
            filename: the-test-titles.09.jpg
          - caption: ''
            filename: the-test-titles.10.jpg
          - caption: ''
            filename: the-test-titles.11.jpg
          - caption: ''
            filename: the-test-titles.12.jpg
          - caption: ''
            filename: the-test-titles.13.jpg
      path: work/2000/the-test-titles/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/the-test-titles/videos/the-test-titles.jpg
      path: work/2000/the-test-titles/videos/the-test-titles
      slug: the-test-titles
      sources:
          - filename: the-test-titles.960x540.mp4
            format: mp4
            height: '540'
            size: 31848718
            width: '960'
          - filename: the-test-titles.1280x720.mp4
            format: mp4
            height: '720'
            size: 28654295
            width: '1280'
          - filename: the-test-titles.640x360.mp4
            format: mp4
            height: '360'
            size: 13801725
            width: '640'
          - filename: the-test-titles.640x360.ogv
            format: ogv
            height: '360'
            size: 11487413
            width: '640'
          - filename: the-test-titles.640x360.webm
            format: webm
            height: '360'
            size: 10980673
            width: '640'
          - filename: the-test-titles.480x270.mp4
            format: mp4
            height: '270'
            size: 10472234
            width: '480'
      title: The Test Titles
credits: []
---

I was given complete creative freedom to design / direct a title sequence for
this dark short film about military genetic testing on inmates, After having
read the script for the film and watched an early cut, I developed the feel
for this sequence. I decided that it needed to involve elements that would
depict the human body being attacked by cellular organisms and disease. The
use of X-ray images, microscopic footage and organic textures was a must. I
also wanted the environment these elements were used in to have a connection,
even abstract, to these elements. For this I used massive scans of
fingerprints moving in and out of light in Inferno. The elements were then
combined a visual bed to which type could be integrated. The type had to
maintain the organic nature of this visual bed but also had to convey the dark
and sinister message of the film. I designed the initial type with a computer
but the final bears little resemblance to this element. Once designed, the
type was treated by outputting to an ink jet printer, running the fresh print
under water, crumpling, photo-copying then scanning and cleaning in Photoshop.
This resulting distressed type was brought into inferno and animated to the
audio track to create a piece that was thoroughly organic in feeling and
conception.
