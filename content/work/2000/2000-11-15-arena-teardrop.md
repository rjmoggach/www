---
title: Arena |"Teardrop"
subtitle:
slug: arena-teardrop
date: 2000-11-15
role:
headline: ''
summary:
excerpt:
published: true
featured: true
categories:
    - Advertising
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: arena-teardrop.01.jpg
          - caption: ''
            filename: arena-teardrop.02.jpg
          - caption: ''
            filename: arena-teardrop.03.jpg
      path: work/2000/arena-teardrop/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2000/arena-teardrop/videos/arena-teardrop.jpg
      path: work/2000/arena-teardrop/videos/arena-teardrop
      slug: arena-teardrop
      sources:
          - filename: arena-teardrop.1280x720.mp4
            format: mp4
            height: '720'
            size: 12414762
            width: '1280'
          - filename: arena-teardrop.960x540.mp4
            format: mp4
            height: '540'
            size: 6991491
            width: '960'
          - filename: arena-teardrop.640x360.mp4
            format: mp4
            height: '360'
            size: 3476026
            width: '640'
          - filename: arena-teardrop.640x360.ogv
            format: ogv
            height: '360'
            size: 3200703
            width: '640'
          - filename: arena-teardrop.640x360.webm
            format: webm
            height: '360'
            size: 2105993
            width: '640'
          - filename: arena-teardrop.480x270.mp4
            format: mp4
            height: '270'
            size: 2021977
            width: '480'
      title: Arena Teardrop
credits: []
---

For this commercial we had to create a photo-realistic tear drop on the face
of a medal winning athlete. The tear was shot in multiple sections on a
mannequin and tracked onto the contours of the athlete's face, neck and chest.
The biggest challenges were the tracking itself, creating realistic highlights
and reflections, the transitions between different tear elements, warping the
tear to match the contours accurately, and matching the various different
focus points in the shot to create one seamless 20 second shot.
