---
title: Lexus LF1
subtitle: Borderless
slug: lexus-borderless
date: 2017-12-30
role:
headline: ''
summary:
excerpt:
published: true
featured: false
categories:
    - Advertising
tags:
    - Automotive
albums:
    - cover:
      images:
          - caption: ''
            filename: lexus-borderless.01.jpg
          - caption: ''
            filename: lexus-borderless.02.jpg
          - caption: ''
            filename: lexus-borderless.03.jpg
          - caption: ''
            filename: lexus-borderless.04.jpg
          - caption: ''
            filename: lexus-borderless.05.jpg
          - caption: ''
            filename: lexus-borderless.06.jpg
          - caption: ''
            filename: lexus-borderless.07.jpg
          - caption: ''
            filename: lexus-borderless.08.jpg
          - caption: ''
            filename: lexus-borderless.09.jpg
          - caption: ''
            filename: lexus-borderless.10.jpg
          - caption: ''
            filename: lexus-borderless.11.jpg
          - caption: ''
            filename: lexus-borderless.12.jpg
          - caption: ''
            filename: lexus-borderless.13.jpg
          - caption: ''
            filename: lexus-borderless.14.jpg
          - caption: ''
            filename: lexus-borderless.15.jpg
          - caption: ''
            filename: lexus-borderless.16.jpg
          - caption: ''
            filename: lexus-borderless.17.jpg
          - caption: ''
            filename: lexus-borderless.18.jpg
          - caption: ''
            filename: lexus-borderless.19.jpg
          - caption: ''
            filename: lexus-borderless.20.jpg
          - caption: ''
            filename: lexus-borderless.21.jpg
          - caption: ''
            filename: lexus-borderless.22.jpg
          - caption: ''
            filename: lexus-borderless.23.jpg
          - caption: ''
            filename: lexus-borderless.24.jpg
          - caption: ''
            filename: lexus-borderless.25.jpg
          - caption: ''
            filename: lexus-borderless.26.jpg
      path: work/2017/lexus-borderless/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: ''
      path: work/2017/lexus-borderless/videos/lexus-borderless
      slug: lexus-borderless
      provider: html5
      sources:
          - filename: lexus-borderless-1280x720.mp4
            format: mp4
            height: '720'
            size:
            width: '1280'
          - filename: lexus-borderless-960x540.mp4
            format: mp4
            height: '540'
            size:
            width: '960'
          - filename: lexus-borderless-480x270.ogv
            format: ogv
            height: '270'
            size:
            width: '480'
          - filename: lexus-borderless-640x360.mp4
            format: mp4
            height: '360'
            size:
            width: '640'
          - filename: lexus-borderless-640x360.webm
            format: webm
            height: '360'
            size:
            width: '640'
          - filename: lexus-borderless-480x270.mp4
            format: mp4
            height: '270'
            size:
            width: '480'
      title: Lexus "Borderless"
credits:
    - companyName: JAMM
      companySlug: jamm
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Asher Edwards
      personSlug: asher-edwards
      roleSlug: vfx-ep
      roleTitle: Executive Producer
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Jake Montgomery
      personSlug: jake-montgomery
      roleSlug: vfx-supervisor
      roleTitle: VFX Supervisor
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Andy Boyd
      personSlug: andy-boyd
      roleSlug: cg-sup
      roleTitle: CG Supervisor
      visible: true
    - companyName: JAMM
      companySlug: jamm
      personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: true
---

Executed flawlessly by friends at JAMM Visual in Santa Monica, on this film I had the privelege of supporting the hard-working team as Flame and Nuke Compositing Lead alongside Creative Director Jake Montgomery.
