---
title: Drew Lightfoot Animation Test
subtitle:
slug: drew-lightfoot-animation-test
date: 2010-08-02
role:
headline: directed by Drew Lightfoot
summary: Just a fun little light projection project we collaborated on with SoftCitizen's,
    Drew Lightfoot.
excerpt: Just a fun little light projection project we collaborated on with SoftCitizen's,
    Drew Lightfoot.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- animation
- projection
albums:
-   cover:
    images:
    -   caption: ''
        filename: drew-lightfoot-animation-test.01.jpg
    -   caption: ''
        filename: drew-lightfoot-animation-test.02.jpg
    -   caption: ''
        filename: drew-lightfoot-animation-test.03.jpg
    -   caption: ''
        filename: drew-lightfoot-animation-test.04.jpg
    path: work/2010/drew-lightfoot-animation-test/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2010/drew-lightfoot-animation-test/videos/drew-lightfoot-animation-test.jpg
    path: work/2010/drew-lightfoot-animation-test/videos/drew-lightfoot-animation-test
    slug: drew-lightfoot-animation-test
    sources:
    -   filename: drew-lightfoot-animation-test-1280x720.mp4
        format: mp4
        height: '720'
        size: 17318113
        width: '1280'
    -   filename: drew-lightfoot-animation-test-960x540.mp4
        format: mp4
        height: '540'
        size: 3110853
        width: '960'
    -   filename: drew-lightfoot-animation-test-640x360.mp4
        format: mp4
        height: '360'
        size: 2090344
        width: '640'
    -   filename: drew-lightfoot-animation-test-640x360.ogv
        format: ogv
        height: '360'
        size: 1835008
        width: '640'
    -   filename: drew-lightfoot-animation-test-640x360.webm
        format: webm
        height: '360'
        size: 1712182
        width: '640'
    -   filename: drew-lightfoot-animation-test-480x270.mp4
        format: mp4
        height: '270'
        size: 1432493
        width: '480'
    title: Drew Lightfoot Animation Test
credits:
-   companyName: Soft Citizen
    companySlug: soft-citizen
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Drew Lightfoot
    personSlug: drew-lightfoot
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Link York
    personSlug: link-york
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: flame
    roleTitle: Flame Artist
    visible: false
---
Just a fun & quirky little light projection project we collaborated on with
Soft Citizen's, Drew Lightfoot.