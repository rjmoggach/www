---
title: Fedex Change
subtitle:
slug: fedex-change
date: 2010-10-06
role:
headline: directed by Tim Godsall, OPC for BBDO
summary: In this award winning spot, Dashing recently showcased their diversity andnimble
    nature in a spot for FedEx, shot by Director Tim Godsall out of BBDOToronto. _Change_
    stars an effortlessly competent businessexecutive/surgeon/tattoo artist/chess
    master/sculptor who orders one of hisoffice drones to use FedEx for a non-urgent
    shipment. When the assistantsecond-guesses his boss, explaining his belief that
    FedEx is only good forurgent shipping needs, he is met with a stern explanation
    that it's possiblebe good at many things.
excerpt: In this award winning spot, Dashing recently showcased their diversity andnimble
    nature in a spot for FedEx, shot by Director Tim Godsall out of BBDOToronto. _Change_
    stars an effortlessly competent businessexecutive/surgeon/tattoo artist/chess
    master/sculptor who orders one of hisoffice drones to use FedEx for a non-urgent
    shipment. When the assistantsecond-guesses his boss, explaining his belief that
    FedEx is only good forurgent shipping needs, he is met with a stern explanation
    that it's possiblebe good at many things.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- photoreal
- compositing
- comedy
albums:
-   cover:
    images:
    -   caption: ''
        filename: fedex-change.01.jpg
    -   caption: ''
        filename: fedex-change.02.jpg
    -   caption: ''
        filename: fedex-change.03.jpg
    -   caption: ''
        filename: fedex-change.04.jpg
    -   caption: ''
        filename: fedex-change.05.jpg
    path: work/2010/fedex-change/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2010/fedex-change/videos/fedex-change.jpg
    path: work/2010/fedex-change/videos/fedex-change
    slug: fedex-change
    sources:
    -   filename: fedex-change-1280x719.mp4
        format: mp4
        height: '719'
        size: 19061666
        width: '1280'
    -   filename: fedex-change-960x540.mp4
        format: mp4
        height: '540'
        size: 2270948
        width: '960'
    -   filename: fedex-change-640x360.ogv
        format: ogv
        height: '360'
        size: 1931925
        width: '640'
    -   filename: fedex-change-640x360.webm
        format: webm
        height: '360'
        size: 1810704
        width: '640'
    -   filename: fedex-change-640x360.mp4
        format: mp4
        height: '360'
        size: 1582316
        width: '640'
    -   filename: fedex-change-480x270.mp4
        format: mp4
        height: '270'
        size: 1221495
        width: '480'
    title: Fedex Change
credits:
-   companyName: FedEx
    companySlug: fedex
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: BBDO Toronto
    companySlug: bbdo-toronto
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Peter Ignazi
    personSlug: peter-ignazi
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Mike Donaghey
    personSlug: mike-donaghey
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Chris Joakim
    personSlug: chris-joakim
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Christie Gawenda
    personSlug: christie-gawenda
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   companyName: OPC
    companySlug: opc
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Tim Godsall
    personSlug: tim-godsall
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Tiko Poulakakis
    personSlug: tiko-poulakakis
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   personName: Harland Weiss
    personSlug: harland-weiss
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Rick Jarjoura
    personSlug: rick-jarjoura
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: false
-   companyName: coverboy
    companySlug: coverboy
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Griff Henderson
    personSlug: griff-henderson
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Michelle Lee
    personSlug: michelle-lee
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
-   personName: Debbie Cooke
    personSlug: debbie-cooke
    roleSlug: vfx-producer
    roleTitle: VFX Producer
    visible: false
---
In this award winning spot, Dashing recently showcased their diversity and
nimble nature in a spot for FedEx, shot by Director Tim Godsall out of BBDO
Toronto. _Change_ stars an effortlessly competent business
executive/surgeon/tattoo artist/chess master/sculptor who orders one of his
office drones to use FedEx for a non-urgent shipment. When the assistant
second-guesses his boss, explaining his belief that FedEx is only good for
urgent shipping needs, he is met with a stern explanation that it's possible
be good at many things.

Dashing complemented Godsall's work with a variety of seamless effects:
creating a real, physics-based trajectory for a golf ball landing perfectly in
a tiny target; using a similar process to create plaster chipping off a
sculpture; perfecting the design and position of a tattoo on the man's back so
it looked as if it was being inked by his own hand, matching the subtle
warping of his skin with deforming tracks and matte painting work.

"The brief was to make everything as natural as possible," noted Dashing
Founder/VFX Supervisor Rob Moggach, who worked in Maya and Flame for all
animation, compositing and finishing. "The comedy of the spot is in the sheer
believability of the extraordinary feats that happen. Going beyond this would
have missed the point. We were careful in this regard not to take any of our
work too far and let cues from real possibilities guide us."

"BBDO was a great partner and trusted our instinct throughout," stated Dashing
EP Danielle Lyons. "From the beginning, Director Tim Godsall was onboard,
knowing instinctively from our reel that with a small brief we'd take the
creative steps necessary to get the job done without any creative hand-
holding." Dashing delivered as promised, putting the final touches on the spot
in just four weeks.