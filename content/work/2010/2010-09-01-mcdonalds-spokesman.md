---
title: Mcdonalds Spokesman
subtitle:
slug: mcdonalds-spokesman
date: 2010-09-01
role:
headline: directed by The Perlorians, Soft Citizen for Cossette West
summary: Cossette West is responsible for bringing you a game of one-upmanship like
    youhaven't seen in a while. Fantastic direction & casting from The Perlorian Brosand
    Dashing VFX make this competition to be the spokesman for McDonald's QPD,a race
    to watch.
excerpt: Cossette West is responsible for bringing you a game of one-upmanship like
    youhaven't seen in a while. Fantastic direction & casting from The Perlorian Brosand
    Dashing VFX make this competition to be the spokesman for McDonald's QPD,a race
    to watch.
published: false
featured: false
categories:
- Advertising
tags:
- VFX
- compositing
- comedy
- food
albums:
-   cover:
    images:
    -   caption: ''
        filename: mcdonalds-spokesman.01.jpg
    -   caption: ''
        filename: mcdonalds-spokesman.02.jpg
    -   caption: ''
        filename: mcdonalds-spokesman.03.jpg
    -   caption: ''
        filename: mcdonalds-spokesman.04.jpg
    path: work/2010/mcdonalds-spokesman/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2010/mcdonalds-spokesman/videos/mcdonalds-spokesman.jpg
    path: work/2010/mcdonalds-spokesman/videos/mcdonalds-spokesman
    slug: mcdonalds-spokesman
    sources:
    -   filename: mcdonalds-spokesman-1275x720.mp4
        format: mp4
        height: '720'
        size: 19835534
        width: '1275'
    -   filename: mcdonalds-spokesman-960x540.mp4
        format: mp4
        height: '540'
        size: 5716222
        width: '960'
    -   filename: mcdonalds-spokesman-640x360.ogv
        format: ogv
        height: '360'
        size: 3900582
        width: '640'
    -   filename: mcdonalds-spokesman-640x360.mp4
        format: mp4
        height: '360'
        size: 3800661
        width: '640'
    -   filename: mcdonalds-spokesman-640x360.webm
        format: webm
        height: '360'
        size: 3290410
        width: '640'
    -   filename: mcdonalds-spokesman-480x270.mp4
        format: mp4
        height: '270'
        size: 2656349
        width: '480'
    title: Mcdonalds Spokesman
credits:
-   companyName: McDonalds
    companySlug: mcdonalds
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Cossette West
    companySlug: cossette-west
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Rob Sweetman
    personSlug: rob-sweetman
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Bart Bachelor
    personSlug: bart-bachelor
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Mike Felix
    personSlug: mike-felix
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   companyName: McDonalds
    companySlug: mcdonalds
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: Cossette West
    companySlug: cossette-west
    roleSlug: agency
    roleTitle: Advertising Agency
    visible: false
-   personName: Rob Sweetman
    personSlug: rob-sweetman
    roleSlug: agency-cd
    roleTitle: Creative Director
    visible: false
-   personName: Bart Bachelor
    personSlug: bart-bachelor
    roleSlug: agency-art-director
    roleTitle: Art Director
    visible: false
-   personName: Mike Felix
    personSlug: mike-felix
    roleSlug: agency-copywriter
    roleTitle: Copywriter
    visible: false
-   personName: Mike Hasinoff
    personSlug: mike-hasinoff
    roleSlug: agency-producer
    roleTitle: Agency Producer
    visible: false
-   roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: 'The Perlorian Bros. '
    personSlug: perlorian-bros
    roleSlug: director
    roleTitle: Director
    visible: false
-   personName: Stops Lagensteiner
    personSlug: stops-lagensteiner
    roleSlug: dp
    roleTitle: Director of Photography
    visible: false
-   personName: Link York
    personSlug: link-york
    roleSlug: prod-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: School Editing
    companySlug: school-editing
    roleSlug: edit
    roleTitle: Editorial
    visible: false
-   personName: Brian Wells
    personSlug: brian-wells
    roleSlug: editor
    roleTitle: Editor
    visible: false
-   personName: Sarah Brooks
    personSlug: sarah-brooks
    roleSlug: edit-ep
    roleTitle: Executive Producer
    visible: false
-   companyName: Dashing Collective
    companySlug: dshng
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
-   personName: Danielle Lyons
    personSlug: danielle-lyons
    roleSlug: vfx-ep
    roleTitle: Executive Producer
    visible: false
---
Cossette West is responsible for bringing you a game of one-upmanship like you
haven't seen in a while. Fantastic direction & casting from The Perlorian Bros
and Dashing VFX make this competition to be the spokesman for McDonald's QPD,
a race to watch.