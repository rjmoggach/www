---
title: Xbox Lips
subtitle:
slug: xbox-lips
date: 2008-11-15
role:
headline: directed by Tom Kuntz, MJZ
summary: A pair of lips travels cross country to reunite with his human couterpart
    fora karaoke session.
excerpt: A pair of lips travels cross country to reunite with his human couterpart
    fora karaoke session.
published: false
featured: false
categories:
- Advertising
tags: []
albums:
-   cover:
    images:
    -   caption: ''
        filename: xbox-lips.01.jpg
    -   caption: ''
        filename: xbox-lips.02.jpg
    -   caption: ''
        filename: xbox-lips.03.jpg
    -   caption: ''
        filename: xbox-lips.04.jpg
    -   caption: ''
        filename: xbox-lips.05.jpg
    -   caption: ''
        filename: xbox-lips.06.jpg
    -   caption: ''
        filename: xbox-lips.07.jpg
    -   caption: ''
        filename: xbox-lips.08.jpg
    path: work/2008/xbox-lips/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/xbox-lips/videos/xbox-lips.jpg
    path: work/2008/xbox-lips/videos/xbox-lips
    provider: html5
    slug: xbox-lips
    sources:
    -   filename: xbox-lips-1280x720.mp4
        format: mp4
        height: '720'
        size: 18465883
        width: '1280'
    -   filename: xbox-lips-960x540.mp4
        format: mp4
        height: '540'
        size: 12122370
        width: '960'
    -   filename: xbox-lips-640x360.mp4
        format: mp4
        height: '360'
        size: 7718442
        width: '640'
    -   filename: xbox-lips-640x360.ogv
        format: ogv
        height: '360'
        size: 6938798
        width: '640'
    -   filename: xbox-lips-640x360.webm
        format: webm
        height: '360'
        size: 5071972
        width: '640'
    -   filename: xbox-lips-480x270.mp4
        format: mp4
        height: '270'
        size: 4863392
        width: '480'
    title: Xbox Lips
credits:
-   companyName: XBox
    companySlug: xbox
    roleSlug: client
    roleTitle: Client
    visible: false
-   companyName: MJZ
    companySlug: mjz
    roleSlug: production
    roleTitle: Production Company
    visible: false
-   personName: Tom Kuntz
    personSlug: tom-kuntz
    roleSlug: director
    roleTitle: Director
    visible: false
-   companyName: Method Studios
    companySlug: method-studios
    roleSlug: vfx
    roleTitle: Visual Effects
    visible: false
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: vfx-supervisor
    roleTitle: VFX Supervisor
    visible: false
---
A pair of lips travels cross country to reunite with his human couterpart for
a karaoke session.