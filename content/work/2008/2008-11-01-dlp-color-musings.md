---
title: DLP "Color Musings"
subtitle:
slug: dlp-color-musings
date: 2008-11-01
role:
headline: ''
summary:
excerpt:
published: false
featured: false
categories: []
tags:
- Live Action
- VFX
- CG
- compositing
- environment
- colour
- set extensions
- surreal
albums:
-   cover:
    images:
    -   caption: ''
        filename: dlp-color-musings.01.jpg
    -   caption: ''
        filename: dlp-color-musings.02.jpg
    -   caption: ''
        filename: dlp-color-musings.03.jpg
    -   caption: ''
        filename: dlp-color-musings.04.jpg
    -   caption: ''
        filename: dlp-color-musings.05.jpg
    -   caption: ''
        filename: dlp-color-musings.06.jpg
    -   caption: ''
        filename: dlp-color-musings.07.jpg
    -   caption: ''
        filename: dlp-color-musings.08.jpg
    -   caption: ''
        filename: dlp-color-musings.09.jpg
    -   caption: ''
        filename: dlp-color-musings.10.jpg
    path: work/2008/dlp-color-musings/images/stills
    slug: stills
    title: Stills
videos:
-   caption: ''
    cover: work/2008/dlp-color-musings/videos/dlp-color-musings.jpg
    path: work/2008/dlp-color-musings/videos/dlp-color-musings
    slug: dlp-color-musings
    sources:
    -   filename: dlp-color-musings-1280x720.mp4
        format: mp4
        height: '720'
        size: 11578734
        width: '1280'
    -   filename: dlp-color-musings-960x540.mp4
        format: mp4
        height: '540'
        size: 7467671
        width: '960'
    -   filename: dlp-color-musings-640x360.ogv
        format: ogv
        height: '360'
        size: 4094662
        width: '640'
    -   filename: dlp-color-musings-640x360.mp4
        format: mp4
        height: '360'
        size: 3977179
        width: '640'
    -   filename: dlp-color-musings-640x360.webm
        format: webm
        height: '360'
        size: 3435389
        width: '640'
    -   filename: dlp-color-musings-480x270.mp4
        format: mp4
        height: '270'
        size: 2686908
        width: '480'
    title: Dlp Color Musings
credits:
-   personName: Robert Moggach
    personSlug: robert-moggach
    roleSlug: director
    roleTitle: Director
    visible: true
-   personName: Harris Charalambous
    personSlug: harris-charalambous
    roleSlug: dp
    roleTitle: Director of Photography
    visible: true
-   personName: Theresa Marth
    personSlug: theresa-marth
    roleSlug: line-producer
    roleTitle: Line Producer
    visible: true
---
