---
title: W
subtitle:
slug: w
date: 2008-10-01
role:
headline: directed by Oliver Stone, LionsGate Film
summary:
excerpt:
published: true
featured: true
categories:
    - Feature Films
tags: []
albums:
    - cover:
      images:
          - caption: ''
            filename: w-bagdhad-dream.01.jpg
          - caption: ''
            filename: w-bagdhad-dream.02.jpg
          - caption: ''
            filename: w-bagdhad-dream.03.jpg
      path: work/2008/w/images/bagdhad-dream
      slug: bagdhad-dream
      title: Bagdhad Dream
    - cover:
      images:
          - caption: ''
            filename: w-baseball.01.jpg
          - caption: ''
            filename: w-baseball.02.jpg
          - caption: ''
            filename: w-baseball.03.jpg
          - caption: ''
            filename: w-baseball.04.jpg
          - caption: ''
            filename: w-baseball.05.jpg
      path: work/2008/w/images/baseball
      slug: baseball
      title: Baseball
    - cover:
      images:
          - caption: ''
            filename: w.01.jpg
          - caption: ''
            filename: w.02.jpg
          - caption: ''
            filename: w.03.jpg
          - caption: ''
            filename: w.04.jpg
          - caption: ''
            filename: w.05.jpg
          - caption: ''
            filename: w.06.jpg
          - caption: ''
            filename: w.07.jpg
          - caption: ''
            filename: w.08.jpg
      path: work/2008/w/images/stills
      slug: stills
      title: Stills
videos:
    - caption: ''
      cover: work/2008/w/videos/w-bagdhad-dream.jpg
      path: work/2008/w/videos/w-bagdhad-dream
      slug: w-bagdhad-dream
      sources:
          - filename: w-bagdhad-dream-1280x720.mp4
            format: mp4
            height: '720'
            size: 3175847
            width: '1280'
          - filename: w-bagdhad-dream-960x540.mp4
            format: mp4
            height: '540'
            size: 1937219
            width: '960'
          - filename: w-bagdhad-dream-640x360.mp4
            format: mp4
            height: '360'
            size: 1213628
            width: '640'
          - filename: w-bagdhad-dream-640x360.ogv
            format: ogv
            height: '360'
            size: 848962
            width: '640'
          - filename: w-bagdhad-dream-480x270.mp4
            format: mp4
            height: '270'
            size: 660543
            width: '480'
          - filename: w-bagdhad-dream-640x360.webm
            format: webm
            height: '360'
            size: 547085
            width: '640'
      title: W Bagdhad Dream
    - caption: ''
      cover: work/2008/w/videos/w-baseball.jpg
      path: work/2008/w/videos/w-baseball
      slug: w-baseball
      sources:
          - filename: w-baseball-1280x720.mp4
            format: mp4
            height: '720'
            size: 9465395
            width: '1280'
          - filename: w-baseball-960x540.mp4
            format: mp4
            height: '540'
            size: 6914088
            width: '960'
          - filename: w-baseball-640x360.ogv
            format: ogv
            height: '360'
            size: 5010824
            width: '640'
          - filename: w-baseball-640x360.mp4
            format: mp4
            height: '360'
            size: 4449062
            width: '640'
          - filename: w-baseball-480x270.mp4
            format: mp4
            height: '270'
            size: 3274115
            width: '480'
          - filename: w-baseball-640x360.webm
            format: webm
            height: '360'
            size: 3172429
            width: '640'
      title: W Baseball
credits:
    - companyName: Lionsgate Films
      companySlug: lionsgate-films
      roleSlug: production
      roleTitle: Production Company
      visible: false
    - personName: Oliver Stone
      personSlug: oliver-stone
      roleSlug: director
      roleTitle: Director
      visible: false
    - companyName: Method Studios
      companySlug: method-studios
      roleSlug: vfx
      roleTitle: Visual Effects
      visible: false
    - personName: Robert Moggach
      personSlug: robert-moggach
      roleSlug: flame
      roleTitle: Flame Artist
      visible: false
---

W
