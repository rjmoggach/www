let activeEnv = process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || 'development'


console.log(`Log: Using environment config: '${activeEnv}'`)


require('dotenv').config({
    path: `.env.${activeEnv}`,
})


const config = require('./src/config')


const pathPrefix = config.pathPrefix === '/' ? '' : config.pathPrefix


const AWS = require('aws-sdk')
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
})


module.exports = {
    pathPrefix: config.pathPrefix,
    siteMetadata: config.siteMetadata,
    plugins: [
        /* Plugins */
        {
            resolve: `gatsby-plugin-layout`,
            options: {
                component: require.resolve(`./src/components/Layout/index.jsx`),
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `work`,
                path: `${__dirname}/content/work/`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `blog`,
                path: `${__dirname}/content/blog/`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `play`,
                path: `${__dirname}/content/play/`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `pages`,
                path: `${__dirname}/content/pages/`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/content/pages/about/images`,
            },
        },
        `gatsby-plugin-sitemap`,
        `gatsby-plugin-lodash`,
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-sass`,
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-emotion`,
        `gatsby-plugin-theme-ui`,
        `gatsby-plugin-catch-links`,
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                plugins: [
                    `gatsby-remark-normalize-paths`,
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            maxWidth: 1920,
                            wrapperStyle: fluidResult => `flex:${_.round(fluidResult.aspectRatio, 2)};`,
                        },
                    },
                    {
                        resolve: `gatsby-remark-prismjs`,
                        options: {
                            classPrefix: 'language-',
                            inlineCodeMarker: null,
                            aliases: {},
                            showLineNumbers: false,
                            noInlineHighlight: false,
                        },
                    },
                    'gatsby-remark-copy-linked-files',
                ],
            },
        },
        {
            resolve: 'gatsby-plugin-module-resolver',
            options: {
                root: './src',
                aliases: {
                    components: './components',
                    containers: './containers',
                    config: './config',
                    images: './images',
                    state: './state',
                    context: './context',
                    styles: './styles',
                    utils: './utils',
                    hooks: './hooks',
                    theme: './gatsby-plugin-theme-ui',
                    static: {
                        root: './public',
                        alias: './static',
                    },
                },
            },
        },
        // {
        //     resolve: `gatsby-plugin-typography`,
        //     options: {
        //         pathToConfigModule: 'src/config/typography.js',
        //         omitGoogleFont: true,
        //     },
        // },
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                trackingId: config.googleAnalyticsID,
            },
        },
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                name: config.siteMetadata.title,
                short_name: config.siteMetadata.titleAlt,
                description: config.siteMetadata.description,
                start_url: pathPrefix,
                background_color: config.siteMetadata.backgroundColor,
                theme_color: config.siteMetadata.themeColor,
                display: 'standalone',
                icon: config.favicon,
            }
        }
    ],
}
