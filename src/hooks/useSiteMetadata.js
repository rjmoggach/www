import { useStaticQuery, graphql } from 'gatsby'

export const useSiteMetadata = () => {
    const { site } = useStaticQuery(
        graphql`
            query SEO {
                site {
                    buildTime(formatString: "YYYY-MM-DD")
                    siteMetadata {
                        defaultTitle: title
                        titleAlt
                        shortName
                        author
                        siteLanguage
                        logo
                        url
                        pathPrefix
                        defaultDescription: description
                        defaultBanner: banner
                        twitter
                    }
                }
            }
        `
    )
    // console.log('useSiteMetadata: ', site)
    return site
}

export default useSiteMetadata
