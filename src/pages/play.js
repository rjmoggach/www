import React from 'react'
import Welcome from 'components/Welcome'
import { MenuSetter } from 'components/Header'
import { BackgroundSetter } from 'components/Background/Context'
class PlayPage extends React.Component {
    componentDidMount() {}

    render() {
        return (
            <React.Fragment>
                <MenuSetter source='play' />
                <BackgroundSetter />
                <Welcome />
            </React.Fragment>
        )
    }
}

export default PlayPage
