/** @jsx jsx */
import { jsx, Box, Flex, Styled } from 'theme-ui'
import React from 'react'
// import Home from 'components/Home'
// import MenuSetter from 'components/MenuSetter'
// import Pusher from 'components/Pusher'

import { Link } from 'gatsby'
import Welcome from 'components/Welcome'

const widths = ['4rem', '6rem', '8rem', '10rem', '12rem']
const filler = {
    width: '100%',
    height: '100%',
    m: 0,
    p: 0,
}
const style = {
    wrapper: {
        width: '100vw',
        height: '100vh',
        background: 'gray',
    },
    colors: {
        gray: '#3e3e3e',
        deep: '#581845',
        red: '#900C3F',
        reddish: '#C70039',
        orange: '#FF5733',
        yellow: '#FFC300',
        accent: '#DAF7A6',
        green: '#148F77',
        teal: '#17A589',
        blue: '#5DADE280',
        sky: '#D6EAF8',
    },
    layout: {
        wrapper: {
            width: '100vw',
            height: '100vh',
        },
        header: {
            position: 'fixed',
            top: 0,
            width: '100%',
            mx: 'auto',
            // ...filler,
            display: 'grid',
            gridTemplateColumns: 'min-content auto min-content',
            gridTemplateRows: 'min-content min-content auto',
            gridTemplateAreas: [
                '"logo title burger" "menu menu menu" ". . ."',
                '"logo title burger" "menu menu menu" ". . ."',
                '"logo . burger" "title . ." "menu . ."',
            ],
            alignItems: 'stretch',
        },
        nav: {},
        content: {
            m: 0,
            p: 0,
            ml: ['5vw', '5vw', '25vw'],
            mr: ['5vw', '5vw', '10vw'],
            transition: 'margin 0.3s',
        },
        logo: {
            gridArea: 'logo',
            width: '25vw',
            height: '20vw',
            display: 'flex',
            flexFlow: 'column nowrap',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            flex: '0 0 auto',
        },
        title: {
            gridArea: 'title',
            ...filler,
            display: 'flex',
            flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
            flex: ['1 1 auto', '1 1 auto', '0 1 auto'],
        },
        burger: {
            width: '10vw',
            gridArea: 'burger',
            // ...filler,
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'flex-end',
        },
        menu: {
            gridArea: 'menu',
            ...filler,
            display: 'flex',
            flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
            alignItems: 'flex-end',
            justifyContent: ['center', 'center', 'flex-start'],
            flex: ['1 1 auto', '1 1 auto', '0 1 auto'],
        },
    },
}

const Wrapper = props => {
    const { hidden, children, sx } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.gray,
                ...style.layout.wrapper,
                overflow: hidden ? 'hidden' : 'visible',
                // maxWidth: 2000,
                // mx: 'auto',
            }}
            sx={sx}>
            {children}
        </Box>
    )
}

const Logo = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.red,
                ...style.layout.logo,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'primary',
                    textAlign: 'right',
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                    transition: 'margin 0.3s',
                    fontSize: '3vw',
                    lineHeight: '2.6vw',
                }}>
                Robert
                <br />
                Moggach
            </Box>
        </Box>
    )
}

const Title = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.deep,
                ...style.layout.title,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'secondary',
                    textAlign: ['left', 'left', 'right'],
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                }}>
                Title
            </Box>
        </Box>
    )
}

const Burger = props => {
    const { children } = props
    return (
        <Box
            sx={{
                ...style.layout.burger,
                backgroundColor: style.colors.orange,
            }}>
            <Box
                sx={{
                    // mr: ['0rem', '0rem', '2rem', '10rem', '16rem'],
                    width: ['2rem', '3rem', '4rem', '5rem'],
                    height: ['2rem', '3rem', '4rem', '5rem'],
                    padding: '1rem',
                    backgroundColor: 'primary',
                    display: 'flex',
                    transition: 'margin 0.3s',
                }}>
                B
            </Box>
        </Box>
    )
}

const Menu = props => {
    const { children } = props
    return (
        <Box
            sx={{
                ...style.layout.menu,
                backgroundColor: style.colors.teal,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'secondary',
                    textAlign: ['left', 'left', 'right'],
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                }}>
                Menu
            </Box>
        </Box>
    )
}

const Header = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.blue,
                ...style.layout.header,
            }}>
            {children}
        </Box>
    )
}

const Nav = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.accent,
                ...style.layout.nav,
            }}>
            {children}
        </Box>
    )
}

const HeaderSpacer = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.blue,
                ...style.layout.header,
            }}>
            {children}
        </Box>
    )
}

const Content = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.yellow,
                ...style.layout.content,
            }}>
            <div>
                <Welcome />
            </div>
        </Box>
    )
}

const TestPage = props => {
    return (
        <React.Fragment>
            <Wrapper
                sx={{
                    position: 'fixed',
                    zIndex: [1000, 1000, 0],
                }}>
                <Header>
                    <Logo />
                    <Title />
                    <Menu />
                    <Burger />
                </Header>
            </Wrapper>
            <Wrapper
                sx={{
                    position: 'fixed',
                    zIndex: [1000, 1000, 0],
                }}>
                <Content />
            </Wrapper>
        </React.Fragment>
    )
}

export default TestPage
