/** @jsx jsx */
import { jsx, Box, Flex, Styled } from 'theme-ui'
import React from 'react'
// import Home from 'components/Home'
// import MenuSetter from 'components/MenuSetter'
// import Pusher from 'components/Pusher'

import { Link } from 'gatsby'
import Welcome from 'components/Welcome'

const widths = ['4rem', '6rem', '8rem', '10rem', '12rem']
const filler = {
    width: '100%',
    height: '100%',
    m: 0,
    p: 0,
}
const style = {
    wrapper: {
        width: '100vw',
        height: '100vh',
        background: 'gray',
    },
    colors: {
        gray: '#3e3e3e',
        deep: '#581845',
        red: '#900C3F',
        reddish: '#C70039',
        orange: '#FF5733',
        yellow: '#FFC300',
        accent: '#DAF7A6',
        green: '#148F77',
        teal: '#17A589',
        blue: '#5DADE280',
        sky: '#D6EAF8',
    },
    layout: {
        wrapper: {
            width: '100vw',
            height: '100vh',
        },
        header: {
            position: 'fixed',
            top: 0,
            width: '100%',
            mx: 'auto',
            // ...filler,
            display: 'grid',
            gridTemplateColumns: 'min-content auto min-content',
            gridTemplateRows: 'min-content min-content auto',
            gridTemplateAreas: [
                '"logo title burger" "menu menu menu" ". . ."',
                '"logo title burger" "menu menu menu" ". . ."',
                '"logo . burger" "title . ." "menu . ."',
            ],
            alignItems: 'stretch',
        },
        nav: {},
        content: {
            position: 'relative',
            width: ['90vw', '80vw', '65vw'],
            left: ['5vw', '10vw', '25vw'],
            top: ['20vw'],
            transition: 'margin 0.3s',
            zIndex: 10,
        },
        logo: {
            position: 'fixed',
            top: 0,
            left: 0,
            width: '25vw',
            height: '20vw',
            display: 'flex',
            flexFlow: 'column nowrap',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            zIndex: 20,
        },
        title: {
            position: 'fixed',
            top: [0, 0, '20vw'],
            left: ['25vw', '25vw', 0],
            width: ['65vw', '65vw', '25vw'],
            height: ['20vw', '20vw', '10vw'],
            display: 'flex',
            flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
            zIndex: 20,
        },
        burger: {
            position: 'fixed',
            top: 0,
            left: '90vw',
            width: '10vw',
            height: '20vw',
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'flex-end',
            zIndex: 20,
        },
        menu: {
            position: 'fixed',
            top: ['20vw', '20vw', '30vw'],

            width: ['100vw', '100vw', '25vw'],
            display: 'flex',
            flexFlow: ['row wrap', 'row wrap', 'column nowrap'],
            alignItems: 'flex-end',
            justifyContent: ['center', 'center', 'flex-start'],
            flex: ['1 1 auto', '1 1 auto', '0 1 auto'],
            zIndex: 20,
        },
    },
}

const Wrapper = props => {
    const { hidden, children, sx } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.gray,
                ...style.layout.wrapper,
                overflow: hidden ? 'hidden' : 'visible',
                // maxWidth: 2000,
                // mx: 'auto',
            }}
            sx={sx}>
            {children}
        </Box>
    )
}

const Logo = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.red,
                ...style.layout.logo,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'primary',
                    textAlign: 'right',
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                    transition: 'margin 0.3s',
                    fontSize: '3vw',
                    lineHeight: '2.6vw',
                }}>
                Robert
                <br />
                Moggach
            </Box>
        </Box>
    )
}

const Title = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.deep,
                ...style.layout.title,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'secondary',
                    textAlign: ['left', 'left', 'right'],
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                }}>
                Title
            </Box>
        </Box>
    )
}

const Burger = props => {
    const { children } = props
    return (
        <Box
            sx={{
                ...style.layout.burger,
                backgroundColor: style.colors.orange,
            }}>
            <Box
                sx={{
                    // mr: ['0rem', '0rem', '2rem', '10rem', '16rem'],
                    width: ['2rem', '3rem', '4rem', '5rem'],
                    height: ['2rem', '3rem', '4rem', '5rem'],
                    padding: '1rem',
                    backgroundColor: 'primary',
                    display: 'flex',
                    transition: 'margin 0.3s',
                }}>
                B
            </Box>
        </Box>
    )
}

const Menu = props => {
    const { children } = props
    return (
        <Box
            sx={{
                ...style.layout.menu,
                backgroundColor: style.colors.teal,
            }}>
            <Box
                sx={{
                    padding: '1rem',
                    backgroundColor: 'secondary',
                    textAlign: ['left', 'left', 'right'],
                    display: 'flex',
                    flexFlow: 'column nowrap',
                    justifyContent: 'flex-end',
                }}>
                Menu
            </Box>
        </Box>
    )
}

const Header = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.blue,
                ...style.layout.header,
            }}>
            {children}
        </Box>
    )
}

const Nav = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.accent,
                ...style.layout.nav,
            }}>
            {children}
        </Box>
    )
}

const HeaderSpacer = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.blue,
                ...style.layout.header,
            }}>
            {children}
        </Box>
    )
}

const Content = props => {
    const { children } = props
    return (
        <Box
            sx={{
                backgroundColor: style.colors.yellow,
                ...style.layout.content,
            }}>
            <div>
                <Welcome />
            </div>
        </Box>
    )
}

const TestPage = props => {
    return (
        <React.Fragment>
            <Logo />
            <Title />
            <Menu />
            <Burger />
            <Content />
        </React.Fragment>
    )
}

export default TestPage
