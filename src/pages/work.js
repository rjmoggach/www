import React from 'react'

import Welcome from 'components/Welcome'
import { MenuSetter } from 'components/Header'
import { WorkHome } from 'components/Work'
import { BackgroundSetter } from 'components/Background/Context'
import { SEO } from 'components/SEO'

const WorkPage = ({ data }) => {
    return (
        <React.Fragment>
            <SEO title='Work: Reel' />
            <MenuSetter source='work' />
            <BackgroundSetter />
            <WorkHome />
        </React.Fragment>
    )
}

export default WorkPage
