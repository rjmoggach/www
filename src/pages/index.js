import React from 'react'
import { Home } from 'components/Home'
import { MenuSetter } from 'components/Header'
import { BackgroundSetter } from 'components/Background/Context'
import { SEO } from 'components/SEO'

const IndexPage = props => {
    return (
        <React.Fragment>
            <SEO />
            <MenuSetter />
            <BackgroundSetter />
            <Home />
        </React.Fragment>
    )
}

export default IndexPage
