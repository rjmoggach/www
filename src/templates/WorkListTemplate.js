/** @jsx jsx */
import { jsx, Box } from 'theme-ui'
import React from 'react'
import { Link, graphql } from 'gatsby'
import { MenuSetter } from 'components/Header'
import { WorkList } from 'components/Work/WorkList'
import { SEO } from 'components/SEO'

const WorkListTemplate = props => {
    console.log('WorkListTemplate', props)
    const posts = props.data.allMarkdownRemark.edges

    return (
        <Box sx={{ variant: 'styles.spacer.work' }}>
            <SEO title='Work' />
            <MenuSetter source='work' />
            <WorkList work={props.data.allMarkdownRemark.edges} pageContext={props.pageContext} />
        </Box>
    )
}

export default WorkListTemplate

export const pageQuery = graphql`
    query($skip: Int!, $limit: Int!, $featured: [Boolean!]) {
        allMarkdownRemark(
            filter: {
                fields: { sourceName: { eq: "work" } }
                fileAbsolutePath: { regex: "/work/" }
                frontmatter: { published: { eq: true }, featured: { in: $featured } }
            }
            sort: { order: DESC, fields: [frontmatter___date] }
            limit: $limit
            skip: $skip
        ) {
            edges {
                node {
                    id
                    excerpt(pruneLength: 200)
                    fields {
                        slug
                        sourceName
                    }
                    frontmatter {
                        albums {
                            images {
                                caption
                                filename
                            }
                            path
                            slug
                            title
                        }
                        categories
                        date
                        draft
                        excerpt
                        headline
                        featured
                        title
                        subtitle
                    }
                }
            }
        }
    }
`
