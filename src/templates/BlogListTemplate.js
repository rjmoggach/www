import React from 'react'
import { Link, graphql } from 'gatsby'
import { MenuSetter } from 'components/Header'
import { BlogList } from 'components/Blog'
import { NextPrev } from 'components/NextPrev'
import { BackgroundSetter } from 'components/Background/Context'

const BlogListTemplate = props => {
    // console.log('BlogListTemplate', props)
    const posts = props.data.allMarkdownRemark.edges

    return (
        <React.Fragment>
            <MenuSetter source='blog' />
            <BackgroundSetter />
            <BlogList data={props.data} />
            <NextPrev
                prev={props.pageContext.nextPagePath || ''}
                next={props.pageContext.previousPagePath || ''}
                nextText='Newer'
                prevText='Older'
            />
        </React.Fragment>
    )
}

export default BlogListTemplate

export const pageQuery = graphql`
    query($skip: Int!, $limit: Int!) {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "/blog/" }, frontmatter: { published: { ne: false } } }
            sort: { order: DESC, fields: [frontmatter___date] }
            limit: $limit
            skip: $skip
        ) {
            edges {
                node {
                    excerpt
                    fields {
                        slug
                    }
                    frontmatter {
                        date
                        title
                        categories
                        tags
                        cover {
                            childImageSharp {
                                fluid(maxWidth: 1920, maxHeight: 480, quality: 90) {
                                    ...GatsbyImageSharpFluid
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`
