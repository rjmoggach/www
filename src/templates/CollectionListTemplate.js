/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import { graphql, Link } from 'gatsby'
import kebabCase from 'lodash/kebabCase'
import startCase from 'lodash/startCase'
import { MenuSetter } from 'components/Header'
import { TagListPage } from 'components/Collections'

const CollectionListTemplate = ({ data, pageContext }) => {
    // console.log('CollectionListTemplate', pageContext)
    const { collections, collectionName, sourceName } = pageContext

    let collectionType
    switch (collectionName) {
        case 'tags':
            return <TagListPage tags={collections} source={sourceName} />
            break
        case 'categories':
            switch (sourceName) {
                case 'blog':
                    collectionType = 'blogCategories'
                    break
                case 'work':
                    collectionType = 'workCategories'
                    break
                default:
                    collectionType = 'genericCategories'
            }
            break
        case 'roles':
            switch (sourceName) {
                case 'work':
                    collectionType = 'workRoles'
                    break
                default:
                    collectionType = 'genericRoles'
            }
            break
        default:
            collectionType = 'generic'
    }

    switch (collectionType) {
        case 'workTags': {
            return <TagListPage tags={collections} source={sourceName} />
            break
        }
        case 'work': {
            return (
                <React.Fragment>
                    <MenuSetter source='work' />
                    {collections.map((collection, index) => (
                        <div>
                            <Link sx={{ variant: 'styles.a' }} to={`/work/${collectionName}/${kebabCase(collection)}`}>
                                {collection}
                            </Link>
                        </div>
                    ))}
                </React.Fragment>
            )
            break
        }
        case 'all': {
            return (
                <React.Fragment>
                    <MenuSetter />
                    {collections.map((collection, index) => (
                        <div>
                            <Link sx={{ variant: 'styles.a' }} to={`/${collectionName}/${kebabCase(collection)}`}>
                                {collection}
                            </Link>
                        </div>
                    ))}
                </React.Fragment>
            )
            break
        }
        default: {
            return (
                <React.Fragment>
                    <MenuSetter source={sourceName} />
                    {collections.map((collection, index) => (
                        <div>
                            <Link
                                sx={{ variant: 'styles.a' }}
                                to={`/${sourceName}/${collectionName}/${kebabCase(collection)}`}>
                                {collection}
                            </Link>
                        </div>
                    ))}
                </React.Fragment>
            )
        }
    }
}

export default CollectionListTemplate
