/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import { darken } from '@theme-ui/color'
import React from 'react'
import PropTypes from 'prop-types'

import { Link, navigate } from 'gatsby'
import Moment from 'react-moment'

import { CategoryList } from 'components/Work'
import { animation } from 'theme'
import { InfoTitle } from 'components/InfoTitle'

const WorkListTitle = props => {
    const { title, subtitle, categories, date, hover, slug } = props
    return (
        <Box
            // to={slug}
            onClick={() => navigate(slug)}
            sx={{
                position: 'absolute !important',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                display: 'flex',
                flexFlow: 'column wrap',
                alignItems: 'center',
                justifyContent: 'center',
                textDecoration: 'none',
                '&:after': {
                    backgroundColor: 'transparent !important',
                },
            }}>
            <Styled.h3
                sx={{
                    m: 0,
                    p: 0,
                    mt: 3,
                    letterSpacing: 'tight',
                    lineHeight: 'normal',
                    textAlign: 'center',
                    textDecoration: 'none',
                    letterSpacing: `${hover ? -0.05 : -0.1}rem`,
                    transition: animation.transition + ',letter-spacing 0.6s ease',
                }}>
                <span sx={{ color: 'primary', fontWeight: 'extrabold' }}>{title}</span>
                {subtitle && <br />}
                {subtitle ? <span sx={{ fontWeight: 'bold', fontSize: '85%' }}>{subtitle}</span> : null}
            </Styled.h3>
            <InfoTitle categories={categories} date={date} source='work' />
        </Box>
    )
}

export default WorkListTitle
