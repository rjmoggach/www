/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'

import React from 'react'
import BackgroundImage from 'gatsby-background-image'
import { visible } from 'ansi-colors'

const BackgroundImageComponent = ({ imageData, className }) => {
    return (
        <BackgroundImage
            Tag='div'
            className={className}
            fluid={imageData}
            sx={{
                position: 'fixed !important',
                top: 0,
                left: 0,
                overflow: 'visible',
                height: '100% !important',
                width: '100% !important',
                backgroundPosition: '50% 40% !important',
                backgroundRepeat: 'no-repeat !important',
                backgroundAttachment: 'fixed !important',
                backgroundSize: '100vmax !important',
                opacity: '0.17 !important',
                zIndex: 'bg',
                transition: 'all 1s ease-in-out, left 1.5s ease-in-out',
            }}
        />
    )
}

export { BackgroundImageComponent }
