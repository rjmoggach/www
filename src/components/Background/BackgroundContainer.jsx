/** @jsx jsx */
import { jsx, Box } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { ContextConsumer } from 'components/Context'

const BackgroundContainer = props => {
    const { clipped, zIndex } = props
    return (
        <React.Fragment>
            <ContextConsumer>
                {({ data, set }) => {
                    // console.log('Header', data)
                    return (
                        <React.Fragment>
                            {clipped ? (
                                <Box
                                    sx={{
                                        zIndex: zIndex,
                                        width: '100vw',
                                        height: `${
                                            data.menuOpen ? 'calc(25vmin + ' + data.menuHeight + 'px)' : '25vmin'
                                        }`,
                                        position: 'fixed',
                                        top: 0,
                                        left: 0,
                                        clipPath: `${data.menuOpen ? 'inset(-33vmin 0 0 0)' : 'inset(-25vmin 0 0 0)'}`,
                                        backgroundColor: 'background',
                                        display: ['block', 'block', 'none'],
                                        transition: 'height 0.3s, clip-path 0.3s',
                                    }}
                                    children={data.background == '' ? null : data.background}
                                />
                            ) : (
                                <Box
                                    sx={{
                                        position: 'fixed',
                                        top: 0,
                                        left: 0,
                                        overflow: 'visible',
                                        height: '100vw',
                                        width: '100vh',
                                        opacity: 1,
                                        backgroundColor: 'background',
                                    }}
                                    children={data.background == '' ? null : data.background}
                                />
                            )}
                        </React.Fragment>
                    )
                }}
            </ContextConsumer>
        </React.Fragment>
    )
}

BackgroundContainer.propTypes = {
    clipped: PropTypes.bool,
    zIndex: PropTypes.string,
}

BackgroundContainer.defaultProps = {
    clipped: false,
    zIndex: 'bg',
}

export default BackgroundContainer
