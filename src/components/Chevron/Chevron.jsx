/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'

const style = {
    arrow: {
        font: 'inherit',
        color: 'inherit',
        textTransform: 'none',
        background: 'transparent',
        border: 'none',
        m: 0,
        p: 0,
        cursor: 'pointer',
        display: 'inline-block',
        position: 'relative',
        transition: 'opacity 0.3s',
        opacity: 0.6,
        '&:hover': {
            opacity: 1,
        },
        '&:focus': {
            outline: 'none',
        },
    },
    line: {
        top: '40%',
        position: 'absolute',
        width: '60%',
        height: '4px',
        borderRadius: '4px',
        display: 'inline-block',
        transition: 'all .2s ease',
    },
}

const Chevron = props => {
    const { menuopen, onClick, width, height, ratio, color } = props

    return (
        <button
            sx={{
                ...style.arrow,
                width: width,
                height: height,
            }}
            onClick={onClick}>
            <span
                sx={{
                    ...style.line,
                    left: '1px',
                    transform: `rotate(${menuopen ? '-45' : '45'}deg)`,
                    backgroundColor: `${color}`,
                }}></span>
            <span
                sx={{
                    ...style.line,
                    right: '1px',
                    transform: `rotate(${menuopen ? '45' : '-45'}deg)`,
                    backgroundColor: `${color}`,
                }}></span>
        </button>
    )
}
Chevron.propTypes = {
    width: PropTypes.array,
    height: PropTypes.array,
    color: PropTypes.string,
    menuopen: PropTypes.bool,
}

Chevron.defaultProps = {
    width: ['32px', '32px', '40px', '50px'],
    height: ['32px', '32px', '40px', '50px'],
    color: `muted`,
    menuopen: false,
}

export default Chevron
