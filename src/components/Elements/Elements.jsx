/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React, { useState, useEffect } from 'react'

import { Link, navigate } from 'gatsby'
import kebabCase from 'lodash/kebabCase'

// TODO get this working https://using-remark.gatsbyjs.org/custom-components/
// MORE INFO https://weknowinc.com/blog/how-embed-react-components-markdown-using-gatsby-mdx-and-short-codes

const Logo = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Title = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const MenuItem = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Subtitle = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Heading1 = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Heading2 = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Heading3 = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Heading4 = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Heading5 = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Body = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

const Blockquote = props => {
    const { children } = props
    return (
        <React.Fragment>
            <Styled.h1>{children}</Styled.h1>
        </React.Fragment>
    )
}

export { Logo, Title, Subtitle, MenuItem, Heading1, Heading2, Heading3, Heading4, Heading5, Body, Blockquote }
