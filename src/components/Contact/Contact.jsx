/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React, { useState, useEffect } from 'react'
import { StaticQuery, graphql, Link } from 'gatsby'

import PropTypes from 'prop-types'

import Typist from 'react-typist'
import { animation } from 'theme'
import Clock from 'react-live-clock'
import Obfuscate from 'react-obfuscate'
import { BackgroundImage } from 'components/Background'

import { BackgroundSetter } from 'components/Background/Context'

const Contact = () => (
    <StaticQuery
        query={graphql`
            query {
                contact: markdownRemark(fileAbsolutePath: { glob: "**/contact/index.md" }) {
                    frontmatter {
                        title
                        date
                        slug
                        email
                        location
                        mobile
                    }
                    fields {
                        slug
                    }
                    html
                }
                desktopImage: imageSharp(fluid: { originalName: { regex: "/CowsCanFly.jpg/" } }) {
                    fluid(quality: 90, maxWidth: 1000, cropFocus: CENTER) {
                        ...GatsbyImageSharpFluid
                    }
                }
                mobileImage: imageSharp(fluid: { originalName: { regex: "/CowsCanFly.jpg/" } }) {
                    fluid(quality: 90, maxWidth: 800, cropFocus: CENTER) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
        `}
        render={data => {
            // console.log('Contact', data)
            // const {
            //     html,
            //     frontmatter: { email, location, mobile, title },
            // } = data.contact.nodes[0]

            // const titleWords = title.split('|')

            const [showidx, setShowIndex] = useState(0)

            useEffect(() => {
                const timer = showidx < 10 ? setTimeout(() => setShowIndex(showidx + 1), animation.page.ms) : null
                return () => (timer ? clearTimeout(timer) : null)
            })

            return (
                <Box sx={{ variant: 'styles.spacer.h2' }}>
                    <BackgroundSetter>
                        <BackgroundImage imageData={data.desktopImage.fluid} className='div-background' />
                    </BackgroundSetter>
                    <Box sx={{ textIndent: ['-0.7rem', '-0.7rem', '-1.1rem'] }}>
                        <Styled.h2
                            showidx={showidx}
                            sx={{
                                opacity: `${showidx > 0 ? '1' : '0'}`,
                                transition: 'opacity 1s ease-in',
                            }}>
                            <q>
                                <Typist cursor={{ show: false }} sx={{ display: 'inline' }}>
                                    Hi
                                    <Typist.Delay ms={1500} />
                                    ,&nbsp;this is Rob.
                                </Typist>
                            </q>
                        </Styled.h2>
                    </Box>
                    <Styled.root
                        showidx={showidx}
                        dangerouslySetInnerHTML={{ __html: data.contact.html }}
                        sx={{
                            opacity: `${showidx > 2 ? '1' : '0'}`,
                            transition: 'opacity 1s ease-in',
                            maxWidth: '50rem',
                            clear: 'both',
                        }}
                    />
                    <Styled.root
                        as='div'
                        showidx={showidx}
                        sx={{
                            opacity: `${showidx > 3 ? '1' : '0'}`,
                            transition: 'opacity 1s ease-in',
                            maxWidth: '50rem',
                            clear: 'both',
                        }}>
                        I am currently in&nbsp;
                        <Typist sx={{ display: 'inline-block' }} cursor={{ show: false }}>
                            <Typist.Delay ms={1000} />
                            {data.contact.frontmatter.location}
                        </Typist>
                        .
                    </Styled.root>
                    <Styled.p
                        showidx={showidx}
                        sx={{
                            opacity: `${showidx > 4 ? '1' : '0'}`,
                            transition: 'opacity 1s ease-in',
                        }}>
                        Local time is <Clock format={'h:mm:ssa'} ticking={true} timezone={'US/Eastern'} />.
                    </Styled.p>
                    <Styled.p
                        showidx={showidx}
                        sx={{
                            opacity: `${showidx > 5 ? '1' : '0'}`,
                            transition: 'opacity 1s ease-in',
                        }}>
                        Call my mobile on&nbsp;
                        <Obfuscate tel={data.contact.frontmatter.mobile} sx={{ variant: 'styles.Link' }} />.
                    </Styled.p>
                    <Styled.p
                        showidx={showidx}
                        sx={{
                            opacity: `${showidx > 6 ? '1' : '0'}`,
                            transition: 'opacity 1s ease-in',
                        }}>
                        Email me at&nbsp;
                        <Obfuscate
                            email={data.contact.frontmatter.email}
                            headers={{
                                subject: 'Hello!',
                                body: 'Hi Rob, Reaching out from your website to say hi.',
                            }}
                            sx={{ variant: 'styles.Link' }}
                        />
                        .
                    </Styled.p>
                </Box>
            )
        }}
    />
)

export default Contact
