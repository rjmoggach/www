import React from 'react'

const defaultContextValue = {
    data: {
        menuType: '',
        menuOpen: false,
        burgerOpen: false,
        searchOpen: false,
        menu: {
            source: '',
            title: '',
            open: false,
            items: [],
        },
        menuHeight: 0,
        background: '',
    },
    set: () => {},
}

const { Provider, Consumer } = React.createContext(defaultContextValue)

class ContextProvider extends React.Component {
    constructor() {
        super()
        this.setData = this.setData.bind(this)
        this.state = {
            ...defaultContextValue,
            set: this.setData,
        }
    }

    setData(newData) {
        this.setState(state => ({
            data: {
                ...state.data,
                ...newData,
            },
        }))
    }

    componentDidMount() {
        const s = this.props.path.split('/')[1]
        if (this.state.data.menuType.toLowerCase() !== s) {
            const menuType = s.charAt(0).toUpperCase() + s.substring(1)
            this.setState(this.setState({ menuType: menuType }))
        }
    }

    render() {
        return <Provider value={this.state}>{this.props.children}</Provider>
    }
}

export default Consumer
export { ContextProvider, Consumer as ContextConsumer }
