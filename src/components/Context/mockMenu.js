export default mockMenu = {
    work: {
        title: 'Work',
        open: false,
        type: 'work',
        items: [
            { to: '/work', label: 'Work Home' },
            { to: '/work/reel', label: 'Reel' },
            { to: '/work/categories/advertising', label: 'Advertising' },
            { to: '/work/categories/feature-films', label: 'Feature Films' },
            { to: '/work/categories/television', label: 'Television' },
        ],
    },
    empty: {
        title: '',
        open: false,
        items: [],
    },
}
