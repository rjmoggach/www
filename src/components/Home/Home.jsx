/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'

import React, { useState, useEffect } from 'react'
import { Link } from 'gatsby'
import Typist from 'react-typist'
import { Revealer } from 'components/Revealer'

import theme from 'theme'
import { animation } from 'theme'

const Home = props => {
    const [showidx, setShowIndex] = useState(0)

    useEffect(() => {
        const timer = showidx < 7 ? setTimeout(() => setShowIndex(showidx + 1), animation.page.ms) : null
        return () => (timer ? clearTimeout(timer) : null)
    })

    const titles = [
        'Artist',
        'Creative Director',
        'VFX Supervisor',
        'Producer',
        'Director',
        'Compositor',
        'Designer',
        'Animator',
        'Photographer',
        'Editor',
        'Colorist',
        'Lighter',
        'Creative Coder',
        'Entrepreneur',
    ]
    return (
        <React.Fragment>
            <Box sx={{ variant: 'styles.spacer.h1', width: ['85%', '85%', '100%'], mx: 'auto' }}>
                <Revealer showidx={showidx} hookidx='0'>
                    <Styled.h1
                        sx={{
                            ml: ['-1rem', '-1rem', '-1.1rem', '-1.3rem', '-1rem'],

                            mb: 3,
                        }}>
                        <q sx={{}}>
                            <Typist cursor={{ show: false }} sx={{ display: 'inline-block' }}>
                                Hi
                                <Typist.Delay ms={1500} />
                                ,&nbsp;this is Rob.
                            </Typist>
                        </q>
                    </Styled.h1>
                </Revealer>
                <Revealer showidx={showidx} hookidx='2'>
                    <Styled.p>
                        I’m an Artist, Creative Director, VFX Supervisor, Flame Artist, Compositor, CG Generalist,
                        Creative Coder, Designer, Producer, Director & Entrepreneur.
                    </Styled.p>
                    <Styled.p>If it's creatively and technically challenging, I want to do it.</Styled.p>
                </Revealer>
                <Styled.h2 sx={{ color: 'primary', mt: 3 }}>
                    <Revealer showidx={showidx} hookidx='3'>
                        <span
                            showidx={showidx}
                            sx={{
                                opacity: `${showidx > 3 ? '1' : '0'}`,
                                mr: 2,
                            }}>
                            I Collaborate.
                        </span>
                    </Revealer>
                    <Revealer showidx={showidx} hookidx='4'>
                        <span
                            showidx={showidx}
                            sx={{
                                opacity: `${showidx > 4 ? '1' : '0'}`,
                                mr: 2,
                            }}>
                            I Craft.
                        </span>
                    </Revealer>
                    <Revealer showidx={showidx} hookidx='5'>
                        <span
                            showidx={showidx}
                            sx={{
                                opacity: `${showidx > 5 ? '1' : '0'}`,
                            }}>
                            <Link to='/work/featured' sx={{ variant: 'styles.LinkHeavy' }}>
                                I Create.
                            </Link>
                        </span>
                    </Revealer>
                </Styled.h2>
            </Box>
        </React.Fragment>
    )
}

export default Home
