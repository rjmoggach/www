/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import React from 'react'
import { Chevron } from 'components/Chevron'
import { animation } from 'theme'
import { relative } from 'path'
import { ContextConsumer } from 'components/Context'
import theme from 'theme'

const HeaderTitle = props => {
    const { title, children, menuopen, menuitems } = props
    // console.log('HeaderTitle', animation.transition)
    return (
        <ContextConsumer>
            {({ data, set }) => {
                // console.log('Header', data)
                return (
                    <React.Fragment>
                        <React.Fragment>
                            <Box sx={{ variant: 'layout.title' }}>
                                <div
                                    sx={{
                                        variant: 'styles.title',
                                        '&:hover': {
                                            cursor: `${data.menu.items == '' ? '' : 'pointer'}`,
                                        },
                                        transition: `${animation.transition}`,
                                    }}
                                    onClick={() => {
                                        return data.menu.items.length > 0 ? set({ menuOpen: !data.menuOpen }) : null
                                    }}>
                                    {data.menu.title || 'Foo'}
                                    {data.menu.items == '' ? null : (
                                        <Box sx={{ ml: '2vmin' }}>
                                            <Chevron
                                                menuopen={data.menuOpen}
                                                width={['1rem', '1.5rem', '2rem', '2.5rem', '3rem']}
                                                height={['1rem', '1.5rem', '2rem', '2.5rem', '3rem']}
                                            />
                                        </Box>
                                    )}
                                </div>
                            </Box>
                        </React.Fragment>
                    </React.Fragment>
                )
            }}
        </ContextConsumer>
    )
}

export default HeaderTitle
