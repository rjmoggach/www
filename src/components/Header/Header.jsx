/** @jsx jsx */
import { jsx, Flex } from 'theme-ui'
import React, { useContext, useEffect } from 'react'
import { ContextConsumer } from 'components/Context'
import { Link, navigate } from 'gatsby'
import { HeaderLogo, HeaderTitle, HeaderMenu } from 'components/Header'
import theme from 'theme'

const Header = props => {
    const { menuOpen, handleMenuClick } = props
    const globalContext = useContext(ContextConsumer)
    console.log(globalContext)
    useEffect(() => console.log(globalContext))
    return (
        <React.Fragment>
            <ContextConsumer>
                {({ data, set }) => {
                    // console.log('Header', data)
                    return (
                        <React.Fragment>
                            <Flex sx={{ ...theme.layout.header }}>
                                <HeaderLogo />
                                {data.menu.title == '' ? null : (
                                    <React.Fragment>
                                        <HeaderTitle />
                                        {data.menu.items == '' ? null : (
                                            <HeaderMenu
                                                items={data.menu.items}
                                                menuOpen={data.menuOpen}
                                                data={data}
                                                set={set}
                                            />
                                        )}
                                    </React.Fragment>
                                )}
                            </Flex>
                        </React.Fragment>
                    )
                }}
            </ContextConsumer>
        </React.Fragment>
    )
}

export default Header
