/** @jsx jsx */
import { jsx } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import capitalize from 'lodash/capitalize'
import kebabCase from 'lodash/kebabCase'

class MenuChanger extends React.Component {
    componentDidMount() {
        // const data = this.props.data
        const { data, set, source, open, items } = this.props
        // console.log('MenuChanger', items)
        const title = capitalize(source)
        const itemArray = []
        // console.log('MenuChanger', items.length)
        if (items.length) {
            for (var key in items) {
                const container = {}
                container['key'] = source === 'work' ? ~~key + 2 : ~~key + 1
                container['to'] = `/${source}/categories/${kebabCase(items[key].label)}`
                container['label'] = items[key].label
                itemArray.push(container)
            }
            switch (source) {
                case 'work': {
                    itemArray.unshift({ key: 1, to: `/${source}/featured`, label: 'Featured Work' })
                    itemArray.unshift({ key: 0, to: `/${source}`, label: 'Reel' })
                    break
                }
                default: {
                    itemArray.unshift({ key: 0, to: `/${source}`, label: 'All' })
                }
            }
        }

        set({
            ...data,
            menu: { source: source, title: title, open: open, items: itemArray },
        })
    }

    render() {
        return <React.Fragment></React.Fragment>
    }
}

MenuChanger.propTypes = {
    source: PropTypes.string,
    items: PropTypes.array,
    open: PropTypes.bool,
}

MenuChanger.defaultProps = {
    source: null,
    open: false,
    items: [],
}

export default MenuChanger
