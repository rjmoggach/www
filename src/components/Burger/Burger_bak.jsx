/** @jsx jsx */
import { jsx, Styled, Box } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { rotate } from '@theme-ui/color'
import { visible } from 'ansi-colors'

const style = {
    box: {
        position: 'fixed',
        height: ['4rem', '5rem', '5rem', '6rem'],
        top: ['3rem', '4rem', '5rem', '6rem', '7rem'],
        right: ['1rem', '1rem', '1rem', '2rem'],
        zIndex: 10000,
    },
    button: {
        font: 'inherit',
        color: 'inherit',
        textTransform: 'none',
        background: 'transparent',
        border: 'none',
        m: 0,
        p: 0,
        display: 'inline-block',
        cursor: 'pointer',
        transition: 'opacity .15s',
        '&:focus': {
            outline: 'none',
        },
        overflow: 'visible',

        position: 'relative',
        opacity: 0.6,
        '&:hover': {
            opacity: 1,
            span: {
                width: '100%',
            },
        },
    },
    line: {
        overflow: 'visible',
        display: 'block',
        position: 'absolute',
        width: '100%',
        height: '4px',
        backgroundColor: 'muted',
        borderRadius: '2px',
        transformOrigin: 'center',
    },
}

const transitionDuration = 0.1

const BurgerLine = props => {
    const { menuopen, width, height, lineIndex, ratio } = props

    return (
        <span
            sx={{
                ...style.line,
                overflow: 'visible',
                // top: `${lineIndex == 1 ? `${menuopen ? '50%' : ratio}` : ''}`,
                // bottom: `${lineIndex == 2 ? `${menuopen ? '50%' : ratio}` : ''}`,
                transform: `translateY(${lineIndex == 1 ? 0 : 0}px) rotate(${
                    lineIndex == 1 ? `${menuopen ? '45deg' : '0deg'}` : `${menuopen ? '-45deg' : '0deg'}`
                })`,
                transition: `${
                    menuopen
                        ? `${
                              lineIndex == 1
                                  ? `top 0.05s ease-in-out, transform 0.05s 0.1s cubic-bezier(0.215, 0.61, 0.355, 1)`
                                  : `bottom 0.05s ease-in-out, transform 0.05s 0.1s cubic-bezier(0.215, 0.61, 0.355, 1)`
                          }`
                        : `${
                              lineIndex == 1
                                  ? `top 0.05s 0.1s ease-in-out, transform 0.05s cubic-bezier(0.55, 0.055, 0.675, 0.19)`
                                  : `bottom 0.05s 0.1s ease-in-out, transform 0.05s cubic-bezier(0.55, 0.055, 0.675, 0.19)`
                          }`
                }`,
            }}></span>
    )
}
BurgerLine.propTypes = {
    ratio: PropTypes.string,
}

BurgerLine.defaultProps = {
    ratio: '35%',
}

const Burger = props => {
    const { menuopen, onClick, width, height, ratio } = props
    return (
        <button sx={{ ...style.button, width: width, height: height }} onClick={onClick} {...props}>
            <BurgerLine menuopen={menuopen} lineIndex='1' ratio={ratio} />
            <BurgerLine menuopen={menuopen} lineIndex='2' ratio={ratio} />
        </button>
    )
}
Burger.propTypes = {
    size: PropTypes.array,
    width: PropTypes.array,
    height: PropTypes.array,
    ratio: PropTypes.number,
}

Burger.defaultProps = {
    size: [32, 32, 40, 50],
    width: ['32px', '32px', '40px', '50px'],
    height: ['32px', '32px', '40px', '50px'],
    ratio: 0.35,
}
export default Burger
