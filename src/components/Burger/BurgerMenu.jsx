/** @jsx jsx */
import { jsx, Styled, Box, Flex } from 'theme-ui'
import { darken, lighten } from '@theme-ui/color'
import React, { useRef, useEffect, createRef } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import { ColorMode } from 'components/ColorMode'

import { Burger } from 'components/Burger'
import { ContextConsumer } from 'components/Context'
import theme from 'theme'

const menuItems = [
    { to: '/work', label: 'Work' },
    // { to: '/blog', label: 'Blog' },
    { to: '/about', label: 'About' },
    { to: '/contact', label: 'Contact' },
]

const MenuItem = React.forwardRef((props, ref) => {
    return (
        <Link
            ref={ref}
            sx={{
                cursor: 'pointer',
                color: 'secondary',
                fontSize: 'calc(1.25rem + 5vh)',
                lineHeight: 'normal',
                fontWeight: 'normal',
                position: 'relative',
                textDecoration: 'none',
                '&:before,&:after': {
                    content: '""',
                    backgroundColor: 'transparent !important',
                },
                '&:hover,&.active': {
                    color: 'accent',
                },
                transition: `color 0.15 ease, opacity ${theme.animation.slower.duration}`,

                '&:focus': {
                    outline: `none`,
                    boxShadow: `none`,
                },
            }}
            activeClassName='active'
            {...props}
        />
    )
})

const PopUpMenu = ({ menuopen, set }) => {
    const linkrefs = useRef([...Array(menuItems.length)].map(() => createRef()))
    const buttonref = useRef()

    const handleClick = e => {
        for (let i = 0; i < linkrefs.current.length; i++) {
            if (linkrefs.current[i].current.contains(e.target)) {
                return
            }
        }
        if (buttonref.current.contains(e.target)) {
            return
        }
        if (e.target.className.includes('Burger')) {
            return
        }
        set({ burgerOpen: false })
    }

    const handleKey = event => {
        if (event.keyCode === 27) {
            set({ burgerOpen: false })
        }
    }

    useEffect(() => {
        document.addEventListener('mousedown', handleClick)
        document.addEventListener('keydown', handleKey)
        return () => {
            document.removeEventListener('mousedown', handleClick)
            document.removeEventListener('keydown', handleKey)
        }
    }, [])

    return (
        <React.Fragment>
            {menuItems.map((item, i) => (
                <MenuItem
                    key={item.label}
                    ref={linkrefs.current[i]}
                    to={item.to}
                    onClick={() => set({ burgerOpen: false, menuOpen: false })}>
                    {item.label}
                </MenuItem>
            ))}
            <Box ref={buttonref} sx={{ mt: 2 }}>
                <ColorMode />
            </Box>
        </React.Fragment>
    )
}

const PopUpWidget = props => {
    const { menuopen, children, direction, mysx } = props
    return (
        <div
            sx={{
                position: 'fixed',
                top: 0,
                left: `${direction == 'right' ? (menuopen ? '0' : '100%') : direction != 'left' ? '0' : ''}`,
                right: `${direction == 'left' ? (menuopen ? '0' : '100%') : direction != 'right' ? '0' : ''}`,
                top: `${direction == 'bottom' ? (menuopen ? '0' : '100%') : direction != 'top' ? '0' : ''}`,
                bottom: `${direction == 'top' ? (menuopen ? '0' : '100%') : direction != 'bottom' ? '0' : ''}`,
                height: '100%',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'backgroundDarker',
                opacity: 0.9,
                color: 'text',
                visibility: `${menuopen ? 'visible' : 'hidden'}`,
                transition: 'left 0.2s ease-in-out, visibility 0.2s linear',
                paddingTop: [2, 3, 4, 5, 6],
                ...mysx,
            }}>
            {children}
        </div>
    )
}

PopUpWidget.propTypes = {
    direction: PropTypes.string,
    zIndex: PropTypes.number,
}

PopUpWidget.defaultProps = {
    direction: 'right',
    zIndex: 9000,
}

const BurgerMenu = props => {
    const { sx } = props
    return (
        <ContextConsumer>
            {({ data, set }) => (
                <React.Fragment>
                    <Flex
                        sx={{
                            ...theme.layout.burger,
                            zIndex: 'burger',
                            flexFlow: 'row nowrap',
                            alignItems: 'center',
                            justifyContent: 'center',
                            alignContent: 'center',
                        }}>
                        <Burger menuopen={data.burgerOpen} onClick={() => set({ burgerOpen: !data.burgerOpen })} />
                    </Flex>
                    <PopUpWidget menuopen={data.burgerOpen} mysx={{ zIndex: 'burgerMenu' }}>
                        <PopUpMenu menuopen={data.burgerOpen} set={set}></PopUpMenu>
                    </PopUpWidget>
                </React.Fragment>
            )}
        </ContextConsumer>
    )
}

export default BurgerMenu
