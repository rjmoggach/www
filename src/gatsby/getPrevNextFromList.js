const _ = require("lodash")
const path = require(`path`)

const getPrevNextFromList = (list, item) => {
    // Create a random selection of the other posts (excluding the current post)
    const filterUnique = _.filter(list, input => input.node.fields.slug !== item.node.fields.slug)
    const sample = _.sampleSize(filterUnique, 2)
    // Only use the current language
    // const filterLanguage = _.filter(filterUnique, input => input.node.lang === item.node.lang)
    // const sample = _.sampleSize(filterLanguage, 2)

    return {
        prev: sample[0].node,
        next: sample[1].node,
    }
}

module.exports = {
    getPrevNextFromList,
}
