/** @jsx jsx */
import { jsx, Styled } from 'theme-ui'
import React from 'react'

export const wrapPageElement = ({ element }) => <Styled.root>{element}</Styled.root>

// export const wrapPageElement = ({ element }) => {element}
