const _ = require('lodash')
const path = require(`path`)

const addWorkExtras = (projects, albums, videos, credits) => {
    projects.forEach(project => {
        const { slug, sourceName } = project.node.fields
        albums.forEach(albumObj => {
            albumObj.node.fields.slug.startsWith(slug)
                ? (project.node['frontmatter']['albums'] = albumObj.node.frontmatter.albums)
                : null
        })
        videos.forEach(videosObj => {
            videosObj.node.fields.slug.startsWith(slug)
                ? (project.node['frontmatter']['videos'] = videosObj.node.frontmatter.albums)
                : null
        })
        credits.forEach(creditsObj => {
            creditsObj.node.fields.slug.startsWith(slug)
                ? (project.node['frontmatter']['videos'] = creditsObj.node.frontmatter.albums)
                : null
        })
    })
}

module.exports = {
    addWorkExtras,
}
