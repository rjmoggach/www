/** @jsx jsx */
import { jsx, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

// import ImageGallery from 'react-image-gallery'
// import 'react-image-gallery/styles/css/image-gallery.css'

// import AliceCarousel from 'react-alice-carousel'
// import 'react-alice-carousel/lib/alice-carousel.css'
// https://github.com/maxmarinich/react-alice-carousel

const AlbumCarousel = props => {
    const {
        album: { images: albumImages, path: albumPath, cover: cover },
        speed,
    } = props

    const settings = {
        dots: false,
        autoplay: true,
        infinite: true,
        speed: speed,
        fade: true,
        arrows: false,
        pauseOnHover: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        rtl: false, // Math.random() >= 0.5,
        vertical: false, //Math.random() >= 0.5,
    }

    const images = []

    if (!albumImages || albumImages.length === 0) return null

    return (
        <StaticQuery
            query={graphql`
                query {
                    allS3Asset(
                        filter: {
                            Key: { glob: "work/*/*/images/*/*.jpg" }
                            internal: { mediaType: { eq: "image/jpeg" } }
                        }
                    ) {
                        totalCount
                        edges {
                            node {
                                Key
                                url
                            }
                        }
                    }
                }
            `}
            render={queryData => {
                const allS3images = queryData.allS3Asset.edges
                var S3images = allS3images.filter(S3Image => {
                    return S3Image.node.Key.includes(albumPath)
                })
                // console.log('S3Carousel', S3images)
                if (S3images.length === 0) return null
                // switch (sourceName) {
                //     case 'blog': {
                switch (S3images.length) {
                    case 0: {
                        return null
                        break
                    }
                    default: {
                        return (
                            <Slider {...settings}>
                                {S3images.map((image, i) => (
                                    <img src={image.node.url} key={i} />
                                ))}
                            </Slider>
                        )
                        break
                    }
                }
            }}
        />
    )
}

AlbumCarousel.propTypes = {
    album: PropTypes.object,
}

AlbumCarousel.defaultProps = {
    album: {},
}

export default AlbumCarousel

// albums:
//     - images:
//           - caption: ''
//             filename: got-milk-thirsty.01.jpg
//           - caption: ''
//             filename: got-milk-thirsty.02.jpg
//           - caption: ''
//             filename: got-milk-thirsty.03.jpg
//           - caption: ''
//             filename: got-milk-thirsty.04.jpg
//       path: /work/2004/got-milk-thirsty/images/stills
//       cover:
//       slug: stills
//       title: Stills

// albums:
//     - images:
//           - caption: ''
//             filename: got-milk-thirsty.01.jpg
//           - caption: ''
//             filename: got-milk-thirsty.02.jpg
//           - caption: ''
//             filename: got-milk-thirsty.03.jpg
//           - caption: ''
//             filename: got-milk-thirsty.04.jpg
//       path: /work/2004/got-milk-thirsty/images/stills
//       cover:
//       slug: stills
//       title: Stills
