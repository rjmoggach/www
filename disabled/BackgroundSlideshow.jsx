/** @jsx jsx */
import { jsx, Box, Flex } from 'theme-ui'
import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

// import Slider from 'react-slick'
// import 'slick-carousel/slick/slick.css'
// import 'slick-carousel/slick/slick-theme.css'

import { Fade } from 'react-slideshow-image'
// https://github.com/femioladeji/react-slideshow#readme

// import ImageGallery from 'react-image-gallery'
// import 'react-image-gallery/styles/css/image-gallery.css'

// import AliceCarousel from 'react-alice-carousel'
// import 'react-alice-carousel/lib/alice-carousel.css'
// https://github.com/maxmarinich/react-alice-carousel

import BackgroundImage from 'gatsby-background-image'
import { get } from '@styled-system/css'
import { transparentize as Ptrans } from 'polished'
const g = (t, c) => get(t, `colors.${c}`, c)
export const transparentize = (c, n) => t => Ptrans(n, g(t, c))
import theme from 'theme'

const BackgroundSlideshow = props => {
    const {
        album: { images: albumImages, path: albumPath, cover: cover },
        speed,
    } = props

    const settings = {
        dots: false,
        autoplay: true,
        infinite: true,
        speed: speed,
        fade: true,
        arrows: false,
        pauseOnHover: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        rtl: false, // Math.random() >= 0.5,
        vertical: false, //Math.random() >= 0.5,
    }

    const properties = {
        duration: 5000,
        transitionDuration: 500,
        infinite: true,
        indicators: false,
        arrows: false,
        autoplay: true,
        infinite: true,
    }

    const images = []
    console.log('BackgroundSlideshow', get(theme, `colors.background`))
    if (!albumImages || albumImages.length === 0) return null

    return (
        <StaticQuery
            query={graphql`
                query {
                    allS3Asset(
                        filter: {
                            Key: { glob: "work/*/*/images/*/*.jpg" }
                            internal: { mediaType: { eq: "image/jpeg" } }
                        }
                    ) {
                        totalCount
                        edges {
                            node {
                                Key
                                url
                            }
                        }
                    }
                }
            `}
            render={queryData => {
                const allS3images = queryData.allS3Asset.edges
                var S3images = allS3images.filter(S3Image => {
                    return S3Image.node.Key.includes(albumPath)
                })
                // console.log('S3Carousel', S3images)
                if (S3images.length === 0) return null
                // switch (sourceName) {
                //     case 'blog': {
                switch (S3images.length) {
                    case 0: {
                        return null
                        break
                    }
                    default: {
                        return (
                            <Fade {...properties}>
                                {S3images.map((image, i) => (
                                    <div
                                        src={image.node.url}
                                        key={i}
                                        sx={{
                                            position: 'fixed',
                                            top: 0,
                                            left: 0,
                                            height: '80vh',
                                            width: '100vw',
                                            background: `url(${image.node.url}) no-repeat center center fixed`,
                                            backgroundSize: 'cover',
                                            opacity: '0.2 !important',
                                            '&:after': {
                                                content: '""',
                                                display: 'block',
                                                position: 'absolute',
                                                width: '100%',
                                                height: '100%',
                                                bottom: 0,

                                                background: `linear-gradient(transparent, ${get(
                                                    theme,
                                                    `colors.background`
                                                )}) left repeat`,
                                            },
                                        }}
                                    />
                                ))}
                            </Fade>
                        )
                        break
                    }
                }
            }}
        />
    )
}

BackgroundSlideshow.propTypes = {
    album: PropTypes.object,
}

BackgroundSlideshow.defaultProps = {
    album: {},
}

export default BackgroundSlideshow

// albums:
//     - images:
//           - caption: ''
//             filename: got-milk-thirsty.01.jpg
//           - caption: ''
//             filename: got-milk-thirsty.02.jpg
//       path: /work/2004/got-milk-thirsty/images/stills
//       cover:
//       slug: stills
//       title: Stills
