#!/usr/bin/env python3
import io
import markdown
import frontmatter
import os
import yaml
import datetime

indexMatter=['title','subtitle','slug','date','role','headline','excerpt','published','is_featured','categories','tags']
imagesMatter=['slug','date','published','is_featured','albums']
videosMatter=['slug','date','published','is_featured','videos']
creditsMatter=['slug','date','published','is_featured','credits']

def parse_markdown(filepath):
    # markdown_parser = markdown.Markdown(extensions=markdown_extensions)
    file_parts = frontmatter.load(filepath)

    return {
        'html': markdown_parser.convert(file_parts.content),
        'metadata': file_parts.metadata
    }

def dict_for_key(dict,key):
    return {key: dict[key]}

def get_filename_data(filename):
    bn=os.path.splitext(filename)[0]
    postyear=int(bn[0:4])
    postdate=datetime.date(int(bn[0:4]),int(bn[5:7]),int(bn[8:10]))
    postdir='work/{}/{}'.format(bn[0:4],bn[11:])
    postslug=bn[11:]
    return postdate, postdir, postslug, postyear

def represent_none(self, _):
    return self.represent_scalar('tag:yaml.org,2002:null', '')

yaml.add_representer(type(None), represent_none)


for filename in os.listdir(os.getcwd()):
    if filename.endswith(".md"):
        print(filename)
        with io.open(filename,'r') as f:
            post = frontmatter.load(f)
            post['date'],post['slug'],fileslug,year=get_filename_data(filename)
            postdir='{}/{}'.format(year,fileslug)
            os.makedirs(postdir,exist_ok=True)
            indexFile="{}/{}".format(postdir,'index.md')
            imagesFile="{}/{}".format(postdir,'images.md')
            videosFile="{}/{}".format(postdir,'videos.md')
            creditsFile="{}/{}".format(postdir,'credits.md')
            with io.open(indexFile,'w+',encoding='utf8') as outfile:
                outfile.write('---\n')
                yaml_cfg={'indent':4, 'allow_unicode':True, 'default_flow_style':False}

                for i in indexMatter:
                    if i=='excerpt':
                        try:
                            yaml.dump({'summary': post['excerpt'] }, outfile, **yaml_cfg)
                        except KeyError:
                            yaml.dump({'summary': None}, outfile, **yaml_cfg)
                    try:
                        yaml.dump(dict_for_key(post,i), outfile, **yaml_cfg)
                    except KeyError:
                        yaml.dump({i: None}, outfile, **yaml_cfg)
                outfile.write('---\n')
                outfile.write(post.content)

            for extra in ['images','videos','credits']:
                extraFile=eval('{}File'.format(extra))
                extraMatter=eval('{}Matter'.format(extra))
                with io.open(extraFile,'w+',encoding='utf8') as outfile:
                    outfile.write('---\n')
                    yaml_cfg={'indent':4, 'allow_unicode':True, 'default_flow_style':False}
                    for i in extraMatter:
                        try:
                            yaml.dump(dict_for_key(post,i), outfile, **yaml_cfg)
                        except KeyError:
                            yaml.dump({i: None}, outfile, **yaml_cfg)
                    outfile.write('---\n')
                        

                # break
            # frontmatter.dump(postObj,newfile)
            # newfile.close()
            # print(postObj)
            # print(post.content)
